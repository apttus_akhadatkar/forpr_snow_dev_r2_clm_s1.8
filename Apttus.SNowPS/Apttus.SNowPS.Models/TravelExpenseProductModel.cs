﻿/*************************************************************
@Name: TravelExpenseProductModel.cs
@Author: Nilesh Keshwala    
@CreateDate: 08-Nov-2017
@Description: This class contains classes & properties for 'Lineitem' and related objects.
@UsedBy: Description of how this class is being used
******************************************************************/

using Newtonsoft.Json;

namespace Apttus.SNowPS.Model
{
    public class TravelExpenseProductModel
    {
        public string Id { get; set; }
        public string Name { get; set; }
        [JsonProperty(PropertyName = "cpq_LineItem")]
        public TELineItemModel LineItem { get; set; }
    }
    public class TELineItemModel
    {
        public string Id { get; set; }
        public string Name { get; set; }
        public decimal? BaseExtendedPrice { get; set; }
        public int? ParentBundleNumber { get; set; }
        public LookUp ProductId { get; set; }
        public LookUp OptionId { get; set; }
    }

}
