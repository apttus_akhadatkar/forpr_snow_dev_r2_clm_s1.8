﻿/*************************************************************
@Name: AgreementObjectModel.cs
@Author: Bhavinkumar Mistry    
@CreateDate: 11-Nov-2017
@Description: This class contains classes & properties for 'Agreement' and related objects.
@UsedBy: This is mainly Used CLM controllers
******************************************************************
@ModifiedBy: Author who modified this class
@ModifiedDate: Date the class was modified
@ChangeDescription: A brief description of what was modified
 
 
**** PS: @ModifiedBy and @ChangeDescription does not apply to ‘In Development’ code – should be used for major changes in functionality or subsequent releases.
******************************************************************/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Apttus.SNowPS.Model
{
    public class AgreementObjectModel
    {
        public string Id { get; set; }
        public string Name { get; set; }
        public string AgreementNumber { get; set; }
        public string ext_PDFTemplate { get; set; }
        public LookUp ext_quoteid { get; set; }
        public string ext_QuoteNumber { get; set; }
        public string  ext_Release { get; set; }
        public string ext_UserType { get; set; }
        public SelectOption Status { get; set; }
        public SelectOption StatusCategory { get; set; }
    }
}
