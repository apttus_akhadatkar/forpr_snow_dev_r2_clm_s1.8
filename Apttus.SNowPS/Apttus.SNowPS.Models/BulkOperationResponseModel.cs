﻿/****************************************************************************************
@Name: AccesToken.cs
@Author: Mahesh Patel
@CreateDate: 25 Sep 2017
@Description: Bulk Operation Response Model 
@UsedBy: This will be used to modify response from generic API

@ModifiedBy: 
@ModifiedDate: 
@ChangeDescription: 
*****************************************************************************************/

using System.Collections.Generic;
using System.Runtime.Serialization;

namespace Apttus.SNowPS.Model
{
    [DataContract]
    public class BulkOperationResponseModel
    {
        [DataMember]
        public List<dynamic> Inserted { get; set; }
        [DataMember]
        public List<dynamic> Updated { get; set; }
        [DataMember]
        public List<dynamic> Deleted { get; set; }
        [DataMember]
        public List<ErrorInfo> Errors { get; set; }
    }

    [DataContract]
    public class ErrorInfo
    {
        [DataMember]
        public string Message { get; set; }
        [DataMember]
        public Dictionary<string, object> Record { get; set; }
    }
}
