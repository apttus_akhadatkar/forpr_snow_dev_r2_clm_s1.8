﻿/****************************************************************************************
@Name: AccountKeyRole.cs
@Author: Akash Kakadiya
@CreateDate: 18 Oct 2017
@Description:  
@UsedBy: This will be used by account key contact role 

@ModifiedBy: 
@ModifiedDate: 
@ChangeDescription: 
*****************************************************************************************/
using Apttus.SNowPS.Model;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.Runtime.Serialization;

public class KeyContactRoles
{
    public List<AccountKeyRole> contacts { get; set; } 
}

public class AccountKeyRole
{
    public string id { get; set; }

    public string roles { get; set; }
}

public class AccountKeyRoleApttus
{
    public string ExternalId { get; set; }
    public string[] ext_KeyContactRole { get; set; }
}