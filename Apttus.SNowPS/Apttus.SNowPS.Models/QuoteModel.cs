﻿/****************************************************************************************
@Name: Quote.cs
@Author: Kiran Satani
@CreateDate: 27 Sep 2017
@Description: All require attributes of Apttus Quote for mule quote sync 
@UsedBy: This will be used by API to get Quote Details

@ModifiedBy: Asha Tank
@ModifiedDate: 12 Oct 2017
@ChangeDescription: Added Model for quote header fields
*****************************************************************************************/

using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;

namespace Apttus.SNowPS.Model
{
	public class QuoteModel
	{
		[JsonProperty(PropertyName = "id")]
		public string Id { get; set; }

        [JsonProperty(PropertyName = "AccountId")]
        public LookUp AccountId { get; set; }

        [JsonProperty(PropertyName = "opportunityID")]
        public string OpportunitySysId { get; set; }

		[JsonProperty(PropertyName = "number")]
		public string Name { get; set; }

		[JsonProperty(PropertyName = "name")]
		public string QuoteName { get; set; }

		[JsonProperty(PropertyName = "accountMdmID")]
		public string AccountMDMID { get; set; }

		[JsonProperty(PropertyName = "type")]
		public string Type { get; set; }

		[JsonProperty(PropertyName = "contractType")]
		public string ContractType { get; set; }

		[JsonProperty(PropertyName = "startDate")]
		public DateTime? ExpectedStartDate { get; set; }

		[JsonProperty(PropertyName = "endDate")]
		public DateTime? ExpectedEndDate { get; set; }

		[JsonProperty(PropertyName = "term")]
		public int? Term { get; set; }

		[JsonProperty(PropertyName = "isPrimary")]
		public bool? IsPrimary { get; set; }

		[JsonProperty(PropertyName = "salesType")]
		public string salestype { get; set; }

		[JsonProperty(PropertyName = "partnerAccountID")]
		public string PartnerAccount { get; set; }


        [JsonProperty(PropertyName = "ext_SalesPartner")]
        public LookUp SalesPartner { get; set; }


        [JsonProperty(PropertyName = "partnershipType")]
        public string PartnershipType { get; set; }

		[JsonProperty(PropertyName = "stage")]
		public string ApprovalStage { get; set; }

		[JsonProperty(PropertyName = "approvalStatus")]
		public string ApprovalStatus { get; set; }

		[JsonProperty(PropertyName = "netNewACV")]
		public decimal? NNACV { get; set; }

		[JsonProperty(PropertyName = "totalAgreementValue")]
		public decimal? TCV { get; set; }

		[JsonProperty(PropertyName = "renewalACV")]
		public decimal? RenewalACV { get; set; }

		[JsonProperty(PropertyName = "lineItems")]
		public List<QuoteLineItem> QLI { get; set; }

		[JsonProperty(PropertyName = "milestones")]
		public List<Milestone> MS { get; set; }

		[JsonProperty(PropertyName = "ext_sellingentity")]
		public string SellingEntity { get; set; }

		[JsonProperty(PropertyName = "ext_ShipToCountry")]
		public string ShipToCountry { get; set; }

		[JsonProperty(PropertyName = "ext_ShipToState")]
		public string ShipToState { get; set; }

		[JsonProperty(PropertyName = "territoryMdmID")]
		public string FieldTerritoryMDMID { get; set; }

		[JsonProperty(PropertyName = "psTerritoryMdmID")]
		public string PSTerritoryMDMID { get; set; }

		[JsonProperty(PropertyName = "quoteOwnerID")]
		public string QuoteOwner { get; set; }

		[JsonProperty(PropertyName = "opportunityLinkReason")]
		public string OpportunityLinkReason { get; set; }

		[JsonProperty(PropertyName = "flags")]
		public string FlaggedAs { get; set; }

		[JsonProperty(PropertyName = "netNewACVInUSD")]
		public decimal? USDTotalNetNewACV { get; set; }

		[JsonProperty(PropertyName = "acv")]
		public decimal? ACV { get; set; }

		[JsonProperty(PropertyName = "acvInUSD")]
		public decimal? USDACV { get; set; }

		[JsonProperty(PropertyName = "totalAgreementValueInUSD")]
		public decimal? USDTCV { get; set; }

		[JsonProperty(PropertyName = "renewalACVInUSD")]
		public decimal? USDRenewalACV { get; set; }

        [JsonProperty(PropertyName = "baseCurrency")]
        public string BaseCurrency { get; set; }

        [JsonProperty(PropertyName = "partnerAgreementNumber")]
        public string PartnerAgreementNumber { get; set; }

        [JsonProperty(PropertyName = "endCustomerMDMID")]
        public string EndCustomerMDMSysID { get; set; }

        [JsonProperty(PropertyName = "isAssociatedPartner")]
        public string DealRegAssociated { get; set; }

        [JsonProperty(PropertyName = "ext_taxexempt")]
        public bool TaxExempt { get; set; }

        [JsonProperty(PropertyName = "ext_TaxCalculation")]
        public bool TaxCalculation { get; set; }

        [JsonProperty(PropertyName = "ext_ShipToLocation")]
        public string ShipToLocation { get; set; }

        [JsonProperty(PropertyName = "ext_ BillToLocation")]
        public string BillToLocation { get; set; }

        [JsonProperty(PropertyName = "ext_RefAgreement")]
        public string ReferenceAgreement { get; set; }
    }

    public class QuoteLineItem
    {
        [JsonProperty(PropertyName = "id")]
        public string Id { get; set; }

        [JsonProperty(PropertyName = "name")]
        public string Name { get; set; }

        [JsonProperty(PropertyName = "quoteID")]
        public string QuoteId { get; set; }

        [JsonProperty(PropertyName = "productID")]
        public string ProductId { get; set; }

        [JsonProperty(PropertyName = "dailyExchangeRate")]
        public decimal? FXRate { get; set; }

        [JsonProperty(PropertyName = "dailyExchangeRateInUSD")]
        public decimal? FXRateDailyUSD { get; set; }

        [JsonProperty(PropertyName = "listPrice")]
        public decimal? ListPrice { get; set; }

        [JsonProperty(PropertyName = "number")]
        public int? PrimaryLineNumber { get; set; }

        [JsonProperty(PropertyName = "itemSequenceNumber")]
        public string ItemSequenceNumber { get; set; }

        [JsonProperty(PropertyName = "parentBundleID")]
        public int? ParentBundleNumber { get; set; }

        //[JsonProperty(PropertyName = "productHours")]
        //public int? ProductHours { get; set; }

        [JsonProperty(PropertyName = "doNotCalculateNetNewACV")]
        public bool? DoNotCalculateNNACV { get; set; }

        [JsonProperty(PropertyName = "variablePriceACVInUSD")]
        public decimal? USDVariablePriceACVCalc { get; set; }

        [JsonProperty(PropertyName = "salesPrice")]
        public decimal? SalesPrice { get; set; }

        [JsonProperty(PropertyName = "netPrice")]
        public decimal? NetPrice { get; set; }

        [JsonProperty(PropertyName = "variablePriceACVCalculate")]
        public decimal? VariablePriceACVCalc { get; set; }

        [JsonProperty(PropertyName = "baseAmount")]
        public decimal? PriceAdjustment { get; set; }

        [JsonProperty(PropertyName = "renewalLineACV")]
        public decimal? RenewalLineACV { get; set; }

        [JsonProperty(PropertyName = "startDate")]
        public DateTime? StartDate { get; set; }

        [JsonProperty(PropertyName = "endDate")]
        public DateTime? EndDate { get; set; }

        [JsonProperty(PropertyName = "term")]
        public int? Term { get; set; }

        [JsonProperty(PropertyName = "summaryLineACVInUSD")]
        public decimal? USDSummaryLineACV { get; set; }

        [JsonProperty(PropertyName = "netNewACVInUSD")]
        public decimal? USDNetNewACV { get; set; }

        [JsonProperty(PropertyName = "quantity")]
        public decimal? Quantity { get; set; }

        [JsonProperty(PropertyName = "netNewACV")]
        public decimal? NetNewACV { get; set; }

        [JsonProperty(PropertyName = "totalValue")]
        public decimal? ExtendedPrice { get; set; }

        [JsonProperty(PropertyName = "listPriceInUSD")]
        public decimal? USDListPrice { get; set; }

        [JsonProperty(PropertyName = "basePriceInUSD")]
        public decimal? USDBasePrice { get; set; }
        public decimal? pricingExchangeRate { get; set; }

        [JsonProperty(PropertyName = "totalValueInUSD")]
        public decimal? USDTotalValue { get; set; }

        [JsonProperty(PropertyName = "salesPriceInUSD")]
        public decimal? USDSalesPrice { get; set; }

        [JsonProperty(PropertyName = "annualListPriceInUSD")]
        public decimal? AnnualListPrice { get; set; }
        public decimal? annualListPrice { get; set; }

        [JsonProperty(PropertyName = "annualContractAmountInUSD")]
        public decimal? USDAnnualContractValue { get; set; }

        [JsonProperty(PropertyName = "annualContractValue")]
        public decimal? AnnualContractValue { get; set; }

        [JsonProperty(PropertyName = "deployment")]
        public string Deployment { get; set; }

        [JsonProperty(PropertyName = "resourcePlan")]
        public string ResourcePlan { get; set; }

        [JsonProperty(PropertyName = "taskID")]
        public string TaskID { get; set; }

        [JsonProperty(PropertyName = "status")]
        public string LineStatus { get; set; }

        [JsonProperty(PropertyName = "type")]
        public string LineType { get; set; }

        [JsonProperty(PropertyName = "ext_InstanceName")]
        public string InstanceName { get; set; }

        public Product Product { get; set; }

        [JsonProperty(PropertyName = "ext_SoldLINameInstMap")]
        public List<string> SoldLINameInstMap { get; set; }

        [JsonProperty(PropertyName = "ext_InstalledLINameInstMap")]
        public List<string> InstalledLINameInstMap { get; set; }

        [JsonProperty(PropertyName = "ext_TCV")]
        public decimal? TCV { get; set; }

        [JsonProperty(PropertyName = "ext_ContributesTo")]
        public string ContributesTo { get; set; }

        //[JsonProperty(PropertyName = "application")]
        //public string Application { get; set; }

        [JsonProperty(PropertyName = "netNewQuantity")]
        public decimal? NetNewQuantity { get; set; }

        [JsonProperty(PropertyName = "deltaQuantity")]
        public decimal? DeltaQuantity { get; set; }

        [JsonProperty(PropertyName = "revRecType")]
        public string RevRecType { get; set; }
        
    }

    public class Milestone
    {
        [JsonProperty(PropertyName = "id")]
        public string Id { get; set; }

        [JsonProperty(PropertyName = "amount")]
        public decimal? Amount { get; set; }

        [JsonProperty(PropertyName = "calculationMethod")]
        public string CalculationMethod { get; set; }

        [JsonProperty(PropertyName = "estimatedDueDate")]
        public DateTime? EstimatedDueDate { get; set; }

        [JsonProperty(PropertyName = "description")]
        public string ExtMilestone { get; set; }

        [JsonProperty(PropertyName = "paymentOrderNumber")]
        public int? PaymentOrder { get; set; }

        [JsonProperty(PropertyName = "percentage")]
        public decimal? Percent { get; set; }

        [JsonProperty(PropertyName = "estimatedHours")]
        public int? EstimatedHours { get; set; }

        [JsonProperty(PropertyName = "agreementID")]
        public string AgreementId { get; set; }
    }

    /***********************  Response Quote from Apptus *******************************/
    public class ResponseQuote
    {
        public string Id { get; set; }

        [JsonProperty(PropertyName = "ext_OpportunitySysId")]
        public string OpportunitySysId { get; set; }
        public string Name { get; set; }
        public string QuoteName { get; set; }

        [JsonProperty(PropertyName = "ext_AccountMDMID")]
        public string AccountMDMID { get; set; }

        [JsonProperty(PropertyName = "ext_Type")]
        public ChoiceType Type { get; set; }

        [JsonProperty(PropertyName = "ext_ContractType")]
        public ChoiceType ContractType { get; set; }

        public DateTime? ExpectedStartDate { get; set; }
        public DateTime? ExpectedEndDate { get; set; }

        [JsonProperty(PropertyName = "ext_Term")]
        public int? Term { get; set; }

        public bool? IsPrimary { get; set; }

        [JsonProperty(PropertyName = "ext_salestype")]
        public ChoiceType SalesType { get; set; }

        [JsonProperty(PropertyName = "ext_PartnerAccount")]
        public LookUp PartnerAccount { get; set; }

        [JsonProperty(PropertyName = "ext_partnershiptype")]
        public ChoiceType PartnershipType { get; set; }

        public ChoiceType ApprovalStage { get; set; }

        public ChoiceType ApprovalStatus { get; set; }

        [JsonProperty(PropertyName = "ext_NNACV")]
        public decimal? NNACV { get; set; }

        [JsonProperty(PropertyName = "ext_TCV")]
        public decimal? TCV { get; set; }

        [JsonProperty(PropertyName = "ext_RenewalACV")]
        public decimal? RenewalACV { get; set; }

        [JsonProperty(PropertyName = "ext_FieldTerritoryMDMID")]
        public string FieldTerritoryMDMID { get; set; }

        [JsonProperty(PropertyName = "ext_PSTerritoryMDMID")]
        public string PSTerritoryMDMID { get; set; }

        [JsonProperty(PropertyName = "ext_QuoteOwner")]
        public string QuoteOwner { get; set; }

        [JsonProperty(PropertyName = "ext_OpportunityLinkReason")]
        public ChoiceType OpportunityLinkReason { get; set; }

		[JsonProperty(PropertyName = "ext_FlaggedAs")]
		public string FlaggedAs { get; set; }

		[JsonProperty(PropertyName = "ext_USDTotalNetNewACV")]
		public decimal? USDTotalNetNewACV { get; set; }

		[JsonProperty(PropertyName = "ext_ACV")]
		public decimal? ACV { get; set; }

		[JsonProperty(PropertyName = "ext_USDACV")]
		public decimal? USDACV { get; set; }

		[JsonProperty(PropertyName = "ext_USDTCV")]
		public decimal? USDTCV { get; set; }

		[JsonProperty(PropertyName = "ext_USDRenewalACV")]
		public decimal? USDRenewalACV { get; set; }

        [JsonProperty(PropertyName = "ext_BaseCurrency")]
        public string BaseCurrency { get; set; }

        [JsonProperty(PropertyName = "ext_PartnerAgreementNumber")]
        public string PartnerAgreementNumber { get; set; }

        [JsonProperty(PropertyName = "ext_EndCustomerMDMSysID")]
        public string EndCustomerMDMSysID { get; set; }

        [JsonProperty(PropertyName = "ext_DealRegAssociated")]
        public string DealRegAssociated { get; set; }

        public QuoteLineItemResponse QLI { get; set; }

        public MilestoneResponse MS { get; set; }
    }

    public class QuoteLineItemResponse
    {
        public string Id { get; set; }
        public LookUp QuoteId { get; set; }
        public LookUp ProductId { get; set; }
        public LookUp OptionId { get; set; }  //Revert after 1.5 Sprint

        [JsonProperty(PropertyName = "ext_FXRate")]
        public decimal? FXRate { get; set; }

        [JsonProperty(PropertyName = "ext_FXRateDailyUSD")]
        public decimal? FXRateDailyUSD { get; set; }
        public decimal? ListPrice { get; set; }

        [JsonProperty(PropertyName = "PrimaryLineNumber")]
        public int? PrimaryLineNumber { get; set; }

        [JsonProperty(PropertyName = "ext_ItemSequenceNumber")]
        public string ItemSequenceNumber { get; set; }
        public int? ParentBundleNumber { get; set; }

        //[JsonProperty(PropertyName = "ext_ProductHours")]
        //public int? ProductHours { get; set; }

        [JsonProperty(PropertyName = "ext_DoNotCalculateNNACV")]
        public bool? DoNotCalculateNNACV { get; set; }

        [JsonProperty(PropertyName = "ext_USDVariablePriceACVCalc")]
        public decimal? USDVariablePriceACVCalc { get; set; }

        [JsonProperty(PropertyName = "ext_SalesPrice")]
        public decimal? SalesPrice { get; set; }

        [JsonProperty(PropertyName = "ext_VariablePriceACVCalc")]
        public decimal? VariablePriceACVCalc { get; set; }
        //public decimal? PriceAdjustment { get; set; }

        [JsonProperty(PropertyName = "ext_ListPriceWithAdjustments")]
        public decimal? ListPriceWithAdjustments { get; set; }

        [JsonProperty(PropertyName = "ext_RenewalLineACV")]
        public decimal? RenewalLineACV { get; set; }
        public DateTime? StartDate { get; set; }
        public DateTime? EndDate { get; set; }
        public int? Term { get; set; }

        [JsonProperty(PropertyName = "ext_USDSummaryLineACV")]
        public decimal? USDSummaryLineACV { get; set; }

        [JsonProperty(PropertyName = "ext_USDNetNewACV")]
        public decimal? USDNetNewACV { get; set; }

        [JsonProperty(PropertyName = "Quantity2")]
        public decimal? Quantity { get; set; }

        [JsonProperty(PropertyName = "ext_NetNewACV")]
        public decimal? NetNewACV { get; set; }
        public decimal? ExtendedPrice { get; set; }

        [JsonProperty(PropertyName = "ext_USDListPrice")]
        public decimal? USDListPrice { get; set; }

        [JsonProperty(PropertyName = "ext_USDBasePrice")]
        public decimal? USDBasePrice { get; set; }
        //public decimal? ext_FXRate { get; set; }

        [JsonProperty(PropertyName = "ext_USDTotalValue")]
        public decimal? USDTotalValue { get; set; }

        [JsonProperty(PropertyName = "ext_USDSalesPrice")]
        public decimal? USDSalesPrice { get; set; }

        [JsonProperty(PropertyName = "ext_AnnualListPrice")]
        public decimal? AnnualListPrice { get; set; }

        [JsonProperty(PropertyName = "ext_USDAnnualContractValue")]
        public decimal? USDAnnualContractValue { get; set; }

        [JsonProperty(PropertyName = "ext_AnnualContractValue")]
        public decimal? AnnualContractValue { get; set; }

        [JsonProperty(PropertyName = "ext_Deployment")]
        public string Deployment { get; set; }

        [JsonProperty(PropertyName = "ext_ResourcePlan")]
        public string ResourcePlan { get; set; }

        [JsonProperty(PropertyName = "ext_TaskID")]
        public string TaskID { get; set; }
        public ChoiceType LineStatus { get; set; }
        public ChoiceType LineType { get; set; }

        //[JsonProperty(PropertyName = "ext_ApplicationPS")]
        //public string Application { get; set; }

        [JsonProperty(PropertyName = "ext_DefaultRevRec")]
        public string RevRecType { get; set; }

        [JsonProperty(PropertyName = "ext_NetNewQuantity")]
        public decimal? NetNewQuantity { get; set; }

        [JsonProperty(PropertyName = "DeltaQuantity")]
        public decimal? DeltaQuantity { get; set; }
    }

    public class MilestoneResponse
    {
        [JsonProperty(PropertyName = "Id")]
        public string Id { get; set; }
        public string Name { get; set; }

        [JsonProperty(PropertyName = "ext_QuoteId")]
        public LookUp QuoteId { get; set; }

        [JsonProperty(PropertyName = "ext_Amount")]
        public decimal? Amount { get; set; }

        [JsonProperty(PropertyName = "ext_CalculationMethod")]
        public ChoiceType CalculationMethod { get; set; }

        [JsonProperty(PropertyName = "ext_EstimatedDueDate")]
        public DateTime? EstimatedDueDate { get; set; }

        [JsonProperty(PropertyName = "ext_Milestone")]
        public string Milestone { get; set; }

        [JsonProperty(PropertyName = "ext_PaymentOrder")]
        public int? PaymentOrder { get; set; }

        [JsonProperty(PropertyName = "ext_Percent")]
        public decimal? Percent { get; set; }

        [JsonProperty(PropertyName = "ext_Number")]
        public string Number { get; set; }

        [JsonProperty(PropertyName = "EstimatedHours")]
        public int? ext_EstimatedHours { get; set; }

        [JsonProperty(PropertyName = "ext_AgreementId")]
        public LookUp AgreementId { get; set; }
    }

    public class ChoiceType
    {
        public string Key { get; set; }
        public string Value { get; set; }
    }

    public class LookUp
    {
        public string Id { get; set; }
        public string Name { get; set; }
		public string ExternalId { get; set; }
        public ChoiceType Ext_Family { get; set; }
		public string ConfigurationType { get; set; }
    }

    #region Quote Header 
    public class QuoteQuoteLineDetails
    {
        public string Id { get; set; }
        public string Name { get; set; }
        public QQuoteLineItem cpq_lineitem { get; set; }
        public QuoteQuoteLineItem cpq_QuoteLineItem { get; set; }
        public QuoteLineItemProduct crm_Product { get; set; }
        public string RowVersion { get; set; }

        //For Invoice Schedule
        public DateTime? expectedStartDate { get; set; }
        public DateTime? expectedEndDate { get; set; }
        public int netPaymentDueIn { get; set; }
        //public int? paymentTerm { get; set; }
        public ChoiceType paymentTerm { get; set; }
        public ChoiceType ext_Type { get; set; }

    }

    public class QQuoteLineItem
    {
        public string Id { get; set; }
        public string Name { get; set; }
        public string CurrencyId { get; set; }
    }

    public class QuoteQuoteLineItem
    {
        public string Id { get; set; }
        public string Name { get; set; }
        public decimal? NetPrice { get; set; }

        [JsonProperty(PropertyName = "ext_EstimatedTotal")]
        public decimal? EstimatedTotal { get; set; }
        [JsonProperty(PropertyName = "ext_AnnualListPrice")]
        public decimal? AnnualListPrice { get; set; }
        [JsonProperty(PropertyName = "ext_AnnualContractValue")]
        public decimal? AnnualContractValue { get; set; }
        public string ext_CurrencyCode { get; set; }

        //Added for USD Fields
        public decimal? ListPrice { get; set; }
        //public decimal? Quantity { get; set; }
        public decimal? Quantity2 { get; set; }

        public decimal? Term { get; set; }
        public decimal? ext_USDBasePrice { get; set; }
        public decimal? ext_TermNew { get; set; }
        public decimal? ext_SalesPrice { get; set; }
        public SellingFrequency SellingFrequency { get; set; }
        //public decimal? ext_AnnualListPrice { get; set; }
        public decimal? ext_NetNewACV { get; set; }
        public decimal? BasePrice { get; set; }
        public decimal? ext_TCV { get; set; }
        public decimal? NetUnitPrice { get; set; }
        public decimal? ext_EstimatedTax { get; set; }

        public ChoiceType ext_FlaggedAs { get; set; }
        public string StartDate { get; set; }
        public string EndDate { get; set; }
        public decimal? DeltaQuantity { get; set; }
        public decimal? ExtendedPrice { get; set; }
        public string CurrencyId { get; set; }

        public ChoiceType PriceType { get; set; }
        public LookUp ProductId { get; set; }
        public decimal Ext_AdjustmentAmount { get; set; }

    }
   
    public class QuoteLineItemProduct
    {
        public string Id { get; set; }
        public string Name { get; set; }
        [JsonProperty(PropertyName = "ext_Family")]
        public ChoiceType Family { get; set; }
        [JsonProperty(PropertyName = "ext_ContributesTo")]
        public ChoiceType ContributesTo { get; set; }
    }

    public class ResultQuoteHeaderModel
    {
        public string Id { get; set; }
        public decimal ext_EducationalServicesandKnowledgeandOther { get; set; }
        public decimal ext_EstimatedTotal { get; set; }
        public decimal ext_EstimatedTravelExpenseFees { get; set; }
        public decimal ext_TotalLicenseValue { get; set; }
        public decimal ext_TotalServicesRevenue { get; set; }
        public decimal ext_TotalOtherRevenue { get; set; }
        public decimal ext_TotalPSRevenue { get; set; }
        public decimal ext_PreTaxTotals { get; set; }
        public decimal ext_TotalAnnualListPrice { get; set; }
        public decimal ext_TotalSubscriptionACV { get; set; }
        public decimal ext_TotalTrainingRevenue { get; set; }
        public decimal ext_USDTotalAnnualListPrice { get; set; }
        public decimal ext_USDTotalLicenseValue { get; set; }
        public decimal ext_USDTotalOtherRevenue { get; set; }
        public decimal ext_USDTotalPSRevenue { get; set; }
        public decimal ext_USDTotalServicesRevenue { get; set; }
        public decimal ext_USDTotalSubscriptionACV { get; set; }
        public decimal ext_USDTotalTrainingRevenue { get; set; }
    }

    //Response Class for Line items calculated financial fields
    public class ResultUSDQuoteHeaderLineItemFields
    {
        public ResultQuoteHeaderFinFields QuoteHeaderFinFields { get; set; }
        public List<ResultQuoteLineItem> ListQuoteLineItemFinFields { get; set; }
    }
    public class ResultQuoteHeaderFinFields
    {
        public decimal? ext_USDTotalAnnualListPrice { get; set; }
        public decimal? ext_USDTotalLicenseValue { get; set; }
        public decimal? ext_USDTotalNetNewACV { get; set; }
        public decimal? ext_USDTotalOtherRevenue { get; set; }
        public decimal? ext_USDTotalPSRevenue { get; set; }
        public decimal? ext_USDTotalServicesRevenue { get; set; }
        public decimal? ext_USDTotalSubscriptionACV { get; set; }
        public decimal? ext_USDTotalTrainingRevenue { get; set; }

    }
    public class ResultQuoteLineItem
    {
        public string Id { get; set; }
        public decimal? ext_ListPriceWithAdjustments { get; set; }
        public decimal ext_AdjustmentAmount { get; set; }
        public decimal ext_AdjustmentDiscountPercent { get; set; }
        public decimal? ext_USDAdjustment { get; set; }
        public decimal? ext_AnnualContractValue { get; set; }
        public decimal? ext_AnnualListPrice { get; set; }
        public decimal? ext_EstimatedTax { get; set; }
        //public decimal? NetPrice { get; set; }
        public ChoiceType ext_FlaggedAs { get; set; }
        //public decimal? BasePrice { get; set; }
        public decimal? ext_USDBasePrice { get; set; }
        public decimal? ext_USDTotalValue { get; set; }
        public decimal? ext_USDAnnualListPrice { get; set; }
        public decimal? ext_USDListPrice { get; set; }
        public decimal? ext_USDSalesPrice { get; set; }
        public decimal? ext_USDAnnualContractValue { get; set; }
        public decimal? ext_EstimatedTotal { get; set; }
        public decimal? ext_SalesPrice { get; set; }
    }
    #endregion

    #region InvoiceSchedule
    public class QuoteLineItemInvSchedule
    {
        public int invoiceScheduleNumber { get; set; }
        public string invoiceScheduleName { get; set; }
        public string dueDate { get; set; } // Cannot assign a DateTime datatype as this may also contain text eg. Upon signature
        public decimal? amountToBeCharged { get; set; }
    }
    #endregion
    public class QuoteValidate
    {
        public string Id { get; set; }
        public string OpportunityId { get; set; }
        public bool IsPrimary { get; set; }
        public string ext_OpportunitySysId { get; set; }
        public AccountCountry crm_Account { get; set; }

        public ChoiceType ext_Type { get; set; }
        public LookUp PriceListId { get; set; }
        public string ext_PartnerAccountMDMID { get; set; }
        public string ext_BaseCurrency { get; set; }
    }

    public class AccountCountry
    {
        public string ext_Country { get; set; }
        public string Id { get; set; }
        public string Name { get; set; }

    }
    public class ValidateDefaultQuoteResult
    {
        public string ext_datacenter { get; set; }
        public bool IsPrimarySet { get; set; }
        public string ext_SellingEntity { get; set; }
        public string PriceListName { get; set; }
        public string BaseCurrency { get; set; }
        public string ErrorMessage { get; set; }
    }

    #region Approvals On Quote

    public class QuoteResponseApproval
    {
        public string Id { get; set; }
        public string ext_FlaggedAs { get; set; }
        public ChoiceType ext_Type { get; set; }
        public ChoiceType ext_salestype { get; set; }
        public CustomerAccount AccountId { get; set; }
        public CustomerAccount ext_SalesPartner { get; set; }
        public double? ext_DealSize { get; set; }
        public decimal? ext_MaxDiscount { get; set; }
        public bool? ext_SpecificDataCenters { get; set; }
        public int? ext_RenewalCount { get; set; }
        public bool? ext_ApproverUserDiscount { get; set; }
        public bool? ext_CustomerSatisfactionExceptionApproval { get; set; }
        public bool? ext_EndOfLifeProductsApproval { get; set; }
        public QLIResponseApproval QLI { get; set; }
        public AgreementResponseApproval AGR { get; set; }

        public class QLIResponseApproval
        {
            public string Id { get; set; }
            public double? ext_USDAnnualListPrice { get; set; }
            public decimal? ext_AdjustmentDiscountPercent { get; set; }
            public QLIProductApproval ProductId { get; set; }
            public QuoteProductAttribute AttributeValueId { get; set; }
            public double? ext_USDListPrice { get; set; }
            public double? ext_ContractQuantity { get; set; }
            public class QLIProductApproval
            {
                public string Id { get; set; }
                public string Name { get; set; }
                public ChoiceType ext_Family { get; set; }
                public ChoiceType ext_ProductLifecycleStatus { get; set; }
            }
        }
        public class CustomerAccount
        {
            public string Id { get; set; }
            public string Name { get; set; }
            public ChoiceType ext_Segment { get; set; }

        }
        public class QuoteProductAttribute
        {
            public string Id { get; set; }
            public string Name { get; set; }
            public ChoiceType ext_UserModel { get; set; }

        }

        public class AgreementResponseApproval
        {
            //public LookUp ext_QuoteId { get; set; }
            public Quote ext_QuoteId { get; set; }
            public class Quote
            {
                public string Id { get; set; }
                public string Name { get; set; }
                public double? ext_DealSize { get; set; }
            }
        }
    }

    public class PSQuoteResponseApproval
    {
        public string Id { get; set; }
        // public decimal?  ext_TEDiscount { get; set; }
        public PSQLIResponseApproval QLI { get; set; }
        public PSMSResponseApproval MS { get; set; }
        public class PSMSResponseApproval
        {
            public string Id { get; set; }
            public string Name { get; set; }
        }
        public class PSQLIResponseApproval
        {
            public string Id { get; set; }
            public decimal? ext_EstimatedTravelExpenseFees { get; set; }
            public decimal? ext_AdjustmentDiscountPercent { get; set; }
            public PSQLIProductApproval ProductId { get; set; }
            public class PSQLIProductApproval
            {
                public string Id { get; set; }
                public string Name { get; set; }

            }
        }
    }

    #endregion Approvals On Quote

    public class QuoteLineItemInvoiceSchedule
    {
        public string InvoiceSchedule { get; set; }
        public string ScheduleName { get; set; }
        public string DueDate { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public double Amount { get; set; }
        public string ProductCategory { get; set; }
    }

    public class InvoiceSelectDetails
    {
        public string Id { get; set; }
        public string Name { get; set; }
    }

    public class CreateInvoiceDetails
    {
        public string Name { get; set; }
        public string ext_InvoiceSchedule { get; set; }
        public string ext_scheduleDueDate { get; set; }
        public double ext_Amount { get; set; }
        public string ext_Quote { get; set; }
    }
}
