﻿/****************************************************************************************
@Name: AddressModel.cs
@Author: Akash Kakadiya
@CreateDate: 28 Sep 2017
@Description: Address Model
@UsedBy: This will be used for address data binding 

@ModifiedBy: 
@ModifiedDate: 
@ChangeDescription: 
*****************************************************************************************/
using Newtonsoft.Json;
using System;
using System.Collections.Generic;

namespace Apttus.SNowPS.Model
{
    public class AddressModel
    {
        [JsonProperty(PropertyName = "Name")]
        public string orderAccountName { get; set; }

        [JsonProperty(PropertyName = "ext_AddressLine1")]
        public string addressLine1 { get; set; }

        [JsonProperty(PropertyName = "ext_AddressLine2")]
        public string addressLine2 { get; set; }

        [JsonProperty(PropertyName = "ModifiedOn")]
        public string lastUpdatedDateTime { get; set; }

        [JsonProperty(PropertyName = "City")]
        public string city { get; set; }

        [JsonProperty(PropertyName = "State")]
        public string stateCode { get; set; }

        [JsonProperty(PropertyName = "PostalCode")]
        public string postalCode { get; set; }

        [JsonProperty(PropertyName = "Country")]
        public string countryCode { get; set; }

        public string sourceSystem { get; set; }

        public string addressTypeCode { get; set; }

        [JsonProperty(PropertyName = "Id")]
        public string sourceAddressId { get; set; }

        [JsonProperty(PropertyName = "ext_PrimaryBillTo")]
        public bool? primaryBillTo { get; set; }

        [JsonProperty(PropertyName = "ext_PrimaryShipTo")]
        public bool? primaryShipTo { get; set; }
        
        public int? sendToSAPIndicator { get; set; }

        [JsonProperty(PropertyName = "ext_MDMAddressOverriddenFlag")]
        public string useOriginalIndicator { get; set; }

        public string vatRegistrationNumber { get; set; }

        public string ExternalId { get; set; }

    }
    public class AddressAccountModel
    {
        public string eventType { get; set; }

        public string accountId { get; set; }

        [JsonProperty(PropertyName = "ExternalId")]
        public string mdmId { get; set; }

        [JsonProperty(PropertyName = "ModifiedOn")]
        public string lastUpdatedDateTime { get; set; }

        [JsonProperty(PropertyName = "Id")]
        public string srcPartyId { get; set; }

        [JsonProperty(PropertyName = "Name")]
        public string partyNm { get; set; }

        //[JsonProperty(PropertyName = "Type")]
        //public string partyTypCd { get; set; }

        public string boClassCode { get; set; }

        public List<AddressModel> addresses { get; set; }
    }
    public class AddressSearchRequestModel
    {
        public string partyName { get; set; }
        public string accountType { get; set; }
        public string searchType { get; set; }
        public string sourceSystem { get; set; }
        public string country { get; set; }
        public string zipCode { get; set; }
        public string state { get; set; }
        public string city { get; set; }
        public string addressLine2 { get; set; }
        public string addressLine1 { get; set; }
        public string ModifiedOn { get; set; }

    }
    public class AddressValidationModel
    {
        [JsonProperty(PropertyName = "addressLine1")]
        public string addressLine1 { get; set; }

        [JsonProperty(PropertyName = "addressLine2")]
        public string addressLine2 { get; set; }

        [JsonProperty(PropertyName = "addressLine3")]
        public string addressLine3 { get; set; }

        [JsonProperty(PropertyName = "addressLine4")]
        public string addressLine4 { get; set; }

        [JsonProperty(PropertyName = "city")]
        public string city { get; set; }

        [JsonProperty(PropertyName = "stateCode")]
        public string stateCode { get; set; }

        [JsonProperty(PropertyName = "stateName")]
        public string stateName { get; set; }

        [JsonProperty(PropertyName = "countryName")]
        public string countryName { get; set; }

        [JsonProperty(PropertyName = "postalCode")]
        public string postalCode { get; set; }

        [JsonProperty(PropertyName = "countryCode")]
        public string countryCode { get; set; }

        [JsonProperty(PropertyName = "validFlag")]
        public string validFlag { get; set; }

        [JsonProperty(PropertyName = "srcSystem")]
        public string srcSystem { get; set; }

    }

    public class BillToShipToModel
    {
        public AddressAttribute ext_BillToLocation { get; set; }
        public AddressAttribute ext_ShipToAddress { get; set; }
    }

    public class AddressAttribute
    {
        public string Id { get; set; }

        [JsonProperty(PropertyName = "ExternalId")]
        public string ExternalId { get; set; }

        [JsonProperty(PropertyName = "Name")]
        public string orderAccountName { get; set; }

        [JsonProperty(PropertyName = "ext_AddressLine1")]
        public string addressLine1 { get; set; }

        [JsonProperty(PropertyName = "ext_AddressLine2")]
        public string addressLine2 { get; set; }

        [JsonProperty(PropertyName = "ModifiedOn")]
        public string lastUpdatedDateTime { get; set; }
        
        public string City { get; set; }
        public string State { get; set; }
        public string Country { get; set; }
        public string PostalCode { get; set; }

        //[JsonProperty(PropertyName = "Id")]
        //public string sourceAddressId { get; set; }

        [JsonProperty(PropertyName = "ext_PrimaryBillTo")]
        public bool? primaryBillTo { get; set; }

        [JsonProperty(PropertyName = "ext_PrimaryShipTo")]
        public bool? primaryShipTo { get; set; }

        public string useOriginalIndicator { get; set; }

        public string vatRegistrationNumber { get; set; }
    }
}
