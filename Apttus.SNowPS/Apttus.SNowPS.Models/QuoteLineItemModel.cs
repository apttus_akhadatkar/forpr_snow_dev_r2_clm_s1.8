﻿/*************************************************************
@Name: QuoteLineItemModel.cs
@Author: Bhavinkumar Mistry    
@CreateDate: 08-Nov-2017
@Description: This class contains classes & properties for 'QuoteLineItem' used for Sales and Contracts
@UsedBy: Description of how this class is being used
******************************************************************
@ModifiedBy: Author who modified this class
@ModifiedDate: Date the class was modified
@ChangeDescription: A brief description of what was modified
 
 
**** PS: @ModifiedBy and @ChangeDescription does not apply to ‘In Development’ code – should be used for major changes in functionality or subsequent releases.
******************************************************************/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Apttus.SNowPS.Model
{
    public class QuoteLineItemModel
    {
        public string Id { get; set; }
        public string Name { get; set; }

        public LookUp ProductId { get; set; }

        public LookUp QuoteId { get; set; }

        public LookUp AssetId { get; set; }

        public LookUp AssetLineItemId { get; set; }

        public LookUp AttributeValueId { get; set; }

        public SelectOption EXT_Quotetype { get; set; }

        public DateTime ServiceDate { get; set; }

        public int Quantity { get; set; }

        public string Description { get; set; }

        public SelectOption ext_Type { get; set; }

        public string ext_Release { get; set; }

    }
}
