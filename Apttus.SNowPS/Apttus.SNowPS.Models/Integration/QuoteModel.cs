﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Apttus.SNowPS.Model.Integration
{
	public class QuoteModel
	{
		[JsonProperty(PropertyName = "id")]
		public string Id { get; set; }

		[JsonProperty(PropertyName = "opportunityID")]
		public string OpportunitySysId { get; set; }

		[JsonProperty(PropertyName = "number")]
		public string Name { get; set; }

		[JsonProperty(PropertyName = "name")]
		public string QuoteName { get; set; }

		[JsonProperty(PropertyName = "accountMdmID")]
		public string AccountMDMID { get; set; }

		[JsonProperty(PropertyName = "type")]
		public string Type { get; set; }

		[JsonProperty(PropertyName = "contractType")]
		public string ContractType { get; set; }

		[JsonProperty(PropertyName = "startDate")]
		public DateTime? ExpectedStartDate { get; set; }

		[JsonProperty(PropertyName = "endDate")]
		public DateTime? ExpectedEndDate { get; set; }

		[JsonProperty(PropertyName = "term")]
		public int? Term { get; set; }

		[JsonProperty(PropertyName = "flags")]
		public string FlaggedAs { get; set; }

		[JsonProperty(PropertyName = "isPrimary")]
		public bool? IsPrimary { get; set; }

		[JsonProperty(PropertyName = "salesType")]
		public string salestype { get; set; }

		[JsonProperty(PropertyName = "partnerAccountID")]
		public string PartnerAccount { get; set; }

		[JsonProperty(PropertyName = "partnershipType")]
		public string PartnershipType { get; set; }

		[JsonProperty(PropertyName = "approvalStatus")]
		public string ApprovalStatus { get; set; }

		[JsonProperty(PropertyName = "stage")]
		public string ApprovalStage { get; set; }

		[JsonProperty(PropertyName = "netNewACV")]
		public decimal? NNACV { get; set; }

		[JsonProperty(PropertyName = "netNewACVInUSD")]
		public decimal? USDTotalNetNewACV { get; set; }

		[JsonProperty(PropertyName = "acv")]
		public decimal? ACV { get; set; }

		[JsonProperty(PropertyName = "acvInUSD")]
		public decimal? USDACV { get; set; }

		[JsonProperty(PropertyName = "totalAgreementValue")]
		public decimal? TCV { get; set; }

		[JsonProperty(PropertyName = "totalAgreementValueInUSD")]
		public decimal? USDTCV { get; set; }

		[JsonProperty(PropertyName = "renewalACV")]
		public decimal? RenewalACV { get; set; }

		[JsonProperty(PropertyName = "renewalACVInUSD")]
		public decimal? USDRenewalACV { get; set; }

		[JsonProperty(PropertyName = "baseCurrency")]
		public string BaseCurrency { get; set; }

		[JsonProperty(PropertyName = "partnerAgreementNumber")]
		public string PartnerAgreementNumber { get; set; }

		[JsonProperty(PropertyName = "endCustomerMDMID")]
		public string EndCustomerMDMSysID { get; set; }

		[JsonProperty(PropertyName = "isAssociatedPartner")]
		public string DealRegAssociated { get; set; }

		[JsonProperty(PropertyName = "territoryMdmID")]
		public string FieldTerritoryMDMID { get; set; }

		[JsonProperty(PropertyName = "psTerritoryMdmID")]
		public string PSTerritoryMDMID { get; set; }

		[JsonProperty(PropertyName = "quoteOwnerID")]
		public string QuoteOwner { get; set; }

		[JsonProperty(PropertyName = "opportunityLinkReason")]
		public string OpportunityLinkReason { get; set; }

		[JsonProperty(PropertyName = "lineItems")]
		public QLIGenericGroup QLI { get; set; }

		[JsonProperty(PropertyName = "milestones")]
		public List<Milestone> MS { get; set; }
	}

	public class QLIGenericGroup
	{
		public List<QuoteLineItem> resourceLines { get; set; }
		public List<QuoteLineItem> lines { get; set; }
	}


	public class QuoteLineItem
	{
		[JsonProperty(PropertyName = "id")]
		public string Id { get; set; }

		[JsonProperty(PropertyName = "name")]
		public string Name { get; set; }

		[JsonProperty(PropertyName = "quoteID")]
		public string QuoteId { get; set; }

		[JsonProperty(PropertyName = "productID")]
		public string ProductId { get; set; }

		[JsonProperty(PropertyName = "dailyExchangeRate")]
		public decimal? FXRate { get; set; }

		[JsonProperty(PropertyName = "dailyExchangeRateInUSD")]
		public decimal? FXRateDailyUSD { get; set; }

		[JsonProperty(PropertyName = "listPrice")]
		public decimal? ListPrice { get; set; }

		[JsonProperty(PropertyName = "number")]
		public int? PrimaryLineNumber { get; set; }

		[JsonProperty(PropertyName = "itemSequenceNumber")]
		public string ItemSequenceNumber { get; set; }

		[JsonProperty(PropertyName = "parentBundleID")]
		public int? ParentBundleNumber { get; set; }

		[JsonProperty(PropertyName = "productHours")]
		public int? ProductHours { get; set; }

		[JsonProperty(PropertyName = "doNotCalculateNetNewACV")]
		public bool? DoNotCalculateNNACV { get; set; }

		[JsonProperty(PropertyName = "variablePriceACVInUSD")]
		public decimal? USDVariablePriceACVCalc { get; set; }

		[JsonProperty(PropertyName = "salesPrice")]
		public decimal? SalesPrice { get; set; }

		[JsonProperty(PropertyName = "flags")]
		public string Flags { get; set; }

		[JsonProperty(PropertyName = "variablePriceACVCalculate")]
		public decimal? VariablePriceACVCalc { get; set; }

		[JsonProperty(PropertyName = "baseAmount")]
		public decimal? PriceAdjustment { get; set; }

		[JsonProperty(PropertyName = "renewalLineACV")]
		public decimal? RenewalLineACV { get; set; }

		[JsonProperty(PropertyName = "startDate")]
		public DateTime? StartDate { get; set; }

		[JsonProperty(PropertyName = "endDate")]
		public DateTime? EndDate { get; set; }

		[JsonProperty(PropertyName = "term")]
		public decimal? SellingTerm { get; set; }

		[JsonProperty(PropertyName = "summaryLineACVInUSD")]
		public decimal? USDSummaryLineACV { get; set; }

		[JsonProperty(PropertyName = "netNewACVInUSD")]
		public decimal? USDNetNewACV { get; set; }

		[JsonProperty(PropertyName = "quantity")]
		public decimal? Quantity { get; set; }

		[JsonProperty(PropertyName = "netNewACV")]
		public decimal? NetNewACV { get; set; }

		[JsonProperty(PropertyName = "totalValue")]
		public decimal? ExtendedPrice { get; set; }

		[JsonProperty(PropertyName = "listPriceInUSD")]
		public decimal? USDListPrice { get; set; }

		[JsonProperty(PropertyName = "basePriceInUSD")]
		public decimal? USDBasePrice { get; set; }
		public decimal? pricingExchangeRate { get; set; }

		[JsonProperty(PropertyName = "totalValueInUSD")]
		public decimal? USDTotalValue { get; set; }

		[JsonProperty(PropertyName = "salesPriceInUSD")]
		public decimal? USDSalesPrice { get; set; }

		[JsonProperty(PropertyName = "annualListPriceInUSD")]
		public decimal? AnnualListPrice { get; set; }

		[JsonProperty(PropertyName = "annualListPrice")]
		public decimal? annualListPrice { get; set; }

		[JsonProperty(PropertyName = "annualContractAmountInUSD")]
		public decimal? USDAnnualContractValue { get; set; }

		[JsonProperty(PropertyName = "annualContractValue")]
		public decimal? AnnualContractValue { get; set; }

		[JsonProperty(PropertyName = "allocationPercentage")]
		public Decimal? AllocationPercent { get; set; }

		[JsonProperty(PropertyName = "deployment")]
		public string Deployment { get; set; }

		[JsonProperty(PropertyName = "resourcePlan")]
		public string ResourcePlan { get; set; }

		[JsonProperty(PropertyName = "taskID")]
		public string TaskID { get; set; }

		[JsonProperty(PropertyName = "practice")]
		public string Practice { get; set; }

		[JsonProperty(PropertyName = "status")]
		public string LineStatus { get; set; }

		[JsonProperty(PropertyName = "type")]
		public string LineType { get; set; }

		[JsonProperty(PropertyName = "application")]
		public string Application { get; set; }

		[JsonProperty(PropertyName = "revRecType")]
		public string RevRecType { get; set; }

		[JsonProperty(PropertyName = "netNewQuantity")]
		public decimal? NetNewQuantity { get; set; }

		[JsonProperty(PropertyName = "deltaQuantity")]
		public decimal? DeltaQuantity { get; set; }

		[JsonIgnore]
		public string RoleGeneric { get; set; }

	}
	public class Milestone
	{
		[JsonProperty(PropertyName = "id")]
		public string Id { get; set; }

		[JsonProperty(PropertyName = "amount")]
		public decimal? Amount { get; set; }

		[JsonProperty(PropertyName = "calculationMethod")]
		public string CalculationMethod { get; set; }

		[JsonProperty(PropertyName = "estimatedDueDate")]
		public DateTime? EstimatedDueDate { get; set; }

		[JsonProperty(PropertyName = "description")]
		public string ExtMilestone { get; set; }

		[JsonProperty(PropertyName = "paymentOrderNumber")]
		public int? PaymentOrder { get; set; }

		[JsonProperty(PropertyName = "percentage")]
		public decimal? Percent { get; set; }

		[JsonProperty(PropertyName = "estimatedHours")]
		public int? EstimatedHours { get; set; }

		[JsonProperty(PropertyName = "agreementID")]
		public string AgreementId { get; set; }
	}

    #region Renewal Opportunity Models
    public class RenewalOpportunityModel
    {

        [JsonProperty(PropertyName = "ext_AccountMDMID")]
        public string mdmID { get; set; }

        [JsonProperty(PropertyName = "ExpectedStartDate")]
        public string startDate { get; set; }

        [JsonProperty(PropertyName = "ExpectedEndDate")]
        public string endDate { get; set; }

        [JsonProperty(PropertyName = "ext_Term")]
        public int? term { get; set; }

        [JsonProperty(PropertyName = "ext_Type")]
        public SelectOption type { get; set; }

        [JsonProperty(PropertyName = "ext_ContractType")]
        public SelectOption agreementType { get; set; }

        [JsonProperty(PropertyName = "ext_BaseCurrency")]
        public string baseCurrency { get; set; }

        [JsonProperty(PropertyName = "MasterAgreementId")]
        public LookUp agreementID { get; set; }

        //[JsonProperty(PropertyName = "DeltaQuantity")]
        public DateTime? agreementEndDate { get; set; }

        [JsonProperty(PropertyName = "ext_RenewalACV")]
        public decimal? renewalACV { get; set; }

        [JsonProperty(PropertyName = "ext_USDRenewalACV")]
        public decimal? renewalACVInUSD { get; set; }
    }
    public class RenewalOpportunityMuleModel
    {
        public string mdmID { get; set; }
        public string startDate { get; set; }
        public string endDate { get; set; }
        public string term { get; set; }
        public string type { get; set; }
        public string agreementType { get; set; }
        public string baseCurrency { get; set; }
        public string agreementID { get; set; }
        public string agreementEndDate { get; set; }
        public string renewalACV { get; set; }
        public string renewalACVInUSD { get; set; }
    }

    public class RenewalOpportunityMuleResponseModel
    {
        [JsonProperty(PropertyName = "id")]
        public string ext_opportunitySYSId { get; set; }
        [JsonProperty(PropertyName = "number")]
        public string ext_opportunityNumber { get; set; }
        [JsonProperty(PropertyName = "name")]
        public string ext_opportunityName { get; set; }
        [JsonProperty(PropertyName = "approverTerritory")]
        //public string ext_Fieldterritory { get; set; }
        public string ext_FieldTerritoryMDMID { get; set; }
        [JsonProperty(PropertyName = "psTerritory")]

        public string ext_PSTerritoryMDMID { get; set; }
        //public string ext_PSterritory { get; set; }
        [JsonProperty(PropertyName = "quoteOwnerID")]
        public string ext_QuoteOwner { get; set; }
        [JsonProperty(PropertyName = "educationISRID")]
        //public string ext_EducationUser { get; set; }
        public string ext_Education { get; set; }
    }
    #endregion

    /***********************  Response Quote from Apptus *******************************/
    public class ResponseQuote
	{
		public string Id { get; set; }

		[JsonProperty(PropertyName = "ext_OpportunitySysId")]
		public string OpportunitySysId { get; set; }
		public string Name { get; set; }
		public string QuoteName { get; set; }

		[JsonProperty(PropertyName = "ext_AccountMDMID")]
		public string AccountMDMID { get; set; }

		[JsonProperty(PropertyName = "ext_Type")]
		public ChoiceType Type { get; set; }

		[JsonProperty(PropertyName = "ext_ContractType")]
		public ChoiceType ContractType { get; set; }

		public DateTime? ExpectedStartDate { get; set; }
		public DateTime? ExpectedEndDate { get; set; }

		[JsonProperty(PropertyName = "ext_Term")]
		public int? Term { get; set; }

		[JsonProperty(PropertyName = "ext_FlaggedAs")]
		public string FlaggedAs { get; set; }

		public bool? IsPrimary { get; set; }

		[JsonProperty(PropertyName = "ext_salestype")]
		public ChoiceType SalesType { get; set; }

		[JsonProperty(PropertyName = "ext_PartnerAccount")]
		public LookUp PartnerAccount { get; set; }

		[JsonProperty(PropertyName = "ext_partnershiptype")]
		public ChoiceType PartnershipType { get; set; }
		public ChoiceType ApprovalStage { get; set; }
		public ChoiceType ApprovalStatus { get; set; }

		[JsonProperty(PropertyName = "ext_NNACV")]
		public decimal? NNACV { get; set; }

		[JsonProperty(PropertyName = "ext_USDTotalNetNewACV")]
		public decimal? USDTotalNetNewACV { get; set; }

		[JsonProperty(PropertyName = "ext_ACV")]
		public decimal? ACV { get; set; }

		[JsonProperty(PropertyName = "ext_USDACV")]
		public decimal? USDACV { get; set; }

		[JsonProperty(PropertyName = "ext_TCV")]
		public decimal? TCV { get; set; }

		[JsonProperty(PropertyName = "ext_USDTCV")]
		public decimal? USDTCV { get; set; }

		[JsonProperty(PropertyName = "ext_RenewalACV")]
		public decimal? RenewalACV { get; set; }

		[JsonProperty(PropertyName = "ext_USDRenewalACV")]
		public decimal? USDRenewalACV { get; set; }

		[JsonProperty(PropertyName = "ext_BaseCurrency")]
		public string BaseCurrency { get; set; }

		[JsonProperty(PropertyName = "ext_PartnerAgreementNumber")]
		public string PartnerAgreementNumber { get; set; }

		[JsonProperty(PropertyName = "ext_EndCustomerMDMSysID")]
		public string EndCustomerMDMSysID { get; set; }

		[JsonProperty(PropertyName = "ext_DealRegAssociated")]
		public string DealRegAssociated { get; set; }

		[JsonProperty(PropertyName = "ext_FieldTerritoryMDMID")]
		public string FieldTerritoryMDMID { get; set; }

		[JsonProperty(PropertyName = "ext_PSTerritoryMDMID")]
		public string PSTerritoryMDMID { get; set; }

		[JsonProperty(PropertyName = "ext_QuoteOwner")]
		public string QuoteOwner { get; set; }

		[JsonProperty(PropertyName = "ext_OpportunityLinkReason")]
		public ChoiceType OpportunityLinkReason { get; set; }
		public QuoteLineItemResponse QLI { get; set; }
		public MilestoneResponse MS { get; set; }
	}

	public class QuoteLineItemResponse
	{
		public string Id { get; set; }
		public LookUp QuoteId { get; set; }
		public ProductRevRecGroup ProductId { get; set; }
		public ProductRevRecGroup OptionId { get; set; }  //Revert after 1.5 Sprint

		[JsonProperty(PropertyName = "ext_FXRate")]
		public decimal? FXRate { get; set; }

		[JsonProperty(PropertyName = "ext_FXRateDailyUSD")]
		public decimal? FXRateDailyUSD { get; set; }
		public decimal? ListPrice { get; set; }

		[JsonProperty(PropertyName = "PrimaryLineNumber")]
		public int? PrimaryLineNumber { get; set; }

		[JsonProperty(PropertyName = "ext_ItemSequenceNumber")]
		public string ItemSequenceNumber { get; set; }
		public int? ParentBundleNumber { get; set; }

		[JsonProperty(PropertyName = "ext_ProductHours")]
		public int? ProductHours { get; set; }

		[JsonProperty(PropertyName = "ext_DoNotCalculateNNACV")]
		public bool? DoNotCalculateNNACV { get; set; }

		[JsonProperty(PropertyName = "ext_USDVariablePriceACVCalc")]
		public decimal? USDVariablePriceACVCalc { get; set; }

		[JsonProperty(PropertyName = "ext_SalesPrice")]
		public decimal? SalesPrice { get; set; }

		[JsonProperty(PropertyName = "ext_VariablePriceACVCalc")]
		public decimal? VariablePriceACVCalc { get; set; }
		//public decimal? PriceAdjustment { get; set; }

		[JsonProperty(PropertyName = "ext_ListPriceWithAdjustments")]
		public decimal? ListPriceWithAdjustments { get; set; }

		[JsonProperty(PropertyName = "ext_RenewalLineACV")]
		public decimal? RenewalLineACV { get; set; }
		public DateTime? StartDate { get; set; }
		public DateTime? EndDate { get; set; }
		public int? Term { get; set; }
		public decimal? SellingTerm { get; set; }

		[JsonProperty(PropertyName = "ext_USDSummaryLineACV")]
		public decimal? USDSummaryLineACV { get; set; }

		[JsonProperty(PropertyName = "ext_USDNetNewACV")]
		public decimal? USDNetNewACV { get; set; }

		[JsonProperty(PropertyName = "Quantity2")]
		public decimal? Quantity { get; set; }

		[JsonProperty(PropertyName = "ext_NetNewACV")]
		public decimal? NetNewACV { get; set; }
		public decimal? ExtendedPrice { get; set; }

		[JsonProperty(PropertyName = "ext_USDListPrice")]
		public decimal? USDListPrice { get; set; }

		[JsonProperty(PropertyName = "ext_USDBasePrice")]
		public decimal? USDBasePrice { get; set; }

		[JsonProperty(PropertyName = "ext_USDTotalValue")]
		public decimal? USDTotalValue { get; set; }

		[JsonProperty(PropertyName = "ext_USDSalesPrice")]
		public decimal? USDSalesPrice { get; set; }

		[JsonProperty(PropertyName = "ext_AnnualListPrice")]
		public decimal? AnnualListPrice { get; set; }

		[JsonProperty(PropertyName = "ext_USDAnnualContractValue")]
		public decimal? USDAnnualContractValue { get; set; }

		[JsonProperty(PropertyName = "ext_AnnualContractValue")]
		public decimal? AnnualContractValue { get; set; }

		[JsonProperty(PropertyName = "ext_AllocationPC")]
		public Decimal? AllocationPercent { get; set; }

		[JsonProperty(PropertyName = "ext_Deployment")]
		public string Deployment { get; set; }

		[JsonProperty(PropertyName = "ext_ResourcePlan")]
		public string ResourcePlan { get; set; }

		[JsonProperty(PropertyName = "ext_TaskID")]
		public string TaskID { get; set; }

		[JsonProperty(PropertyName = "ext_Practice")]
		public string Practice { get; set; }

		[JsonProperty(PropertyName = "ext_ApplicationPS")]
		public string Application { get; set; }

		[JsonProperty(PropertyName = "ext_DefaultRevRec")]
		public string RevRecType { get; set; }
		public ChoiceType LineStatus { get; set; }
		public ChoiceType LineType { get; set; }

		[JsonProperty(PropertyName = "ext_NetNewQuantity")]
		public decimal? NetNewQuantity { get; set; }

		[JsonProperty(PropertyName = "DeltaQuantity")]
		public decimal? DeltaQuantity { get; set; }
	}
	public class MilestoneResponse
	{
		[JsonProperty(PropertyName = "Id")]
		public string Id { get; set; }
		public string Name { get; set; }

		[JsonProperty(PropertyName = "ext_QuoteId")]
		public LookUp QuoteId { get; set; }

		[JsonProperty(PropertyName = "ext_Amount")]
		public decimal? Amount { get; set; }

		[JsonProperty(PropertyName = "ext_CalculationMethod")]
		public ChoiceType CalculationMethod { get; set; }

		[JsonProperty(PropertyName = "ext_EstimatedDueDate")]
		public DateTime? EstimatedDueDate { get; set; }

		[JsonProperty(PropertyName = "ext_Milestone")]
		public string Milestone { get; set; }

		[JsonProperty(PropertyName = "ext_PaymentOrder")]
		public int? PaymentOrder { get; set; }

		[JsonProperty(PropertyName = "ext_Percent")]
		public decimal? Percent { get; set; }

		[JsonProperty(PropertyName = "ext_Number")]
		public string Number { get; set; }

		[JsonProperty(PropertyName = "EstimatedHours")]
		public int? ext_EstimatedHours { get; set; }

		[JsonProperty(PropertyName = "ext_AgreementId")]
		public LookUp AgreementId { get; set; }
	}
	public class ChoiceType
	{
		public string Key { get; set; }
		public string Value { get; set; }
	}
	public class LookUp
	{
		public string Id { get; set; }
		public string Name { get; set; }
	}

	public class ProductRevRecGroup
	{
		public string Id { get; set; }
		public string Name { get; set; }

		[JsonProperty(PropertyName = "ext_DefaultRevRec")]
		public ChoiceType DefaultRevRec { get; set; }

		[JsonProperty(PropertyName = "ext_RoleGeneric")]
		public ChoiceType RoleGeneric { get; set; }
	}
}
