﻿/****************************************************************************************
@Name: NoteModel.cs
@Author: Akash Kakadiya
@CreateDate: 18 Oct 2017
@Description:  
@UsedBy: This will be used by notes in agreement

@ModifiedBy: 
@ModifiedDate: 
@ChangeDescription: 
*****************************************************************************************/

namespace Apttus.SNowPS.Model
{
    public  class NoteModel
    {
        public string Name { get; set; }
        public string NoteText { get; set; }
        public ContextObject ContextObject { get; set; }

    }
    public class ContextObject
    {
        public string Id { get; set; }
        public string Type { get; set; }
    }
}
