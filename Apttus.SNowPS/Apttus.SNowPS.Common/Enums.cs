﻿/*************************************************************
@Name: Enum.cs
@Author: Chirag Modi
@CreateDate: 19-Sep-2017
@Description: This class contains all Enums used in Solution.
@UsedBy: Description of how this class is being used
******************************************************************
@ModifiedBy: Author who modified this class
@ModifiedDate: Date the class was modified
@ChangeDescription: A brief description of what was modified
 
 
**** PS: @ModifiedBy and @ChangeDescription does not apply to ‘In Development’ code – should be used for major changes in functionality or subsequent releases.
******************************************************************/

using System;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Reflection;

namespace Apttus.SNowPS.Common
{
    public static class Enums
    {
        /// <summary>
        /// This Enum is being used to Identify FlaggedAs Scenarios.
        /// </summary>
        // ToDo Update Enum Display Names as per new Multi picklist Falgged As values 
        public enum ScenarioType
        {
            None,
            Upsell,
            Renewal,
            EarlyRenewal,
            [Display(Name = "First Time Purchase")]
            FirstTimePurchase,
            BuildInUpsell,
            HasPriceReset,
            Superseding,
            CreditAndReplace,
            HasReplacement,
            [Display(Name = "Specific Data Center")]
            SpecificDataCenter
        }

        public enum QuoteType
        {
            [Display(Name ="new_business")]
            NewBusiness,
            [Display(Name = "Renewal")]
            Renewal,
            [Display(Name ="License Upsell")]
            LicenseUpsell
        }
        public enum SpecificDataCenter
        {
            Brazil,
            Netherlands,
            London,
            Canada,
            [Display(Name = "United States")]
            UnitedStates,
            EMEA
        }

        public enum PriceType
        {
            OneTime,
            Recurring
        }
        /// <summary>
        /// This method is used to get Enum Display Name by Enum Value
        /// </summary>
        /// <param name="enumType">Type of Enum</param>
        /// <returns>string: Enum Display Name </returns>
        public static string GetEnumDisplayName(this Enum enumType)
        {
            var attribute = enumType.GetType().GetMember(enumType.ToString())
                           .First()
                           .GetCustomAttribute<DisplayAttribute>();
            return (attribute == null) ? enumType.ConvertToString() : attribute.Name;
        }

        public enum SearchOperators
        {
            In,
            Equal
        }
    }
}
