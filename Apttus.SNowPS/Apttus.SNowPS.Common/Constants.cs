﻿/****************************************************************************************
@Name: Constants.cs
@Author: Mahesh Patel,Meera Kant, Kiran, Akash, Varun
@CreateDate: 1 Sep 2017
@Description: Defines Constants 
@UsedBy: This will be used by Mule as an API call

@ModifiedBy: 
@ModifiedDate: 
@ChangeDescription: 
*****************************************************************************************/

namespace Apttus.SNowPS.Common
{
    public static class Constants
    {
        //Fields
        public const string FIELD_EXTERNALID = "ExternalId";
        public const string FIELD_PARENTID = "ParentId";

        public const string FIELD_ACCOUNTID = "AccountId";
        public const string FIELD_EXT_PARENTCUSTOMERMDMID = "ext_ParentCustomerMDMID";
        public const string FIELD_PARENTCUSTOMERID = "ParentCustomerID";
        public const string FIELD_EXT_TERRITORYID = "ext_TerritoryId";
        public const string FIELD_EXT_USERID = "ext_UserId";
        public const string FIELD_EXT_ROLEID = "ext_RoleId";
        public const string FIELD_EXT_APPROVALTYPE = "ext_ApprovalType";
        public const string FIELD_EXT_JOBFUNCTION = "ext_JobFunction";
        public const string FIELD_EXT_JOBFUNCTIONID = "ext_JobFunctionId";
        public const string FIELD_EXT_ISACTIVE = "ext_IsActive";
        public const string FIELD_EXT_MDMPARENTID = "ext_MDMParentId";
        public const string FIELD_EXT_PARENTTERRITORYID = "ext_ParentTerritoryId";
        public const string FIELD_USERGROUPID = "UserGroupId";
        public const string FIELD_USER_ID = "UserId";
        public const string FIELD_TERRITORYEXTID = "TerritoryExtId";

        public const string FIELD_EXT_PARTNERACCOUNT = "ext_PartnerAccount";
        public const string FIELD_EXT_ENDCUSTOMERMDMID = "ext_EndCustomerMDMID";
        public const string FIELD_EXT_MSPACCOUNT = "ext_MSPAccount";
        public const string FIELD_EXT_FIELDTERRITORY = "ext_FieldTerritory";
        //public const string FIELD_EXT_MSPTERRITORY = "ext_MSPTerritory";
        public const string FIELD_EXT_PSTERRITORY = "ext_PSTerritory";
        public const string FIELD_EXT_EDUCATIONUSER = "ext_EducationUser";
        public const string FIELD_EXT_QUOTEOWNERUSER = "ext_QuoteOwnerUser";
        public const string FIELD_EXT_QUOTEORIGINATORUSER = "ext_QuoteOriginatorUser";
        public const string FIELD_EXT_ACCOUNTMDMID = "ext_AccountMDMId";

        public const string FIELD_STR_EXT_PARTNERACCOUNT = "ext_PartnerAccountMDMID";
        public const string FIELD_STR_EXT_ENDCUSTOMERMDMID = "ext_EndCustomerMDMSysID";
        public const string FIELD_STR_EXT_MSPACCOUNT = "ext_MSPAccountMDMID";
        public const string FIELD_STR_EXT_FIELDTERRITORY = "ext_FieldTerritoryMDMID";
        public const string FIELD_STR_EXT_PSTERRITORY = "ext_PSTerritoryMDMID";
        public const string FIELD_STR_EXT_EDUCATIONUSER = "ext_Education";
        public const string FIELD_STR_EXT_QUOTEOWNERUSER = "ext_QuoteOwner";
        public const string FIELD_STR_EXT_QUOTEORIGINATORUSER = "ext_QuoteOriginator";

        public const string FIELD_COMPONENTPRODUCTID = "ComponentProductId";
        public const string FIELD_EXT_LKP = "ext_LKP";
		public const string FIELD_EXT_AUTOMATICALLYONOFF = "ext_AutomaticallyOnOff";

		public const string FIELD_PRODUCTID = "ProductId";
        public const string FIELD_ATTRIBUTEGROUPID = "AttributeGroupId";
        public const string FIELD_VALUEEXPRESSION = "ValueExpression";
        public const string FIELD_ACTION = "Action";
        public const string FIELD_FIELD = "Field";
        public const string FIELD_PRODUCTATTRIBUTERULEID = "productAttributeRuleId";
        public const string FIELD_TYPE = "Type";
        public const string CUSTOM_SYNC_BOCLASSCODE = "Organization";
        public const string CUSTOM_SYNC_SOURCESYSTEM = "APTTUS";
        public const string FIELD_ADDRESSTYPECODE = "PRMRY_ADDR";
        public const string FIELD_ADDRESSEVENTTYPE = "ADDR_SYNC";
        public const string FIELD_ADDRESSCREATEEVENTTYPE = "ADDR_CREATE";
        public const string FIELD_ADDRESSUPDATEEVENTTYPE = "ADDR_UPDATE";
        public const string FIELD_ACCOUNTCREATEEVENTTYPE = "ACCT_CREATE";
        public const string FIELD_ACCOUNTUPDATEEVENTTYPE = "ACCT_UPDATE";
        public const string FIELD_ADDRESSSENDTOSAPEVENTTYPE = "SEND_TO_SAP";
        public const string FIELD_UPDATECUSTOMEREVENTTYPE = "CUSTOMER_UPDT";
        public const string FIELD_DISPLAYNAME = "DisplayName";
        public const string FIELD_EXT_MANAGERSYSID = "ext_ManagerSysId";
        public const string FIELD_CURRENCYCODE = "ext_CurrencyCode";

        public const string FIELD_BOCLASSCODE = "Organization";
        public const string FIELD_EVENTTYPE = "eventType";
        //public const string FEILD_AGREMENTNOTE = "Description";
        public const string FEILD_AGREMENTNOTE = "NoteText";
        public const string FIELD_AGREEMENTTYPE = "AgreementType";
        public const string FIELD_RECORDTYPENAME = "Name";
        public const string FIELD_RECORDTYPEID = "RecordTypeId";
        public const string FIELD_USERID = "RequestorId";//ext_AgreementOriginatorUser
        public const string FIELD_TERRITORYMDMID = "TerritoryID";//ext_FieldTerritory
        public const string FIELD_MOAAGREEMENTTYPE = "MOA";
        public const string FIELD_WOAGREEMENTTYPE = "WO";
        public const string FIELD_COMPANYENTITY = "Name";
        public const string FIELD_SNENITITY = "ext_SellingEntity"; //ext_SNEntityID
        public const string FIELD_PARTNERACCOUNTMDMID = "ext_PartnerAccountMDMID"; //ext_PartnerMDMId
        public const string FIELD_PARTNERACCOUNTID = "ext_PartnerAccountID";
        public const string FIELD_CUSTOMERACCOUNNTID = "ext_EndCustomerMDMID"; //ext_CustomerMDMId
        public const string FIELD_AGREEMENTACCOUNTMDMID = "ext_AccountMDMID";
        public const string FIELD_ACCOUNTTYPEENTITY = "Entity";
        public const string FIELD_ACCOUNTFIELDTERRITOTYMDMID = "ext_FieldTerritoryMDMId";
        public const string FIELD_ACCOUNTPSTERRITOTYMDMID = "ext_PSTerritoryMDMId";
        public const string FIELD_ACCOUNTFIELDTERRITORYID = "ext_FieldTerritoryId";
        public const string FIELD_ACCOUNTPSTERRITORYID = "ext_PSTerritoryId";
        public const string FIELD_QUOTEOWNERUSERMDMID = "ext_QuoteOwner";
        public const string FIELD_QUOTEOWNERUSERID = "ext_QuoteOwnerUserID";
        public const string FIELD_CUSTOMERMDMID = "ext_CustomerMDMId";
        public const string FIELD_CUSTOMERID = "ext_CustomerId";
        public const string FIELD_ORGINATORUSERMDMID = "ext_OriginatorUserSysID";
        public const string FIELD_MODIFIEDON = "ModifiedOn";
        //public const string FIELD_PARTYTYPECD = "Order Account";

        public const string FIELD_EXT_ADDRESSLINE1 = "ext_AddressLine1";
        public const string FIELD_EXT_ADDRESSLINE2 = "ext_AddressLine2";
        public const string FIELD_EXT_CITY = "ext_City";
        public const string FIELD_EXT_STATE = "ext_State";
        public const string FIELD_EXT_COUNTRY = "ext_Country";
        public const string FIELD_EXT_POSTALCODE = "ext_PostalCode";
        public const string FIELD_EXT_PRIMARYADDRESSID = "ext_PrimaryAddressId";
        public const string FIELD_EXT_ISPRIMARY = "ext_IsPrimary";
        public const string FIELD_EXT_PRIMARYBILLTO = "ext_PrimaryBillTo";
        public const string FIELD_EXT_PRIMARYSHIPTO = "ext_PrimaryShipTo";
        public const string FIELD_CITY = "City";
        public const string FIELD_STATE = "State";
        public const string FIELD_COUNTRY = "Country";
        public const string FIELD_POSTALCODE = "PostalCode";
        public const string FIELD_NETPRICE = "NetPrice";
        public const string FIELD_EXT_FAMILY = "ext_Family";
        public const string FIELD_FAMILY = "Family";
        public const string FIELD_EXT_PRODUCTCATEGORY = "ext_ProductCategory";
        public const string FIELD_EXT_TAXTYPE = "ext_TaxType";
        public const string FIELD_EXT_TAXRATENEW = "ext_TaxRateNew";
        public const string FIELD_EXT_TAX = "ext_TaxNew";
        public const string FIELD_EXT_TAXPS = "ext_TaxPSNew";
        public const string FIELD_EXT_TAXEDUCATION = "ext_TaxEducationNew";
        public const string FIELD_EXT_TAXSUBSCRIPTION = "ext_TaxSubscriptionNew";
        public const string FIELD_EXT_INSTANCE_NAME = "ext_InstanceName";
        public const string FIELD_LINETYPE = "LineType";
        public const string FIELD_EXT_SOLDLINAMEINSTMAP = "ext_SoldLINameInstMap";
        public const string FIELD_EXT_INSTALLEDLINAMEINSTMAP = "ext_InstalledLINameInstMap";
        public const string FIELD_EXT_SOLDLINUMINSTMAP = "ext_SoldLINumInstMap";
        public const string FIELD_EXT_INSTALLEDLINUMINSTMAP = "ext_InstalledLINumInstMap";
        public const string FIELD_EXT_LINumInstMap = "ext_LINumInstMap";
        public const string FIELD_CONFIGURATIONID = "ConfigurationId";
        public const string FIELD_EXT_FAMILY1 = "ext_Family1";
        public const string FIELD_KEY = "Key";
        public const string FIELD_VALUE = "Value";
        public const string FIELD_EXT_TCV = "ext_TCV";
        public const string FIELD_EXT_SELLINGENTITY = "ext_sellingentity";
        public const string FIELD_EXT_CONTRIBUTESTO = "ext_ContributesTo";
        public const string FIELD_EXT_SHIPTOCOUNTRY = "ext_ShipToCountry";
        public const string FIELD_EXT_SHIPTOSTATE = "ext_ShipToState";
        public const string FIELD_EXT_TAXEXEMPT = "ext_taxexempt";
        public const string FIELD_EXT_TAXCALCULATION = "ext_TaxCalculation";
        public const string FIELD_EXT_SHIPTOLOCATION = "ext_ShipToLocation";

        public const string FIELD_EXT_ISBILLTO = "ext_IsBillTo";
        public const string FIELD_EXT_ISSHIPTO = "ext_IsShipTo";
        public const string FIELD_EXT_ACCOUNTID = "ext_AccountId";
        public const string FIELD_SENDTOSAP = "ext_SendToSAP";
        public const string FIELD_AGREEMENTID = "AgreementId";
        public const string FIELD_SHIPTOADDRESS = "ext_ShipToAddress";
        public const string FIELD_BILLTOADDRESS = "ext_BillToLocation";
        public const string FIELD_CUSTOMERSINCE = "ext_CustomerSince";
        public const string FIELD_OPPORTUNITYSYSID = "ext_OpportunitySysId";
        public const string FIELD_SALESMASTERAGREEMENTTYPE = "Sales – Master Agreement";
        public const string FIELD_CATEGORY = "ext_Category";
        public const string FIELD_STATUSCATEGORY = "StatusCategory";
        public const string FIELD_STATUSCATEGORYREQUEST = "100000000";
        public const string FIELD_STATUS = "Status";
        public const string FIELD_STATUSREQUEST = "100000000";
        public const string FIELD_CREATEDBYID = "CreatedById";
        public const string FIELD_CRM_USERID = "crm_UserId";
        public const string FIELD_CMN_ROLEID = "cmn_RoleId";
        public const string FIELD_ROLEID = "RoleId";
        public const string FIELD_ROLENAME = "roleName";
        public const string FIELD_CONTEXTOBJECT = "ContextObject";
        public const string FIELD_PARTICIPANT = "Participant";
        public const string FIELD_PARTICIPANTID = "ParticipantID";
        public const string FIELD_TIMEZONEID = "TimezoneId";
        //Common Fields
        public const string FIELD_NAME = "Name";
        public const string FIELD_ID = "Id";
        public const string FIELD_MULERESPONSEID = "id";
        public const string FIELD_CONSUMERID = "consumerID";
        public const string FIELD_MDMID = "MDMId";
        public const string FIELD_SENDTOSAPIND = "sendToSAPInd";
        public const string FIELD_DERIVEDFROMId = "DerivedFromId";
        public const string FIELD_SETISPRIMARY = "SetIsPrimary";
        public const string FIELD_BUSINESSLINEITEMID = "BusinessLineItemId";
        
        //Objects
        public const string OBJ_ACCOUNT = "crm_Account";
        public const string OBJ_CONTACT = "crm_Contact";
        public const string OBJ_ACCOUNTLOCATION = "cpq_AccountLocation";
        public const string OBJ_QUOTE = "cpq_Quote";
        public const string OBJ_ORDER = "cpq_Order";
        public const string OBJ_USER = "crm_User";
        public const string OBJ_ROLE = "cmn_Role";
        public const string OBJ_TERRITORY = "ext_Territory";
        public const string OBJ_PRODUCT = "crm_Product";
        public const string OBJ_JNC_EXT_TERRITORY_CRM_USER = "ext_jnc_ext_Territory_crm_User";
        public const string OBJ_CPQ_LINEITEM = "cpq_LineItem";
        public const string OBJ_CPQ_ASSETLINEITEM = "cpq_AssetLineItem";
        public const string OBJ_CPQ_QUOTELINEITEM = "cpq_QuoteLineItem";
        public const string OBJ_CPQ_ORDERLINEITEM = "cpq_OrderLineItem";
        public const string OBJ_EXT_MILESTONE = "ext_milestone";
        public const string OBJ_EXT_INVOICESCHEDULE = "ext_InvoiceSchedule";
        public const string OBJ_CUSTOMER = "ext_CustomerInstance";
        public const string OBJ_PRODUCTATTRIBUTEGROUP = "cpq_ProductAttributeGroup";
        public const string OBJ_PRODUCTATTRIBUTE = "cpq_ProductAttribute";
        public const string OBJ_PRODUCTOPTIONCOMPONENT = "cpq_ProductOptionComponent";
        public const string OBJ_SNPLUGINS = "ext_SNPlugins";
        public const string OBJ_PRODUCTATTRIBUTEVALUE = "cpq_ProductAttributeValue";
        public const string OBJ_PRODUCTATTRIBUTERULEACTION = "cpq_ProductAttributeRuleAction";
        public const string OBJ_PRODUCTATTRIBUTEGROUPMEMBER = "cpq_ProductAttributeGroupMember";
        public const string OBJ_PRICELISTITEM = "cpq_PriceListItem";
        public const string OBJ_PRICELIST = "cpq_PriceList";
        public const string OBJ_LINEITEM = "cpq_lineitem";
        public const string OBJ_DAILYEXCHANGERATE = "ext_dailyexchangerate";
        public const string OBJ_PRODUCTCONFIGURATION = "cpq_ProductConfiguration";
        public const string OBJALIAS_PRODUCTCONFIGURATION = "PC";
        public const string OBJALIAS_QUOTE = "Q";

        public const string OBJ_EXT_SALESTAXRATES = "ext_SalesTaxRates";
        public const string OBJ_EXT_CUSTOMERINSTANCE = "ext_CustomerInstance";
        public const string OBJ_AGREEMENT = "clm_Agreement";
        public const string OBJ_RECORDTYPE = "cmn_RecordType";
        public const string OBJ_ASSETLINEITEM = "cpq_AssetLineItem";
        public const string OBJ_NOTES = "cmn_Note";
        public const string OBJ_WOPARTICIPATE = "ext_WorkOrderParticipants";
        public const string OBJ_AGREEMENTDOCUMENT = "clm_AgreementDocument";
        public const string OBJ_AGREEMENTLINEITEM = "clm_AgreementLineItem";
        public const string OBJ_JNC_CMN_ROLE_CRM_USER = "jnc_cmn_Role_crm_User";
        public const string OBJ_USERGROUP = "crm_UserGroup";
        public const string OBJ_USERGROUPMEMBERSHIP = "crm_UserGroupMembership";
        public const string OBJ_PARTICIPANT = "cmn_Participant";
        public const string OBJ_PARTICIPANTROLE = "cmn_ParticipantRole";
        public const string OBJ_TIMEZONE = "cmn_Timezone";

        //Nodes
        public const string NODE_USERS = "Users";
        public const string NODE_ROLE = "Role";
        public const string NODE_APPROVALTYPE = "ApprovalType";
        public const string NODE_TYPE = "Type";
        public const string NODE_FIELDS = "Fields";
        public const string NODE_SELECTOPTIONSET = "SelectOptionSet";
        public const string NODE_SELECTOPTIONS = "SelectOptions";
        public const string NODE_OPTIONVALUE = "OptionValue";
        public const string NODE_RESULT = "result";
        public const string NODE_GENERICRULEDOS = "GenericRuleDOs";
        public const string NODE_SERIALIZEDRESULTENTITIES = "SerializedResultEntities";
        public const string NODE_ISACTIVE = "IsActive";
        public const string NODE_JOBFUNCTION = "JobFunction";

        //Config
        public const string CONFIG_UPSERTAPI = "UpsertAPI";
        public const string CONFIG_UPSERTAPIWOKey = "UpsertAPIWOKey";
        public const string CONFIG_CREATEAPI = "CreateAPI";
        public const string CONFIG_SEARCHAPI = "SearchAPI";
        public const string CONFIG_AQLSEARCHAPI = "AQLSearchAPI";
        public const string CONFIG_GETSEARCHAPI = "GetSearchAPI";
        public const string CONFIG_APPINSTANCE = "AppInstance";
        public const string CONFIG_APPURL = "AppUrl";
        public const string CONFIG_DELETEMULTIPLEAPI = "DeleteMultipleAPI";

        public const string CONFIG_MOCKCONTACTURL = "ContactMockUrl";
        public const string CONFIG_MOCKKEYCONTACTROLEAPI = "KeyContactRoleAPI";
        public const string CONFIG_MOCKAPIURL = "MockAPIUrl";
        public const string CONFIG_MULEADDRESSSEARCHURL = "MockAPIUrlAddressSearch";

        public const string CONFIG_MULEADDRESSVALIDATORTOKEN = "Mule_AddressValidationToken";
        public const string CONFIG_MULEXCLIENTID = "Mule_X-Client-ID";
        public const string CONFIG_MULEXSOURCESYSTEM = "Mule_X-SourceSystem";
        public const string CONFIG_MULEUSERNAME = "MuleUserName";
        public const string CONFIG_MULEPASSWORD = "MulePassword";
        public const string CONFIG_CPQADMINAPI = "CpqAdminAPI";
        public const string CONFIG_METADATAAPI = "MetadataAPI";
        public const string CONFIG_ADMINAPPURL = "AdminAppUrl";
        public const string CONFIG_ACCOUNTSYNCAPINAME = "Account_Sync_APIName";
        public const string CONFIG_ADDRESSVALIDATORAPINAME = "Address_Validator_APIName";
        public const string CONFIG_ADDRESSSEARCHAPINAME = "Address_Search_APIName";
        public const string CONFIG_ADDRESSSYNCAPINAME = "Address_Sync_APIName";
        public const string CONFIG_QUOTESYNCAPI = "QuoteSyncAPI";
        public const string CONFIG_ORDERSYNCAPI = "OrderSyncAPI";
        public const string CONFIG_ACCOUNTXBATCHID = "AccountXBatchID";
        public const string CONFIG_CONTACTUPDATEAPI = "ContactUpdateAPI";
        public const string CONFIG_CONTACTCREATEAPI = "ContactCreateAPI";
        public const string CONFIG_AGREEMENTSYNCAPI = "AgreementSyncAPI";
		public const string CONFIG_ORDERSYNCIAPI = "OrderSyncHiAPI";
		public const string CONFIG_OPPORTUNITYUPSERTAPI = "RenewalOpportunityUpsertAPI";
        public const string CONFIG_MULEXTARGETSYSTEM = "Mule_TargetSystem";
        public const string CONFIG_ASSETSYNCAPI = "AssetLineItemAPI";
        public const string CONFIG_ADDRESSSENDTOSAPAPI = "AddressSendToSAPAPI";

        public const string OBJ_LINEITEM_SELECTFIELD = "Id,Name,AdjustmentAmount,ListPrice,ProductId,Quantity,Term,NetPrice,AdjustedPrice,ext_AnnualContractValue,ext_AnnualListPrice,ext_EstimatedTotal,ext_SalesPrice,AdjustmentType,AdjustmentAmount,BillingFrequency,ext_TermNew,SellingFrequency,NetUnitPrice,BasePrice,ext_EstimatedTax,ConfigurationId,ext_FlaggedAs,CurrencyId,ext_USDAdjustment,DeltaQuantity,ExtendedPrice,ext_TCV,StartDate,EndDate,crm_Product.Name,crm_Product.ext_Family,crm_Product.Uom,crm_Product.ProductType,cpq_ProductConfiguration.QuoteId,cpq_Quote.ext_taxexempt";
        public const string OBJ_DAILYEXCHANGERATE_SELECTFIELD = "Name,ext_CurrencyCode,ExternalLastUpdatedOn,ModifiedOn,ext_CurrencySymbol,ext_Rate";

        //Object Select Fields
        public const string OBJ_CONTACT_SELECTFIELD = "Id,ext_Active,BusinessPhone,ext_DoNotEmail,ext_DoNotPhone,ext_JobTitle,Name,MobilePhone,ext_Salutation,FirstName,LastName,ExternalId,ParentCustomerID,EmailAddress1,ext_ParentCustomerMDMID,ext_KeyContactRole";

        public const string OBJ_ACCOUNT_SELECTFIELD = "Id,Name,ExternalId,Type,ext_AddressLine1,ext_AddressLine2,ext_City,ext_Country,ext_PostalCode,ext_State,ModifiedOn,ext_FieldTerritoryMDMId,ext_FieldTerritoryId,ext_PSTerritoryMDMId,ext_PSTerritoryId";//ext_PrimaryBillTo,ext_PrimaryShipTo
        public const string OBJ_ACCOUNTLOCATION_SELECTFIELD = "Id,ExternalId,Name,ext_AddressLine1,ext_AddressLine2,County,City,State,Country,PostalCode,ModifiedOn,AccountId,ext_PrimaryBillTo,ext_PrimaryShipTo,ext_MDMAddressOverriddenFlag";
        public const string OBJ_SENDTOSAP_ACCOUNTLOCATION = "Id,Name,ExternalId,ext_ShipToAddress,ext_BillToLocation,ext_ShipToAddress.ext_AddressLine1,ext_ShipToAddress.ext_AddressLine2,ext_ShipToAddress.City,ext_ShipToAddress.State,ext_ShipToAddress.Country,ext_ShipToAddress.PostalCode,ext_ShipToAddress.ModifiedOn,ext_BillToLocation.ext_AddressLine1,ext_BillToLocation.ext_AddressLine2,ext_BillToLocation.City,ext_BillToLocation.State,ext_BillToLocation.Country,ext_BillToLocation.PostalCode,ext_BillToLocation.ModifiedOn";
        public const string OBJ_PRODUCT_SELECTFIELD = "Id,Name,ConfigurationType,Description,ext_SKU,ProductCode,Family,ExpirationDate,EffectiveDate,Uom,ext_AppVersion,ext_BusinessUnit,ext_CalculateACV,ext_CalculatedACV,ext_ContributesTo,ext_DefaultRevRec,ext_ExcludeFromOpportunityFinancials,ext_ExcludeFromQuoteFinancials,ext_Family,ext_Family1,ext_Family2,ext_Family3,ext_FilteredOrderTypes,ext_FilteredOrdersTypes,ext_HasQuantityRestrictions,ext_LicenseProductType,ext_MaximumQuantity,ext_MinimumQuantity,ext_ProductApprovalStatus,ext_ProductFirstReleaseVersion,ext_ProductLastReleaseVersion,ext_LastReleasesVersion,ext_ProductsFirstReleaseVersion,ext_ProductOptionApplicationPercentageAllocation,ext_ProductLifecycleStatus,ext_RevRecGroup,ext_SKU,ext_SysId,ext_TaxRateType,ext_TechnologyPartnerEmail,ext_TechnologyPartnerName,ext_UAAttribute,ext_OrderFormOnly,ext_NeedDeploymentApplication,ext_EnableHIRSA,ext_RequiresSurvey,ext_DeploymentCategory,ext_RequiresGoLive,ext_Practice,ext_RequiresStatusReport,ext_RoleGeneric,Options.ParentProductId,Plugins.ext_PluginId,Plugins.ext_PluginName,Plugins.ext_PluginfirstReleaseVersion,Plugins.ext_PluginlastReleaseVersion,Plugins.ext_PluginDescription,Plugins.ext_AutomaticallyOnOff,Attributes.Field";
        public const string OBJ_CARTLINEITEM_SELECTFIELDS = "ListPrice,ProductId,quantity,Term,BasePrice,ext_TermNew,SellingFrequency,NetPrice,NetUnitPrice,ext_BlendedPrice,ext_NNACV,ext_RenewalACV,ext_TCV,itemsequence,linenumber,assetquantity,ExtendedPrice,assetlineitemid,startdate,enddate,sellingterm,Productid.ext_Family,Configurationid,Configurationid.QuoteId.ext_Datacenter,Configurationid.QuoteId.ext_RefAgreement,AGR.ext_quoteid.ext_datacenter,ALI.Id,ALI.startdate,ALI.enddate,ALI.netunitprice,ALI.quantity,ALI.ext_BlendedPrice,Q.ext_Type";
        public const string OBJ_QUOTE_SELECTFIELD = "Id,AccountId,OpportunityId,ExpectedStartDate,ext_AccountMDMID,ExpectedEndDate,ext_Term,ext_Type,ext_ContractType,ext_BaseCurrency,MasterAgreementId,ext_FieldTerritoryMDMID,ext_PSTerritoryMDMID,ext_QuoteOwner,ext_OpportunityLinkReason";
        public const string OBJ_ASSETLINEITEM_SELECTFIELDS = "Id,Name,startdate,enddate,netunitprice,quantity";
        public const string OBJ_QUOTEVALIDATE_DEFAULT_SELECTFIELD = "Id,OpportunityId,ext_OpportunitySysId,ext_OpportunityNumber,IsPrimary,ext_Type,PriceListId,ext_BaseCurrency,crm_Account.ext_Country,ext_PartnerAccountMDMID";
        public const string OBJ_QUOTEOPPORTUNITY_SELECTFIELD = "Id,OpportunityId,ext_OpportunitySysId,ext_OpportunityNumber,IsPrimary";
        public const string OBJ_RENEWALOPPORTUNITY_SELECTFIELD = "Id,ExternalId,ExpectedStartDate,ext_AccountMDMID,ExpectedEndDate,ext_Term,ext_Type,ext_ContractType,ext_BaseCurrency,MasterAgreementId,ext_RenewalACV,ext_USDRenewalACV";
        public const string OBJ_PRICELIST_SELECTFIELD = "Id,Name,CurrencyId";
        //Quote/QuoteLine/Milestone Fields
        public const string OBJ_FIELDQUOTE = "Id,ext_OpportunitySysId,Name,QuoteName,ext_AccountMDMID,ext_Type,ext_ContractType,ExpectedStartDate,ExpectedEndDate,ext_Term,IsPrimary,ext_salestype,ext_PartnerAccount,ext_partnershiptype,ApprovalStage,ApprovalStatus,ext_NNACV,ext_TCV,ext_RenewalACV,ext_FieldTerritoryMDMID,ext_PSTerritoryMDMID,ext_QuoteOwner,ext_OpportunityLinkReason,ext_FlaggedAs,ext_BaseCurrency,ext_PartnerAgreementNumber,ext_EndCustomerMDMSysID,ext_DealRegAssociated,ext_USDTotalNetNewACV,ext_ACV,ext_USDACV,ext_USDTCV,ext_USDRenewalACV,QLI.Id,QLI.QuoteId,QLI.ProductId,QLI.ProductId.ext_DefaultRevRec,QLI.ProductId.ext_RoleGeneric,QLI.ext_FXRate,QLI.ext_FXRateDailyUSD,QLI.ListPrice,QLI.PrimaryLineNumber,QLI.ext_ItemSequenceNumber,QLI.ParentBundleNumber,QLI.ExtendedPrice,QLI.ext_DoNotCalculateNNACV,QLI.ext_USDVariablePriceACVCalc,QLI.ext_SalesPrice,QLI.ext_VariablePriceACVCalc,QLI.PriceAdjustment,QLI.ext_RenewalLineACV,QLI.StartDate,QLI.EndDate,QLI.Term,QLI.ext_USDSummaryLineACV,QLI.OptionId,QLI.OptionId.ext_DefaultRevRec,QLI.OptionId.ext_RoleGeneric,QLI.ext_USDNetNewACV,QLI.Quantity2,QLI.ext_NetNewACV,QLI.ExtendedPrice,QLI.ext_USDListPrice,QLI.ext_USDBasePrice,QLI.ext_ListPriceWithAdjustments,QLI.ext_FXRate,QLI.NetAdjustmentPercent,QLI.ext_USDTotalValue,QLI.ext_AnnualListPrice,QLI.ext_USDSalesPrice,QLI.ext_AnnualListPrice,QLI.ext_USDAnnualContractValue,QLI.ext_AnnualContractValue,QLI.ext_Deployment,QLI.ext_ResourcePlan,QLI.ext_TaskID,QLI.ext_Practice,QLI.LineStatus,QLI.LineType,QLI.DeltaQuantity,QLI.ext_NetNewQuantity,QLI.ext_AllocationPC,QLI.NetAdjustmentPercent,QLI.ext_NetNewACV,QLI.SellingTerm,QLI.ext_FlaggedAs,QLI.ext_ProductHours,QLI.ProductId.ext_DefaultRevRec,QLI.OptionId.ext_DefaultRevRec,MS.ext_QuoteId,MS.ext_Amount,MS.ext_CalculationMethod,MS.ext_Milestone,MS.ext_PaymentOrder,MS.ext_Percent,MS.ext_EstimatedHours,MS.ext_AgreementId,MS.ext_EstimatedDueDate,MS.ext_Phase";

        //Cart Line Item Fields
        public const string FIELD_LINEITEMID = "LineItemID";

        //Asset Line Item Fields
        public const string FIELD_ASSETLINEITEMID = "AssetLineItemID";
        public const string OBJALIAS_ASSETLINEITEM = "ALI";

        //Quote/QuoteLine
        public const string OBJ_FIELDQUOTELINEITEM = "Id,Name,cpq_lineitem.CurrencyId,cpq_QuoteLineItem.Name,cpq_QuoteLineItem.Id,cpq_QuoteLineItem.NetPrice,cpq_QuoteLineItem.ext_TCV,cpq_QuoteLineItem.ext_EstimatedTotal,cpq_QuoteLineItem.ext_AnnualListPrice,cpq_QuoteLineItem.ext_AnnualContractValue,crm_Product.Id,crm_Product.Name,crm_Product.ext_Family,crm_Product.ext_ContributesTo";
        //public const string OBJ_FIELDQUOTELINEITEMFINANCIAL = "Id,Name,cpq_QuoteLineItem.Name,cpq_QuoteLineItem.Id,cpq_QuoteLineItem.ext_CurrencyCode,cpq_QuoteLineItem.AdjustmentAmount,cpq_QuoteLineItem.ListPrice,cpq_QuoteLineItem.Term,cpq_QuoteLineItem.Quantity,cpq_QuoteLineItem.NetPrice,cpq_QuoteLineItem.ext_USDBasePrice,cpq_QuoteLineItem.ext_SalesPrice,cpq_QuoteLineItem.ext_TermNew,cpq_QuoteLineItem.ext_EstimatedTotal,cpq_QuoteLineItem.ext_USDAnnualListPrice,cpq_QuoteLineItem.ext_AnnualListPrice,cpq_QuoteLineItem.ext_AnnualContractValue,cpq_QuoteLineItem.ext_TCV,cpq_QuoteLineItem.NetUnitPrice,cpq_QuoteLineItem.BasePrice,cpq_QuoteLineItem.ext_EstimatedTax,cpq_QuoteLineItem.ext_FlaggedAs,cpq_QuoteLineItem.StartDate,cpq_QuoteLineItem.EndDate,cpq_QuoteLineItem.DeltaQuantity,cpq_QuoteLineItem.ExtendedPrice,crm_Product.Id,crm_Product.Name,crm_Product.ext_Family,crm_Product.ext_ContributesTo";
        public const string OBJ_FIELDQUOTELINEITEMFINANCIAL = "Id,Name,cpq_lineitem.CurrencyId,cpq_QuoteLineItem.Name,cpq_QuoteLineItem.Id,cpq_QuoteLineItem.ext_CurrencyCode,cpq_QuoteLineItem.AdjustmentAmount,cpq_QuoteLineItem.ListPrice,cpq_QuoteLineItem.Term,cpq_QuoteLineItem.Quantity2,cpq_QuoteLineItem.NetPrice,cpq_QuoteLineItem.ext_USDBasePrice,cpq_QuoteLineItem.ext_SalesPrice,cpq_QuoteLineItem.ext_TermNew,cpq_QuoteLineItem.ext_EstimatedTotal,cpq_QuoteLineItem.ext_USDAnnualListPrice,cpq_QuoteLineItem.ext_AnnualListPrice,cpq_QuoteLineItem.ext_AnnualContractValue,cpq_QuoteLineItem.ext_TCV,cpq_QuoteLineItem.NetUnitPrice,cpq_QuoteLineItem.BasePrice,cpq_QuoteLineItem.ext_EstimatedTax,cpq_QuoteLineItem.ext_FlaggedAs,cpq_QuoteLineItem.StartDate,cpq_QuoteLineItem.EndDate,cpq_QuoteLineItem.DeltaQuantity,cpq_QuoteLineItem.ExtendedPrice,cpq_QuoteLineItem.Ext_AdjustmentAmount,crm_Product.Id,crm_Product.Name,crm_Product.ext_Family,crm_Product.ext_ContributesTo,cpq_QuoteLineItem.ProductId.ext_Family";

        //Quote Line Item Fields
        public const string FIELD_QUOTEID = "QuoteId";
        public const string OBJALIAS_QUOTELINEITEM = "QLI";

        //Milestone Field
        public const string FIELD_EXT_QUOTEID = "ext_QuoteId";
        public const string OBJALIAS_MILESTONE = "MS";

        //Agreement Fields
        public const string OBJ_AGREEMENT_SELECTFIELD = "Name,Id,ext_AccountMDMID,RecordTypeId,ext_OpportunitySysId,ext_OpportunityNumber,RequestorId,ext_FieldTerritoryMDMID,Description,ExternalId,ext_QuoteOwner,Status,StatusCategory,ext_Category,ext_ContractNumber,ext_ServiceNowContractNumber,ext_quoteid,ext_CustomerContractNumber,ParentAgreementId,AgreementStartDate,AgreementEndDate,ext_Term,ext_BaseCurrency,ext_PartnerAccountMDMID,ext_ACV,ext_USDACV,ext_TCV,ext_USDTCV,ext_AnnualListPrice,ext_USDAnnualListPrice,CreatedOn,CreatedById.Externalid,ActivatedDate,ActivatedById.Externalid,ext_partnershiptype,clm_AgreementDocument.Id,clm_AgreementDocument.Name,clm_AgreementDocument.Type";

		//Order Fields Only for Hi
		public const string OBJ_ORDERHI_SELECTED = "Id,OriginalOrderNumber,ext_AccountMDMId,ext_ContractType,ext_datacenter,OLI.Id,OLI.ParentBundleNumber,OLI.LineNumber,OLI.ItemSequence,OLI.PrimaryLineNumber,OLI.ext_InstanceName,OLI.ProductId,OLI.ProductId.Name,OLI.OptionId";
		// Plugin Fields
		public const string OBJ_SNPLUGINS_SELECTED = "Id,Name,ext_PluginName,ext_AutomaticallyOnOff,ext_LKP,ext_PluginId";

		//AssetLineItem
		//public const string OBJ_ASSETLINEITEM_SELECTFIELD = "Id,AccountId,AssetStatus,StartDate,EndDate,ParentBundleNumber,ProductId,OptionId,NetPrice";
		public const string OBJ_ASSETLINEITEM_SELECTFIELD = "Id,AccountId,AssetStatus,StartDate,EndDate,ParentBundleNumber,ProductId,OptionId,NetPrice,CreatedOn,ItemSequence,LineNumber,PrimaryLineNumber,Quantity"
            + ",Name,AdjustedPrice,AllowedActions,AssetARR,AssetCode,AssetMRR,AssetNumber,AssetTCV,AttributeValueId,AutoRenew,AutoRenewalType,AvailableBalance"
            + ",BaseCost,BaseExtendedCost,BaseExtendedPrice,BasePrice,BasePriceMethod,BillingEndDate,BillingFrequency,BillingPlanId,BillingPreferenceId"
            + ",BillingRule,BillingStartDate,BundleAssetId,BusinessLineItemId,BusinessObjectId,BusinessObjectType,CancelledDate,ChargeType,Comments"
            + ",DeltaPrice,DeltaQuantity,Description,ExtendedCost,ExtendedDescription,ExtendedPrice,Frequency,HasAttributes,HasOptions,ext_AgreementId"
            + ",HideInvoiceDisplay,InitialActivationDate,IsInactive,IsOptionRollupLine,IsPrimaryLine,IsPrimaryRampLine,IsReadOnly,IsRenewalPending"
            + ",IsRenewed,IsUsageTierModifiable,LastRenewEndDate,LineType,ListPrice,LocationId,MaxUsageQuantity,MinUsageQuantity,MustUpgrade"
            + ",NetUnitPrice,NextRenewEndDate,OptionCost,OptionPrice,OriginalStartDate,ParentAssetId,PaymentTermId,PriceGroup,PriceIncludedInBundle"
            + ",PriceListId,PriceListItemId,PriceMethod,PriceType,PriceUom,PricingDate,ProductType,PurchaseDate,RenewalAdjustmentAmount,RenewalAdjustmentType"
            + ",RenewalDate,RenewalFrequency,RenewalTerm,SellingFrequency,SellingTerm,ShipToAccountId,TaxCodeId,TaxInclusive,Taxable,Term,TotalBalance,QuoteId,QuoteLineItemId"
            + ",ext_Datacenter,ext_ReleaseVersion,ext_DCInstanceID,ext_InstanceName,ext_OperationStatus,ext_ChangeTicket,ext_billToMDMID,ext_shipToMDMID,ext_AccountMDMId,BillThroughDate,AgreementId.ext_ServiceNowContractNumber,ext_PhysicalDataCentre,ext_InstalledMappingID,ext_InstalledMappingName"
            + ",ext_AnnualContractValue,ext_USDAnnualContractValue,ext_AnnualListPrice,ext_USDAnnualListPrice,ext_BusinessUnit,ext_USDAdjustment,ext_USDBasePrice,ext_USDListPrice"
            + ",ext_USDTotalValue,ext_USDSalesPrice,ext_AllocationPercent,ext_Deployment,ext_ICLevel,ext_Practice,ext_ResourcePlan,ext_ResourceRolesFixedFeeItems,ext_SoldMappingID,ext_SoldMappingName,ext_TaskID";

        //Order/Order Line/Invoice Schedule Fields
        public const string OBJ_FIELDORDER = "Id,Name,ext_QuoteType,ext_SoldToMDMID,ext_ShipToMDMID,ext_BillToMDMID,ext_AccountMDMID,ext_PrimarySalesRepID,ext_AffiliatedToCustomer,"
            + "PONumber,ActivatedDate,OrderStartDate,OrderEndDate,ext_PSTerritoryXRefID,ext_SalesTerritoryXRefID,PriceListId,ext_datacenter,ext_T4C,ext_SellingEntityCode,"
            + "ext_TotalNewRenewalACV,ext_NetPrice,ext_USDTotalNetNewACV,ext_AgreementActivatedDate,PaymentTermId,ext_ApttusParentContractNo,ext_taxexempt,ext_ContractType,"
            + "ext_TotalNetNewACV,ext_PrimarySalesRepID,ext_SignificantPenalty,ext_PenaltyAmt,ext_NoticePeriod,ext_RighttoRefund,ext_RefundPercentage,ext_BillingContact,"
            + "ext_OpportunityNumber,ext_BaseCurrency,ext_salestype,ext_ReferenceContract,ext_OrderARNotes,ext_AgreementSignedDate,ext_AgreementStartDate,ext_partnershiptype,"
            + "OLI.Id,OLI.Name,OLI.ParentBundleNumber,OLI.ItemSequence,OLI.LineNumber,OLI.PrimaryLineNumber,OLI.Quantity,OLI.Uom,OLI.ext_ProductCode,OLI.ext_ProductDescription,"
            + "OLI.ext_Deployment,OLI.ext_NetNewACV,OLI.ext_RenewalLineACV,OLI.ext_ItemTCV,OLI.StartDate,OLI.EndDate,OLI.BillingFrequency,OLI.ext_TaskID,OLI.ext_Datacenter,"
            + "OLI.ListPrice,OLI.ExtendedPrice,OLI.NetAdjustmentPercent,OLI.AdjustmentAmount,"
            + "ISS.Id,ISS.Name,ISS.ext_Quote,ISS.ext_OrderId,ISS.ext_GrandTotal,ISS.ext_Amount,ISS.ext_EstimatedTax,ISS.ext_InvoiceDisplay,ISS.ext_OrderSequence,ISS.ext_InvoiceType,ISS.ext_InvoiceSchedule,ISS.ext_InvoiceDate";

        //Quote/Quote Line/Invoice Schedule
        public const string OBJ_FIELDORDER_INVSCH = "Id,Name,ExpectedStartDate,ExpectedEndDate,PaymentTerm,ext_Type,cpq_QuoteLineItem.Name,cpq_QuoteLineItem.Id,cpq_QuoteLineItem.NetPrice,cpq_QuoteLineItem.StartDate,cpq_QuoteLineItem.EndDate,cpq_QuoteLineItem.ProductId.ext_Family,cpq_QuoteLineItem.PriceType";
        public const string OBJ_FIELDORDER_INVSCH_SELECTFIELD = "Id,Name";
        //Order Line Item Fields
        public const string FIELD_ORDERID = "OrderId";
        public const string OBJALIAS_ORDERLINEITEM = "OLI";

        //Invoice Schedule Field
        public const string FIELD_EXT_QUOTE = "ext_Quote";
        public const string OBJALIAS_INVOICESCEDULE = "ISS";

        //Action Types
        public const string ACTIONTYPE_ALLOW = "Allow";
        public const string ACTIONTYPE_HIDDEN = "Hidden";
        public const string ACTIONTYPE_DISABLED = "Disabled";

        //Alias
        public const string ALIAS_OPTIONS = "Options";
        public const string ALIAS_PLUGINS = "Plugins";
        public const string ALIAS_ATTRIBUTES = "Attributes";
        public const string ALIAS_PLI = "PLI";


        //Char Constants
        public const char CHAR_SEMICOLON = ';';
        public const char CHAR_DOT = '.';
        public const char CHAR_SINGLEQUOTE = '\'';
        public const char CHAR_COMMA = ',';
        public const char CHAR_EQUAL = '=';
        public const char CHAR_AMPERSAND = '&';
        public const char CHAR_QUESTIONMARK = '?';
        public const char CHAR_M = 'M';
        public const char CHAR_I = 'I';
        public const char CHAR_HYPHEN = '-';

        //String Constants
        public const string STR_NONE = "None";
        public const string STR_YEARLY = "yearly";
        public const string STR_SUBSCRIPTION = "Subscription";
        public const string STR_MONTHLY = "Monthly";
        public const string TOTALPSREVENUE = "Total PS Revenue";
        public const string LICENSEREVENUE = "License Revenue";
        public const string OTHERREVENUE = "Other Revenue";
        public const string SUBSCRIPTIONANDLICENSEREVENUE = "Subscription & License Revenue";
        public const string TRAININGREVENUE = "Training Revenue";
        public const string STR_MESSAGE = "Message";
        public const string STR_TRAINING = "Training";
        public const string STR_INACTIVE = "Inactive";
        public const string STR_ACTIVE = "Active";
		public const string STR_OPTION = "Option";

		//String Constants Country/DataCenter
		public const string STR_UNITEDKINGDOM = "United Kingdom";
        public const string STR_UNITEDSTATES = "US - United States";
        public const string STR_CANADA = "Canada";
        public const string STR_SWITZERLAND = "Switzerland";
        public const string STR_MEXICO = "Mexico";
        public const string STR_AUSTRALIA = "Australia";
        public const string STR_NEWZEALAND = "New Zealand";
        public const string STR_BRASIL = "Brasil";
        public const string STR_NEDERLAND = "Nederland";
        public const string STR_JAPAN = "Japan";

        public const string STR_UNITEDSTATES_DC = "United States";
        public const string STR_SWITZERLAND_DC = "Switzerland";
        public const string STR_Canada_DC = "Canada";
        public const string STR_BRAZIL_DC = "Brazil";
        public const string STR_AUSTRALIA_DC = "Australia";
        public const string STR_EMEA_DC = "EMEA";

        public const string STR_USD = "USD";
        public const string STR_AUD = "AUD";
        public const string STR_BRL = "BRL";
        public const string STR_CAD = "CAD";
        public const string STR_CHF = "CHF";
        public const string STR_DKK = "DKK";
        public const string STR_EUR = "EUR";
        public const string STR_HKD = "HKD";
        public const string STR_JPY = "JPY";
        public const string STR_NZD = "NZD";
        public const string STR_SGD = "SGD";
        public const string STR_GBP = "GBP";
        public const string STR_MXN = "MXN";

        //Activate status of agreement
        public const string STR_AGREEMENTSTATUS = "100000011";

		public const string STR_EXT_HIINTEGRATIONPROCESSED = "ext_HiIntegrationProcessed";

		//Operator Constants
		public const string OPR_EQUAL = "Equal";

        //Error Messages
        public const string ERR_AUTH_TOKEN_MISSING = "Auhtorization Token Missing.";
        public const string ERR_UNRESOLVED_MESSAGE = "Following fields are not available in Apttus.";
        public const string ERR_INSTANCE_NAME = "Instance name already exists";
        public const string ERR_INSTANCE_GENERIC = "Issue while loading instances";
        public const string ERR_INVALID_ARGUMENT_VALUE = "Invalid argument value.";
        public const string ERR_TAX_MANDATORYFIELDSNULL = "One or more of the fields (Selling Entity, Country, State) is either null or empty.Hence no tax was computed";
        public const string ERR_TAX_NOQUOTEFOUND = "No Quote is found.";
        public const string ERR_TAX_EXEMPTTRUE = "Tax exempt is true for Quote. No Tax is computed.";

        //ContributesTo Mapping
        public const string SUBSCRIPTION_LICENSE_REVENUE = "Subscription & License Revenue";
        public const string LICENSE_REVENUE = "License Revenue";
        public const string TOTAL_PS_REVENUE = "Total PS Revenue";
        public const string TRAINING_REVENUE = "Training Revenue";

        //Product Category on TAX Rate Mapping
        public const string SUBSCRIPTION = "Subscription";
        public const string EDUCATION = "Education";
        public const string SERVICES = "Services";

        //EXT_Family values on Product
        public const string PROD_INSTANCE = "Prod Instance";
        public const string NON_PROD_INSTANCE = "Non_Prod Instance";
        public const string ADDITIONAL_PROD_INSTANCE = "Additional Prod Instance";
        public const string ADDITIONAL_NON_PROD_INSTANCE = "Additional Non Prod Instance";

        //Misc
        public const string MISC_SALES_TAX = "Sales Tax";
        public const string MISC_PRODUCT_SERVICE = "Product/Service";
        public const string MISC_PLATFORM = "Platform";
        public const string MISC_TBD = "TBD";
        public const string MISC_COMPLETED = "Completed";
        // Caching constants
        public const string CACHE_ACCESSTOKEN = "accesstoken";

        // Clone Agreement API Url
        public const string CLONEAGREEMENTAPIURL = "https://canary-aql-test1.apttuscloud.com/api/clm/v1/agreements/{0}/lifecycle/Clone";
        public const string MSG_SUCCESS = "Success!";
        public const string OBJ_AGREEMENTCLAUSE = "clm_AgreementClause";

        public const string FIELD_EXT_SALESTYPE = "ext_salestype";
        public const string FIELD_SHIPTOACCOUNT = "ShipToAccountId";
        public const string FIELD_EXT_INVOICEACCOUNT = "ext_invoiceaccount";
        //public const string FIELD_EXT_SHIPTOLOCATION = "ext_ShipToLocation";
        public const string FIELD_SALESPARTNER = "ext_SalesPartner";
        public const string FIELD_EXT_BILLTOLOCATION = "ext_BillToLocation";

        public const string MISC_DIRECT = "Direct";
        public const string MISC_INDIRECT = "Indirect";
        public const string MISC_NONE = "None";
        public const string MISC_TRUE = "true";
        public const string MISC_FALSE = "false";
        public const string MISC_SHIP = "ShipTo";


        public const string CLAUSE_SEPARATOR = "#@#";

        public const string MISC_BILL = "BillTo";

        //Common Values
        public const bool VALUE_TRUE = true;
        public const bool VALUE_FALSE = false;



        #region Approvals

        public const string OBJ_APPROVALCALCULATION_DEFAULT_SELECTFIELDS = "Id,ext_DealSize,ext_MaxDiscount,ext_RenewalCount,ext_ApproverUserDiscount,ext_CustomerSatisfactionExceptionApproval," +
                "ext_EndOfLifeProductsApproval,ext_Type,ext_salestype,ext_SalesPartner,AccountId,AccountId.ext_Segment,QLI.Id,QLI.ext_USDAnnualListPrice,QLI.ext_AdjustmentDiscountPercent,QLI.ext_USDListPrice,QLI.ext_ContractQuantity" +
                "QLI.ProductId,QLI.ProductId.ext_ProductLifecycleStatus,QLI.ProductId.ext_Family,QLI.AttributeValueId.ext_UserModel,AGR.ext_QuoteId,AGR.ext_QuoteId.ext_DealSize";
        public const string STR_PRODUCT_FAMILY_SUBSCRIPTIONS = "Subscriptions";
        public const string OBJALIAS_AGREEMENT = "AGR";
        public const string FIELD_EXT_REFAGREEMENT = "ext_refagreement";

        public const string OBJ_PSAPPROVALCALCULATION_DEFAULT_SELECTFIELDS = "Id,MS.Id,MS.Name,QLI.ext_AdjustmentDiscountPercent,QLI.ProductId";

        #endregion Approvals

        #region PS
        public const string ERR_ARGUMENT_NULL = "Argument value is null.";
        public const string OBJALIAS_PRODUCT = "PRD";
        public const string STR_SUCCESS = "Success";
        public const string STR_CPQ_QUOTE = "cpq_Quote";
        public const string STR_CRM_USER = "crm_User";
        public const string STR_TRAVELEXPENSE_GROUP = "Resource SKUS PS";
        public const string CONTROLLED = "Controlled (CA)";
        public const string GENERAL = "General (GA)";
        public const string FEDRAMP = "FedRamp";
        public const string EXPRESS = "Express";
        public const string ENTERPRISE = "Enterprise";
        public const string FIXEDFEESERVICES = "'Fixed Fee Services";
        public const string EDUCATIONSERVICES = "Education Services";
        public const string TAILOREDSERVICES = "Tailored Services";
        public const string STR_TAILOREDSERVICES = "Tailored Services";

        public const string OBJ_PSMILESTONES_SELECTFIELD = "Id,Name,ext_Milestone.ext_AgreementId,ext_Milestone.ext_PaymentOrder,ext_Milestone.ext_Amount,ext_Milestone.ext_Due,ext_Milestone.ext_EstimatedDueDate,ext_Milestone.ext_Milestone,ext_Milestone.ext_Phase";
        public const string OBJ_PRODCUTGROUPMEMBER_SELECTFIELD = "ProductGroupId,ProductId";
        public const string OBJ_JNC_CMN_ROLE_CRM_USER_SELECTFIELD = "cmn_RoleId,crm_UserId,ext_IsActive";
        public const string OBJ_CMN_ROLE_SELECTFIELD = "Id,Name";
        public const string OBJ_FIELDS_POPULATEPRODUCTAPPLICATION = "Name,QuoteId,ProductId,ClassificationId,AttributeValueId,PRD.ext_Application";
        public const string OBJ_FIELDORDER_HI_SAP = "Id,ActivatedDate,ext_SAPContractID";
        public const string OBJ_FIELDORDER_LINEITEM_HI_SAP = "Id,ext_SAPContractLineID,ext_HiIntegrationProcessed";
        public const string OBJ_TANDECARTLINEITEM_SELECTFIELD = "Id,Name,cpq_LineItem.BaseProductId,cpq_LineItem.ProductOptionId,cpq_LineItem.ParentBundleNumber,cpq_LineItem.BaseExtendedPrice,cpq_LineItem.ProductId,cpq_LineItem.OptionId,cpq_LineItem.ParentBundleNumber";
        public const string OBJ_CARTLINEITEM_SELECTFIELD = "Id,Name,MasterAgreementId,crm_Product.Id,crm_Product.Name,cpq_LineItem.Quantity,cpq_LineItem.BasePrice,cpq_LineItem.NetPrice,cpq_LineItem.OptionId,cpq_LineItem.OptionId.ext_RoleGeneric,cpq_ProductClassification.ClassificationId.Name";
        public const string OBJ_CMN_PARTICIPANT_SELECTFIELD = "Id,Name,ContextObject,Participant,cmn_ParticipantRole.Role";
        public const string OBJ_CRM_USERGROUPMEMBERSHIP_SELECTFIELD = "Id,UserGroupId,UserId,ext_RoleId,ext_IsActive";

        public const string FIELD_EXT_QUOTENUMBER = "ext_QuoteNumber";
        public const string FIELD_EXT_RELEASE = "ext_Release";
        public const string FIELD_EXT_RELEASEVERSION = "ext_ReleaseVersion";
        public const string FIELD_EXT_ENTERPRISEINTERNAL = "ext_EnterpriseInternal";
        public const string FIELD_EXT_EXPRESSINTERNAL = "ext_ExpressInternal";
        public const string FIELD_EXT_RELEASEAVAILABILITY = "ext_ReleaseAvailability";
        public const string FIELD_EXT_PUBLIC = "ext_Public";
        public const string FIELD_EXT_ENTERPRISENEWINSTANCEDEFAULT = "ext_EnterpriseNewInstanceDefault";
        public const string FIELD_EXT_EXPRESSNEWINSTANCEDEFAULT = "ext_ExpressNewInstanceDefault";
        public const string FIELD_EXT_FEDRAMPNEWINSTANCEDEFAULT = "ext_FedrampNewInstanceDefault";
        public const string FIELD_EXT_Application = "ext_Application";

        public const string OBJ_EXT_SERVICESAPPLICATION = "ext_ServicesApplication";
        public const string OBJ_PSSOWRESOURCE = "ext_PSSOWResource";
        public const string OBJ_CMN_PARTICIPANT = "cmn_Participant";
        public const string OBJ_CMN_PARTICIPANTROLE = "cmn_ParticipantRole";
        public const string OBJ_CPQ_PRODCUTGROUPMEMBER = "cpq_ProductGroupMember";
        public const string OBJ_CMN_ROLE = "cmn_Role";
        public const string OBJ_EXT_RELEASEVERSION = "ext_ReleaseVersion";
        public const string OBJ_PRODUCTCLASSIFICATION = "cpq_ProductClassification";

        #endregion PS
    }

    public static class AgreementObjectConstants
    {
        public const string ENTITYNAME = "clm_Agreement";
        public const string OBJ_ALIAS = "AGMNT";

        public const string EXT_AGREEMENTCLAUSEGROUPINCLUDED = "ext_AgreementClauseGroupIncluded";
        public const string EXT_AGREEMENTCLAUSESINCLUDED = "ext_AgreementClausesIncluded";
        public const string EXT_AGREEMENTCLAUSEGROUPMODIFIED = "ext_AgreementClauseGroupModified";
        public const string EXT_AGREEMENTCLAUSESMODIFIED = "ext_AgreementClausesModified";
        public const string RECORDTYPEID = "RecordTypeId";
        public const string EXT_PDFTEMPLATE = "ext_PDFTemplate";
        public const string EXT_QUOTENUMBER = "ext_QuoteNumber";
        public const string EXT_RELEASE = "ext_Release";
        public const string EXT_USERTYPE = "ext_UserType";
        public const string EXT_QUOTEID = "ext_quoteid";
        public const string EXT_AGREEMENTLINEITEMDATA = "ext_AgreementLineItemData";

        public static string Id = "Id";
        public static string SalesOpsRep = "ext_SalesOpsRep";
        public static string SubmitToSalesOpsReasons = "ext_SubmitToSalesOpsReasons";

        public static string FIELDS_PDFPOPULATION = "";

        public const string EXT_PDFTEMPLATE_SERVICES = "Services";
        public const string EXT_PDFTEMPLATE_FULFILLERORDERFORM = "Fulfiller Order Form";
        public const string EXT_PDFTEMPLATE_SERVICEWATCH = "ServiceWatch";
        public const string EXT_PDFTEMPLATE_KNOWLEDGEEVENT = "Knowledge Event";
        public const string EXT_PDFTEMPLATE_ELAUNRESTRICTEDUSER = "ELA Unrestricted User";
        public const string SALESMASTERAGREEMENTOBJ_ALIAS = "SALESMASTERAGMNT";
        public const string SALESMASTERAGREEMENTFIELDSTOCOPY = "SalesMasterAgreementFieldsToCopy";
        public const string MASTERCONTRACT = "ext_u_master_contract";
        public static string FIELDS_SALESMASTERAGREEMENTFETCH = string.Join(Constants.CHAR_COMMA.ToString(),
                    new string[]
                    {
                   Id
                    });
        public static string FIELDS_SALESMASTERAGREEMENTDETAILFETCH = string.Join(Constants.CHAR_COMMA.ToString(),
            new string[]
            {
                   Id
            });
    }

    public static class InvoiceScheduleObjectConstants
    {
        public static string Id = "Id";
        public static string Quote = "ext_Quote";
        public static string Agreement = "ext_agreement";
    }

    public static class AgreementLineItemConstants
    {
        public const string ENTITYNAME = "clm_AgreementLineItem";
        public const string OBJ_ALIAS = "ALI";

        public const string PRODUCTID = "ProductId";
        public const string AGREEMENTID = "AgreementId";
        public const string EXT_USERTYPE = "ext_UserType";
        public const string EXT_DELTAQUANTITY = "ext_DeltaQuantity";
        public const string EXT_CONTRACTQUANTITY = "ext_ContractQuantity";
        public const string QUANTITY = "Quantity";
        public const string EXT_SALESPRICE = "ext_SalesPrice";
        public const string EXT_ANNUALCONTRACTVALUE = "ext_AnnualContractValue";
        public const string EXT_NETNEWACV = "ext_NetNewACV";
        public const string EXT_USDTOTALVALUE = "ext_USDTotalValue";
        public const string EXT_INSTANCENAME = "ext_InstanceName";

        public static string FIELDS_PDFPOPULATION = string.Join(Constants.CHAR_COMMA.ToString(),
            new string[]
            {
                   PRODUCTID, EXT_USERTYPE, EXT_DELTAQUANTITY, EXT_CONTRACTQUANTITY,
                   AgreementObjectConstants.OBJ_ALIAS + Constants.CHAR_DOT + AgreementObjectConstants.EXT_QUOTENUMBER,
                   QuoteConstants.OBJ_ALIAS + Constants.CHAR_DOT + QuoteConstants.EXT_TYPE,
                   ProductConstants.OBJ_ALIAS + Constants.CHAR_DOT + ProductConstants.FAMILY
            });
        public static string[] FIELDS_HTMLROW = new string[]
            {
                   PRODUCTID,AGREEMENTID,QUANTITY,EXT_SALESPRICE,EXT_ANNUALCONTRACTVALUE,EXT_NETNEWACV,EXT_USDTOTALVALUE,
                   AgreementObjectConstants.OBJ_ALIAS + Constants.CHAR_DOT + AgreementObjectConstants.EXT_AGREEMENTLINEITEMDATA,
            };

        public static string[] FIELDS_INSTANCEDETAILS = new string[]
            {
                   PRODUCTID,EXT_INSTANCENAME,EXT_SALESPRICE,EXT_ANNUALCONTRACTVALUE,EXT_NETNEWACV,EXT_USDTOTALVALUE,
                   ProductConstants.OBJ_ALIAS + Constants.CHAR_DOT + ProductConstants.EXT_FAMILY,
            };

        public const string HTML_ROW_FORMAT = "<tr><td>{0}</td><td>{1}</td><td>{2}</td><td>{3}</td><td>{4}</td><td>{5}</td><td>{6}</td></tr>";
    }

    public static class ApprovalRequestConstants
    {
        public const string ENTITYNAME = "iwa_ApprovalRequest";
        public const string OBJ_ALIAS = "ARQ";

        public const string OBJECT = "Object";
        public const string EXT_AGREEMENTID = "ext_AgreementId";

        public static string FIELDS_COPYAGREEMENTID = string.Join(Constants.CHAR_COMMA.ToString(),
            new string[]
            {
                   OBJECT, EXT_AGREEMENTID
            });
    }

    public static class QuoteConstants
    {
        public const string ENTITYNAME = "cpq_Quote";
        public const string OBJ_ALIAS = "QT";


        public const string EXT_TYPE = "ext_Type";
        public const string EXT_QUOTENUMBER = "ext_QuoteNumber";

        public const string EXT_TYPE_SERVICES = "Services";
        public const string EXT_TYPE_TRAINING = "Training";
    }
    public static class ProductConstants
    {
        public const string ENTITYNAME = "crm_Product";
        public const string OBJ_ALIAS = "PRD";

        public const string FAMILY = "Family";
        public const string EXT_FAMILY = "ext_Family";
        public const string EXT_FAMILY1 = "ext_Family1";
        public const string EXT_FAMILY2 = "ext_Family2";
        public const string EXT_FAMILY3 = "ext_Family3";

        public const string NAME_SERVICEWATCH = "ServiceWatch";
        public const string FAMILY_EVENTS = "Events";
        public const string FAMILY_EDUCATIONSERVICES = "Education Services";
    }
    public static class AgreementClauseConstants
    {
        public const string ENTITYNAME = "clm_AgreementClause";
        public const string OBJ_ALIAS = "AC";

        public const string CLAUSEID = "ClauseId";
        public const string AGREEMENTID = "AgreementId";
        public const string ACTION = "Action";
        public const string KEYWORDS = "Keywords";

        public const string ACTION_INSERTED = "Inserted";
        public const string ACTION_MODIFIED = "Modified";
        public const string ACTION_DELETED = "Deleted";

        public const string OBJ_AGREEMENTCLAUSE_SELECTFIELD = "Action,AgreementId,ClauseId,TMP.Name,TMP.Keywords,AGMNT.ext_AgreementClausesIncluded,AGMNT.ext_AgreementClauseGroupIncluded,AGMNT.ext_AgreementClausesModified,AGMNT.ext_AgreementClauseGroupModified";

    }

    public static class TemplateConstants
    {
        public const string ENTITYNAME = "dgn_Template";
        public const string OBJ_ALIAS = "TMP";
    }

    public static class ParticipantConstants
    {
        public const string ENTITYNAME = "cmn_Participant";
        public const string CONTEXTOBJECT = "ContextObject";
        public const string EXT_ISACTIVE = "ext_IsActive";
        public const string EXT_JOBID = "ext_JobId";
        public const string EXT_JOBNAME = "ext_JobName";
        public const string EXTERNALID = "ExternalId";
        public const string PARTICIPANT = "Participant";
    }

    public static class ParticipantRoleConstants
    {
        public const string ENTITYNAME = "cmn_ParticipantRole";
        public const string PARTICIPANTID = "ParticipantId";
        public const string CONTEXTOBJECT = "ContextObject";
        public static string FIELDS = "Id,Name";
        public const string NAME = "Name";
        public const string ROLE = "Role";
        public const string EXTERNALID = "ExternalId";
    }

    public static class QuoteLineItemConstants
    {
        public const string ENTITYNAME = "cpq_QuoteLineItem";
        public const string QUOTEID = "QuoteId";
        //TODO: Bhavin Correct this once the field is available
        public const string EXT_RELEASE = "ext_Release";
        public const string FIELDS = "Id,ext_Type,ext_Release";
    }

    public static class ProductSettingValueConstants
    {
        public const string ENTITYNAME = "cmn_ProductSettingValue";
        public const string OBJ_ALIAS = "PSV";
        public const string VALUE = "Value";

        public const string VALUE_AGREEMENTFIELDSANDDEFAULTVALUES = "Agreement-fields-and-default-values";
        public const string FIELD = "Field";
    }

    public static class ProductSettingConstants
    {
        public const string ENTITYNAME = "cmn_productsettingvalue";
        public const string VALUE = "value";
        public const string NAME = "name";
        public static string FIELDS_PRODUCTSETTING = string.Join(Constants.CHAR_COMMA.ToString(),
            new string[]
            {
                   NAME,VALUE
            });
    }

    public static class InstanceDetailsConstants
    {
        public const string ENTITYNAME = "ext_InstanceDetail";
        public const string OBJ_ALIAS = "INSDET";
    }

    public static class AccountLocationConstants
    {
        public const string OBJ_ACCOUNTLOCATIONS_SELECTFIELD = "Id,Name,ExternalId,Type,ext_AddressLine1,ext_AddressLine2,ext_City,ext_Country,ext_PostalCode,ext_State,ModifiedOn,ext_FieldTerritoryMDMId,ext_FieldTerritoryId,ext_PSTerritoryMDMId,ext_PSTerritoryId";//ext_PrimaryBillTo,ext_PrimaryShipTo
    }
}
