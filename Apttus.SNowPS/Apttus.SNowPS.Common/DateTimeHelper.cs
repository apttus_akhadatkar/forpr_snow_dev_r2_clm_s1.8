﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Apttus.SNowPS.Common
{
    public class DateTimeHelper
    {
        private DateTime _date1;
        private DateTime _date2;
        private int _years;
        private int _months;
        private int _days;
        private int _hours;
        private int _minutes;
        private int _seconds;
        private int _milliseconds;

        public int Years { get { return _years; } }
        public int Months { get { return _months; } }
        public int Days { get { return _days; } }
        public int Hours { get { return _hours; } }
        public int Minutes { get { return _minutes; } }
        public int Seconds { get { return _seconds; } }
        public int Milliseconds { get { return _milliseconds; } }

        public DateTimeHelper(DateTime date1, DateTime date2)
        {
            _date1 = (date1 > date2) ? date1 : date2;
            _date2 = (date2 < date1) ? date2 : date1;

            _years = _date1.Year - _date2.Year;
            _months = (_years * 12) + _date1.Month - _date2.Month;
            TimeSpan t = (_date2 - _date1);
            _days = t.Days;
            _hours = t.Hours;
            _minutes = t.Minutes;
            _seconds = t.Seconds;
            _milliseconds = t.Milliseconds;

        }

        public static DateTimeHelper CompareDates(DateTime date1, DateTime date2)
        {
            return new DateTimeHelper(date1, date2);
        }
    }
}
