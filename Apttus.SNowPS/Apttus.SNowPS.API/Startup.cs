﻿/****************************************************************************************
@Name: Startup.cs
@Author: Auto Generated
@CreateDate: 1 Sep 2017
@Description: Configures Routes and applicaion level properties 
@UsedBy: 

@ModifiedBy: 
@ModifiedDate: 
@ChangeDescription: 
*****************************************************************************************/

using System.Web.Http;
using System.Web.Http.Cors;
using System.Fabric;
using Owin;

namespace Apttus.SNowPS.API {
    public class Startup
    {
        // This code configures Web API. The Startup class is specified as a type
        // parameter in the WebApp.Start method.

        StatelessServiceContext serviceContext { get; set; }

        public Startup(StatelessServiceContext ctx)
        {
            serviceContext = ctx;
        }

        public void ConfigureApp(IAppBuilder appBuilder) {
            // Configure Web API for self-host. 
            HttpConfiguration config = new HttpConfiguration();
            var cors = new EnableCorsAttribute("*", "*", "*");
            config.EnableCors(cors);

            config.Routes.MapHttpRoute(
                name: "CustomSyncAPI",
                routeTemplate: "api/snowps/v1/{controller}/{action}/{id}",
                defaults: new { id = RouteParameter.Optional }
            );

            config.Routes.MapHttpRoute(
                name: "DefaultApi",
                routeTemplate: "api/snowps/v1/{controller}/{id}",
                defaults: new { id = RouteParameter.Optional }
            );
            config.MapHttpAttributeRoutes();
            var x = config.Routes;
            appBuilder.UseWebApi(config);
        }
    }
}
