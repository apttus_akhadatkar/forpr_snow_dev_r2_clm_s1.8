﻿/****************************************************************************************
@Name: AgreementClauseController.cs
@Author: Bhavinkumar Mistry
@CreateDate: 25 Oct 2017
@Description: API calls related to Agreement Clause Entity
@UsedBy: Apttus AIC CLM

@ModifiedBy: Bharat Kumbhar
@ModifiedDate: 31 Oct 2017
@ChangeDescription: Added the method "PopulateAgreementClauseDetailsToAgreement"
*****************************************************************************************/

using Apttus.SNowPS.Common;
using Apttus.SNowPS.Repository.CLM;
using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace Apttus.SNowPS.API.Controllers
{
    /// <summary>
    /// Agreement Clause Contoller
    /// </summary>
    [ServiceRequestActionFilter]
    public class AgreementClauseController : BaseController
    {
        #region Action Methods
        /// <summary>
        /// Action Method to return all or filtered Agreement Clauses based on the query string input
        /// </summary>
        /// <returns>HttpResponseMessage</returns>+
        public HttpResponseMessage Get()
        {
            try
            {
                if (Request != null && Request.RequestUri != null)
                {
                    var agreementClauseRepository = AgreementClauseRepository.Instance(AccessToken);
                    var response = agreementClauseRepository.GetAgreementClauses(Request.RequestUri.Query);
                    if (response != null)
                        return Utilities.CreateResponse(response);
                    else
                        return Request.CreateResponse(HttpStatusCode.ExpectationFailed, "");
                }
                return Request.CreateResponse(HttpStatusCode.BadRequest, "");
            }
            catch (Exception ex)
            {
                //return BadRequest(ex.Message); 
                //Use this if HttpResponse
                return Request.CreateResponse(HttpStatusCode.BadRequest, new Dictionary<string, object> { { Constants.STR_MESSAGE, ex.Message } });
            }
        }

        /// <summary>
        /// Action Method to get AgreementClase record data based on ID and columns
        /// </summary>
        /// <param name="agreementClauseId"></param>
        /// <returns>HttpResponseMessage</returns>
        /// TODO: Implememted for the AgreementClauseId and Columns input
        public HttpResponseMessage Get(string agreementClauseId)
        {
            try
            {
                if (Request != null && Request.RequestUri != null)
                {
                }
                return Request.CreateResponse(HttpStatusCode.BadRequest, "");

            }
            catch (Exception ex)
            {
                //return BadRequest(ex.Message); 
                //Use this if HttpResponse
                return Request.CreateResponse(HttpStatusCode.BadRequest, new Dictionary<string, object> { { Constants.STR_MESSAGE, ex.Message } });
            }
        }

        /// <summary>
        /// Populates the agreement clause details to agreement header.
        /// </summary>
        /// <param name="id">The agreement clause id.</param>
        /// <returns></returns>
        /// <exception cref="ArgumentException">id</exception>
        [HttpPost]
        public IHttpActionResult PopulateClauseDetailsToAgreement([FromBody]Dictionary<string, object> body)
        {
            try
            {
                Guid guid;
                if (body.ContainsKey("id") && Guid.TryParse(body["id"].ToString(), out guid))
                {
                    var agreementClauseRepository = AgreementClauseRepository.Instance(AccessToken);

                    return Ok(agreementClauseRepository.PopulateAgreementClauseDetailsToAgreement(guid));
                }
                else
                {
                    throw new ArgumentException(Constants.ERR_INVALID_ARGUMENT_VALUE, "id");
                }
            }
            catch (Exception ex)
            {
                if (ex.InnerException != null)
                {
                    throw ex.InnerException;
                }
                throw ex;
            }
        }

        #endregion
    }
}
