﻿/****************************************************************************************
@Name: AgreementClauseRepository.cs
@Author: Bhavinkumar Mistry
@CreateDate: 26 Oct 2017
@Description: Agreement Clause related business logic
@UsedBy: This will be used by AgreementClauseController.cs

@ModifiedBy: Bharat Kumbhar
@ModifiedDate: 31 Oct 2017
@ChangeDescription: Added the method "PopulateAgreementClauseDetailsToAgreement"
*****************************************************************************************/


using Apttus.SNowPS.Common;
using Apttus.SNowPS.Model;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;

namespace Apttus.SNowPS.Repository.CLM
{
    /// <summary>
    /// Agreement Clause Repository
    /// </summary>
    public sealed class AgreementClauseRepository
    {
        #region Private Fields
        /// <summary>
        /// Static object of AgreementClauseRepository
        /// </summary>
        private static AgreementClauseRepository agreementClauseRepository;
        #endregion

        #region Public Properties
        /// <summary>
        /// Property to store Authentication token
        /// </summary>
        public string AuthToken { get; set; }
        #endregion

        #region Constructors
        // Explicit static constructor to tell C# compiler
        // not to mark type as beforefieldinit
        static AgreementClauseRepository()
        {
        }

        /// <summary>
        /// Parameterized constuctor
        /// </summary>
        /// <param name="authToken"></param>
        private AgreementClauseRepository(string authToken)
        {
            AuthToken = authToken;
        }
        #endregion

        #region Public Methods
        /// <summary>
        /// Use to Get Single Instance of AgreementClauseRepository
        /// </summary>
        /// <param name="authToken"></param>
        /// <returns></returns>
        public static AgreementClauseRepository Instance(string authToken)
        {
            if (agreementClauseRepository == null)
            {
                agreementClauseRepository = new AgreementClauseRepository(authToken);
            }
            else
            {
                agreementClauseRepository.AuthToken = authToken;
            }
            return agreementClauseRepository;
        }

        /// <summary>
        /// Method to retrive AgreementClauses based on the queryString
        /// </summary>
        /// <param name="queryString">Input QueryString</param>
        /// <returns>HttpResponseMessage</returns>
        public HttpResponseMessage GetAgreementClauses(string queryString)
        {
            // Creating a Request Config Model
            var requestConfig = new RequestConfigModel
            {
                accessToken = AuthToken,
                objectName = Constants.OBJ_AGREEMENTCLAUSE
            };

            var response = Utilities.GetSearch(!string.IsNullOrWhiteSpace(queryString) ? Utilities.GetDecodedQuery(queryString) : string.Empty, requestConfig);
            return Utilities.CreateResponse(response);
        }


        /// <summary>
        /// Populates the agreement clause details to agreement.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <param name="accessToken">The access token.</param>
        /// <returns></returns>
        public string PopulateAgreementClauseDetailsToAgreement(Guid id)
        {
            /* Query expression*/
            var clauseQuery = new DataAccess.Common.Model.Query
            {
                EntityName = AgreementClauseConstants.ENTITYNAME,
                Columns = AgreementClauseConstants.OBJ_AGREEMENTCLAUSE_SELECTFIELD.Split(Constants.CHAR_COMMA).ToList(),
                Criteria = new DataAccess.Common.Model.Expression
                {
                    Conditions = new List<DataAccess.Common.Model.Condition>()
                    {
                        new DataAccess.Common.Model.Condition /* Filter with given Id. */
                        {
                            FieldName      = Constants.FIELD_ID,
                            FilterOperator = DataAccess.Common.Enums.FilterOperator.Equal,
                            Value          = id
                        }
                    }
                },
                Joins = new List<DataAccess.Common.Model.Join>
                {
                    new DataAccess.Common.Model.Join /* inner join with dgn_Template entity from clm_AgreementClause entity based on ClauseId */
                    {
                        EntityAlias   = TemplateConstants.OBJ_ALIAS,
                        FromEntity    = AgreementClauseConstants.ENTITYNAME,
                        FromAttribute = AgreementClauseConstants.CLAUSEID,
                        ToEntity      = TemplateConstants.ENTITYNAME,
                        ToAttribute   = Constants.FIELD_ID,
                        JoinType      = DataAccess.Common.Enums.JoinType.INNER
                    },
                    new DataAccess.Common.Model.Join /* inner join with clm_Agreement entity from clm_AgreementClause entity based on AgreementId */
                    {
                        EntityAlias   = AgreementObjectConstants.OBJ_ALIAS,
                        FromEntity    = AgreementClauseConstants.ENTITYNAME,
                        FromAttribute = AgreementClauseConstants.AGREEMENTID,
                        ToEntity      = AgreementObjectConstants.ENTITYNAME,
                        ToAttribute   = Constants.FIELD_ID,
                        JoinType      = DataAccess.Common.Enums.JoinType.INNER
                    }
                }
            };
            try
            {
                var response = Utilities.Search(clauseQuery.Serialize(), new RequestConfigModel
                {
                    accessToken = AuthToken,
                    objectName = AgreementClauseConstants.ENTITYNAME,
                    searchType = SearchType.AQL
                });

                if (response?.Content != null)
                {
                    var responseString = JObject.Parse(response.Content.ReadAsStringAsync().Result).SelectToken(Constants.NODE_SERIALIZEDRESULTENTITIES)?.ToString();

                    var agreementClauses = JsonConvert.DeserializeObject<List<Dictionary<string, object>>>(responseString);

                    if (agreementClauses != null && agreementClauses.Count > 0)
                    {
                        var agreementClause = agreementClauses[0];

                        var agreements = new List<dynamic>();

                        if (agreementClause != null)
                        {
                            string clauses, action, keywords, agreementClausesIncluded, agreementClauseGroupIncluded, agreementClausesModified, agreementClauseGroupModified;

                            clauses = action = keywords = agreementClausesIncluded = agreementClauseGroupIncluded = agreementClausesModified = agreementClauseGroupModified = string.Empty;

                            Guid? agreementId = null;

                            if (agreementClause.ContainsKey(AgreementClauseConstants.ACTION))
                            {
                                var actionObj = JsonConvert.DeserializeObject<Dictionary<string, object>>(agreementClause[AgreementClauseConstants.ACTION]?.ToString());
                                if (actionObj != null)
                                {
                                    action = actionObj[Constants.FIELD_VALUE] != null ? actionObj[Constants.FIELD_VALUE].ToString() : string.Empty;
                                }
                            }
                            if (agreementClause.ContainsKey(TemplateConstants.OBJ_ALIAS))
                            {
                                var template = JsonConvert.DeserializeObject<Dictionary<string, object>>(agreementClause[TemplateConstants.OBJ_ALIAS]?.ToString());

                                if (template != null)
                                {
                                    clauses = template.ContainsKey(Constants.FIELD_NAME) ? template[Constants.FIELD_NAME]?.ToString() : null;
                                    keywords = template.ContainsKey(AgreementClauseConstants.KEYWORDS) ? template[AgreementClauseConstants.KEYWORDS]?.ToString() : null;
                                }
                            }

                            if (agreementClause.ContainsKey(AgreementObjectConstants.OBJ_ALIAS))
                            {
                                var agreement = JsonConvert.DeserializeObject<Dictionary<string, object>>(agreementClause[AgreementObjectConstants.OBJ_ALIAS].ToString());

                                if (agreement != null)
                                {
                                    agreementClausesIncluded = agreement.ContainsKey(AgreementObjectConstants.EXT_AGREEMENTCLAUSESINCLUDED) && agreement[AgreementObjectConstants.EXT_AGREEMENTCLAUSESINCLUDED] != null ? agreement[AgreementObjectConstants.EXT_AGREEMENTCLAUSESINCLUDED].ToString() : string.Empty;
                                    agreementClauseGroupIncluded = agreement.ContainsKey(AgreementObjectConstants.EXT_AGREEMENTCLAUSEGROUPINCLUDED) && agreement[AgreementObjectConstants.EXT_AGREEMENTCLAUSEGROUPINCLUDED] != null ? agreement[AgreementObjectConstants.EXT_AGREEMENTCLAUSEGROUPINCLUDED].ToString() : string.Empty;

                                    agreementClausesModified = agreement.ContainsKey(AgreementObjectConstants.EXT_AGREEMENTCLAUSESMODIFIED) && agreement[AgreementObjectConstants.EXT_AGREEMENTCLAUSESMODIFIED] != null ? agreement[AgreementObjectConstants.EXT_AGREEMENTCLAUSESMODIFIED].ToString() : string.Empty;
                                    agreementClauseGroupModified = agreement.ContainsKey(AgreementObjectConstants.EXT_AGREEMENTCLAUSEGROUPMODIFIED) && agreement[AgreementObjectConstants.EXT_AGREEMENTCLAUSEGROUPMODIFIED] != null ? agreement[AgreementObjectConstants.EXT_AGREEMENTCLAUSEGROUPMODIFIED].ToString() : string.Empty;

                                    agreementId = new Guid(agreement[Constants.FIELD_ID].ToString());
                                }
                            }

                            if (action == AgreementClauseConstants.ACTION_INSERTED)
                            {
                                if (clauses != null)
                                {
                                    agreementClausesIncluded += Constants.CLAUSE_SEPARATOR + clauses;
                                    agreementClausesIncluded = agreementClausesIncluded?.TrimStart(',').TrimEnd(',');
                                }

                                agreementClausesIncluded = string.Join(Constants.CLAUSE_SEPARATOR.ToString(), agreementClausesIncluded.Split(new string[] { Constants.CLAUSE_SEPARATOR }, StringSplitOptions.None)?.Distinct()?.OrderBy(clause => clause)?.ToArray());

                                if (keywords != null)
                                {
                                    agreementClauseGroupIncluded += Constants.CLAUSE_SEPARATOR + string.Join(Constants.CLAUSE_SEPARATOR.ToString(), keywords.Split(Constants.CHAR_COMMA).Where(group => group.StartsWith((Constants.CHAR_I.ToString() + Constants.CHAR_HYPHEN.ToString()))).Select(group => group.Replace((Constants.CHAR_I.ToString() + Constants.CHAR_HYPHEN.ToString()), string.Empty)).ToArray());
                                    agreementClauseGroupIncluded = agreementClauseGroupIncluded?.TrimStart(',').TrimEnd(',');
                                }

                                agreementClauseGroupIncluded = string.Join(Constants.CLAUSE_SEPARATOR.ToString(), agreementClauseGroupIncluded?.Split(new string[] { Constants.CLAUSE_SEPARATOR }, StringSplitOptions.None).Distinct().OrderBy(group => group).ToArray());
                                agreementClauseGroupIncluded = agreementClauseGroupIncluded + Constants.CLAUSE_SEPARATOR;
                                agreements.Add(new
                                {
                                    ext_AgreementClausesIncluded = agreementClausesIncluded,
                                    ext_AgreementClauseGroupIncluded = agreementClauseGroupIncluded,
                                    Id = agreementId
                                });
                            }
                            else if (action == AgreementClauseConstants.ACTION_MODIFIED)
                            {
                                if (clauses != null)
                                {
                                    agreementClausesModified += Constants.CLAUSE_SEPARATOR + clauses;
                                    agreementClausesModified = agreementClausesModified?.TrimStart(',').TrimEnd(',');
                                }
                                agreementClausesModified = string.Join(Constants.CLAUSE_SEPARATOR.ToString(), agreementClausesModified.Split(new string[] { Constants.CLAUSE_SEPARATOR }, StringSplitOptions.None).Distinct().OrderBy(clause => clause).ToArray());

                                if (keywords != null)
                                {
                                    agreementClauseGroupModified += Constants.CLAUSE_SEPARATOR + 
                                        string.Join(Constants.CLAUSE_SEPARATOR.ToString(), keywords.Split(Constants.CHAR_COMMA)
                                        .Where(group => group.StartsWith((Constants.CHAR_M.ToString() + Constants.CHAR_HYPHEN.ToString()))).Select(group => group.Replace((Constants.CHAR_M.ToString() + Constants.CHAR_HYPHEN.ToString()), string.Empty)).ToArray());
                                    //agreementClauseGroupModified = agreementClauseGroupModified;
                                }
                                agreementClauseGroupModified = string.Join(Constants.CLAUSE_SEPARATOR.ToString(), agreementClauseGroupModified.Split(new string[] { Constants.CLAUSE_SEPARATOR }, StringSplitOptions.None).Distinct().OrderBy(group => group).ToArray());
                                agreementClauseGroupModified = agreementClauseGroupModified + Constants.CLAUSE_SEPARATOR;
                                agreements.Add(new
                                {
                                    ext_AgreementClausesModified = agreementClausesModified,
                                    ext_AgreementClauseGroupModified = agreementClauseGroupModified,
                                    Id = agreementId
                                });
                            }
                            else if (action == AgreementClauseConstants.ACTION_DELETED)
                            {
                                if (clauses != null)
                                {
                                    agreementClausesModified += Constants.CLAUSE_SEPARATOR + clauses;
                                    agreementClausesModified = agreementClausesModified?.TrimStart(',').TrimEnd(',');
                                }
                                agreementClausesModified = string.Join(Constants.CLAUSE_SEPARATOR.ToString(), agreementClausesModified.Split(new string[] { Constants.CLAUSE_SEPARATOR }, StringSplitOptions.None).Distinct().OrderBy(clause => clause).ToArray());
                                if (keywords != null)
                                {
                                    agreementClauseGroupModified += Constants.CLAUSE_SEPARATOR + string.Join(Constants.CLAUSE_SEPARATOR.ToString(), keywords.Split(Constants.CHAR_COMMA).Where(group => group.StartsWith((Constants.CHAR_M.ToString() + Constants.CHAR_HYPHEN.ToString()))).Select(group => group.Replace((Constants.CHAR_M.ToString() + Constants.CHAR_HYPHEN.ToString()), string.Empty)).ToArray());
                                    agreementClauseGroupModified = agreementClauseGroupModified?.TrimStart(',').TrimEnd(',');
                                }
                                agreementClauseGroupModified = string.Join(Constants.CLAUSE_SEPARATOR.ToString(), agreementClauseGroupModified.Split(new string[] { Constants.CLAUSE_SEPARATOR }, StringSplitOptions.None).Distinct().OrderBy(group => group).ToArray());
                                agreementClauseGroupModified = agreementClauseGroupModified + Constants.CLAUSE_SEPARATOR;
                                agreements.Add(new
                                {
                                    ext_AgreementClausesModified = agreementClausesModified,
                                    ext_AgreementClauseGroupModified = agreementClauseGroupModified,
                                    Id = agreementId
                                });
                            }

                            var updateResponse = Utilities.Update(agreements, new RequestConfigModel
                            {
                                objectName = Constants.OBJ_AGREEMENT,
                                resolvedID = agreementId.ToString(),
                                accessToken = AuthToken
                            });

                            if (updateResponse != null)
                            {
                                return updateResponse.Content.ReadAsStringAsync().Result;
                            }
                        }

                    }
                }

            }
            catch (Exception exc)
            {
                string error = exc.Message;

                if (exc.InnerException != null)
                {
                    error += Environment.NewLine + exc.InnerException.Message;
                }
                return error;
            }
            return Constants.MSG_SUCCESS;
        }
        #endregion
    }
}
