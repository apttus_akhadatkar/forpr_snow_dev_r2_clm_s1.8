﻿/****************************************************************************************
@Name: QuoteDefaultRepository.cs
@Author: Vijay Kiran
@CreateDate: 27 Oct 2017
@Description: All required functions to default account location on quote
@UsedBy: This will be used by APII(DefaultAccountLocationController) to default the ShipToLocation and BillToLocation on Quote

@ModifiedBy: 
@ModifiedDate: 
@ChangeDescription: 
*****************************************************************************************/
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Apttus.SNowPS.Model;
using Apttus.SNowPS.Common;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Script.Serialization;

namespace Apttus.SNowPS.Repository
{
    public class QuoteDefaultRepository
    {
        public static QuoteModel GetQuote(string quoteId, string accessToken)
        {
            QuoteModel quote = new QuoteModel();
            try
            {
                if (!String.IsNullOrEmpty(quoteId))
                {
                    //Update Address
                    var reqConfig = new RequestConfigModel();
                    reqConfig.accessToken = accessToken;
                    reqConfig.objectName = Constants.OBJ_QUOTE;
                    reqConfig.select = new string[] { Constants.FIELD_NAME, Constants.FIELD_ID, Constants.FIELD_EXT_SALESTYPE, Constants.FIELD_ACCOUNTID, Constants.FIELD_SALESPARTNER, Constants.FIELD_EXT_BILLTOLOCATION,Constants.FIELD_EXT_SHIPTOLOCATION};

                    RequestModel requestModel = new RequestModel();
                    requestModel.logicalExpression = null;
                    requestModel.select = reqConfig.select;
                    requestModel.filter = new List<FilterModel>();

                    reqConfig.apttusFilterField = Constants.FIELD_ID;
                    string[] filterValue = new string[] { quoteId.ToString() };
                    requestModel.filter.Add(new FilterModel { field = reqConfig.apttusFilterField, @operator = Constants.OPR_EQUAL, value = filterValue });

                    var jsonQuery = new JavaScriptSerializer().Serialize(requestModel);
                    var responseString = Utilities.GetSearch(jsonQuery, reqConfig);

                    if (responseString.IsSuccessStatusCode)
                    {
                        string responseJSON = responseString.Content.ReadAsStringAsync().Result.ToString();
                        ResponseModel jsonResp = new JavaScriptSerializer().Deserialize<ResponseModel>(responseJSON);
                        quote = MapRespToQuote(jsonResp.SerializedResultEntities, accessToken);
                    }
                }
                return quote;
            }

            catch (Exception ex)
            {
                throw;
            }
        }


        public static List<AccountLocationModel> GetAccount(string accountId, string accessToken)
        {
            List<AccountLocationModel> accountLocation = new List<AccountLocationModel>();
            try
            {
                if (!String.IsNullOrEmpty(accountId))
                {
                    //Update Address
                    var reqConfig = new RequestConfigModel();
                    reqConfig.accessToken = accessToken;
                    reqConfig.objectName = Constants.OBJ_ACCOUNTLOCATION;
                    reqConfig.select = new string[] { Constants.FIELD_NAME, Constants.FIELD_ID, Constants.FIELD_ACCOUNTID,Constants.FIELD_EXT_PRIMARYSHIPTO,Constants.FIELD_EXT_PRIMARYBILLTO};

                    RequestModel requestModel = new RequestModel();
                    requestModel.logicalExpression = "((1 AND 2 AND 3) OR(1 AND(2 OR 3)))";
                    requestModel.select = reqConfig.select;
                    requestModel.filter = new List<FilterModel>();
                    //string accid = Constants.FIELD_ACCOUNTID + "." + Constants.FIELD_ID;
                    reqConfig.apttusFilterField = Constants.FIELD_ACCOUNTID + "." + Constants.FIELD_ID;
                    string[] filterValue = new string[] { accountId.ToString() };
                    requestModel.filter.Add(new FilterModel { field = reqConfig.apttusFilterField, @operator = Constants.OPR_EQUAL, value = filterValue });

                    reqConfig.apttusFilterField = Constants.FIELD_EXT_PRIMARYBILLTO;
                    filterValue = new string[] { Constants.MISC_TRUE };
                    requestModel.filter.Add(new FilterModel { field = reqConfig.apttusFilterField, @operator = Constants.OPR_EQUAL, value = filterValue });

                    reqConfig.apttusFilterField = Constants.FIELD_EXT_PRIMARYSHIPTO;
                    filterValue = new string[] { Constants.MISC_TRUE };
                    requestModel.filter.Add(new FilterModel { field = reqConfig.apttusFilterField, @operator = Constants.OPR_EQUAL, value = filterValue });



                    //reqConfig.apttusFilterField1 = Constants.FIELD_EXT_PRIMARYSHIPTO;
                    //string[] filterValue = new string[] { accountId.ToString() };

                    var jsonQuery = new JavaScriptSerializer().Serialize(requestModel);
                    var responseString = Utilities.GetSearch(jsonQuery, reqConfig);

                    if (responseString.IsSuccessStatusCode)
                    {
                        string responseJSON = responseString.Content.ReadAsStringAsync().Result.ToString();
                        ResponseModel jsonResp = new JavaScriptSerializer().Deserialize<ResponseModel>(responseJSON);
                        accountLocation = MapRespToAccountLocation(jsonResp.SerializedResultEntities, accessToken);
                    }

                }
                return accountLocation;
            }

            catch (Exception ex)
            {
                throw;
            }
        }


        public static List<AccountLocationModel> MapRespToAccountLocation(dynamic[] deserializedEntity, string accessToken)
        {
            List<AccountLocationModel> ListaccountLocation = new List<AccountLocationModel>();
            
            foreach (dynamic obj in deserializedEntity)
            {
                AccountLocationModel accountLocation = new AccountLocationModel();
                accountLocation.Id = obj[Constants.FIELD_ID];
                accountLocation.Name = obj[Constants.FIELD_NAME];
                accountLocation.AccountId = new LookUp();
                if (obj[Constants.FIELD_ACCOUNTID] != null)
                    accountLocation.AccountId.Id = obj[Constants.FIELD_ACCOUNTID][Constants.FIELD_ID];

                if(obj[Constants.FIELD_EXT_PRIMARYBILLTO]!=null)
                    accountLocation.ext_PrimaryBillTo = Convert.ToString(obj[Constants.FIELD_EXT_PRIMARYBILLTO]);
                else
                    accountLocation.ext_PrimaryBillTo = Constants.MISC_FALSE;

                if (obj[Constants.FIELD_EXT_PRIMARYSHIPTO] != null)
                    accountLocation.ext_PrimaryShipTo = Convert.ToString(obj[Constants.FIELD_EXT_PRIMARYSHIPTO]);
                else
                    accountLocation.ext_PrimaryShipTo = Constants.MISC_FALSE;

                ListaccountLocation.Add(accountLocation);
                
            }
            return ListaccountLocation;


        }

        public static HttpResponseMessage UpdateQuote(string id, string accountId, string shipToAccount, string billTotLocation, string accessToken)
        {
            HttpResponseMessage respMsg = new HttpResponseMessage();
            // Creating body for UpsertAPI call
            List<Dictionary<string, object>> listQuote = new List<Dictionary<string, object>>();
            Dictionary<string, object> quotes = new Dictionary<string, object>();
            quotes.Add(Constants.FIELD_ID, id);
            quotes.Add(Constants.FIELD_EXT_INVOICEACCOUNT, accountId);
            quotes.Add(Constants.FIELD_SHIPTOACCOUNT, accountId);
            quotes.Add(Constants.FIELD_EXT_SHIPTOLOCATION, shipToAccount);
            quotes.Add(Constants.FIELD_BILLTOADDRESS, billTotLocation);
            listQuote.Add(quotes);

            var reqConfig = new RequestConfigModel();
            reqConfig.accessToken = accessToken;
            reqConfig.objectName = Constants.OBJ_QUOTE;
            respMsg = Utilities.Upsert(listQuote, reqConfig, false);
            return respMsg;
        }

        public static QuoteModel MapRespToQuote(dynamic[] deserializedEntity, string accessToken)
        {
            QuoteModel quote = new QuoteModel();
            foreach (dynamic obj in deserializedEntity)
            {
                quote.Id = obj[Constants.FIELD_ID];
                quote.Name = obj[Constants.FIELD_NAME];
                if (obj[Constants.FIELD_EXT_SALESTYPE] != null)
                    quote.salestype = obj[Constants.FIELD_EXT_SALESTYPE][Constants.FIELD_VALUE];

                quote.AccountId = new LookUp();
                if (obj[Constants.FIELD_ACCOUNTID] != null)
                    quote.AccountId.Id = obj[Constants.FIELD_ACCOUNTID][Constants.FIELD_ID];

                quote.SalesPartner = new LookUp();
                if (obj[Constants.FIELD_SALESPARTNER] != null)
                    quote.SalesPartner.Id = obj[Constants.FIELD_SALESPARTNER][Constants.FIELD_ID];

                if(obj[Constants.FIELD_EXT_BILLTOLOCATION]!=null)
                    quote.BillToLocation = obj[Constants.FIELD_EXT_BILLTOLOCATION][Constants.FIELD_ID];

                if (obj[Constants.FIELD_EXT_SHIPTOLOCATION] != null)
                    quote.ShipToLocation = obj[Constants.FIELD_EXT_SHIPTOLOCATION][Constants.FIELD_ID];

            }
            return quote;
        }
    }
}
