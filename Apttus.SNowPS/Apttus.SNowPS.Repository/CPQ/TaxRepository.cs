﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Apttus.SNowPS.Model;
using Apttus.SNowPS.Common;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Script.Serialization;

namespace Apttus.SNowPS.Respository
{
    public static class TaxRepository
    {
        public static QuoteModel GetQuote(string quoteId, string accessToken)
        {
            QuoteModel quote = null;
            try
            {                
                if (!String.IsNullOrEmpty(quoteId))
                {
                    //Update Address
                    var reqConfig = new RequestConfigModel();
                    reqConfig.accessToken = accessToken;
                    reqConfig.objectName = Constants.OBJ_QUOTE;
                    reqConfig.select = new string[] { Constants.FIELD_NAME, Constants.FIELD_ID, Constants.FIELD_EXT_SELLINGENTITY, Constants.FIELD_EXT_SHIPTOLOCATION, Constants.FIELD_EXT_SHIPTOCOUNTRY, Constants.FIELD_EXT_SHIPTOSTATE, Constants.FIELD_EXT_TAXEXEMPT };

                    RequestModel requestModel = new RequestModel();
                    requestModel.logicalExpression = null;
                    requestModel.select = reqConfig.select;
                    requestModel.filter = new List<FilterModel>();

                    reqConfig.apttusFilterField = Constants.FIELD_ID;// "QuoteId"; //Territory MDM ID
                    string[] filterValue = new string[] { quoteId.ToString() };
                    requestModel.filter.Add(new FilterModel { field = reqConfig.apttusFilterField, @operator = Constants.OPR_EQUAL, value = filterValue });                   

                    var jsonQuery = new JavaScriptSerializer().Serialize(requestModel);
                    var responseString = Utilities.GetSearch(jsonQuery, reqConfig);

                    if (responseString.IsSuccessStatusCode)
                    {
                        quote = new QuoteModel();
                        string responseJSON = responseString.Content.ReadAsStringAsync().Result.ToString();
                        ResponseModel jsonResp = new JavaScriptSerializer().Deserialize<ResponseModel>(responseJSON);
                        quote = MapRespToQuote(jsonResp.SerializedResultEntities, accessToken);
                    }
                }
                return quote;
            }

            catch (Exception ex)
            {
                throw;
            }
        }
        public static List<QuoteLineItem> GetQuoteLineItems(string quoteId, string accessToken)
        {
            List<QuoteLineItem> lstQLIems = new List<QuoteLineItem>();
            try
            {
                if (!String.IsNullOrEmpty(quoteId))
                {
                    //Update Address
                    var reqConfig = new RequestConfigModel();
                    reqConfig.accessToken = accessToken;
                    reqConfig.objectName = Constants.OBJ_CPQ_QUOTELINEITEM;                 
                    //reqConfig.select = new string[] { Constants.FIELD_NAME, Constants.FIELD_ID,Constants.FIELD_EXT_CONTRIBUTESTO, Constants.FIELD_PRODUCTID, Constants.FIELD_EXT_TCV, Constants.FIELD_LINETYPE};
                    reqConfig.select = new string[] { Constants.FIELD_NAME, Constants.FIELD_ID, Constants.FIELD_PRODUCTID, Constants.FIELD_EXT_TCV, Constants.FIELD_NETPRICE, Constants.FIELD_LINETYPE };

                    RequestModel requestModel = new RequestModel();
                    requestModel.logicalExpression = null;
                    requestModel.select = reqConfig.select;
                    requestModel.filter = new List<FilterModel>();

                    reqConfig.apttusFilterField = Constants.FIELD_QUOTEID;// "QuoteId"; //Territory MDM ID
                    string[] filterValue = new string[] { quoteId.ToString() };
                    requestModel.filter.Add(new FilterModel { field = reqConfig.apttusFilterField, @operator = Constants.OPR_EQUAL, value = filterValue });

                    reqConfig.apttusFilterField = Constants.FIELD_LINETYPE;// "QuoteId"; //Territory MDM ID
                    filterValue = new string[] { Constants.MISC_PRODUCT_SERVICE };
                    requestModel.filter.Add(new FilterModel { field = reqConfig.apttusFilterField, @operator = Constants.OPR_EQUAL, value = filterValue });

                    var jsonQuery = new JavaScriptSerializer().Serialize(requestModel);                   
                    var responseString = Utilities.GetSearch(jsonQuery, reqConfig);
                   
                    if (responseString.IsSuccessStatusCode)
                    {
                        string responseJSON = responseString.Content.ReadAsStringAsync().Result.ToString();
                        ResponseModel jsonResp = new JavaScriptSerializer().Deserialize<ResponseModel>(responseJSON);
                        lstQLIems = MapRespToQLI(jsonResp.SerializedResultEntities, accessToken);
                    }                                        
                }
                return lstQLIems;
            }

            catch (Exception ex)
            {
                throw;
            }
        }

        public static Product GetProductFamily(string productId, string accessToken)
        {
            Product product = new Product();
            try
            {
                if (!String.IsNullOrEmpty(productId))
                {                    
                    var reqConfig = new RequestConfigModel();
                    reqConfig.accessToken = accessToken;
                    reqConfig.objectName = Constants.OBJ_PRODUCT;                   
                    reqConfig.select = new string[] { Constants.FIELD_NAME,Constants.FIELD_ID,Constants.FIELD_EXT_FAMILY };                    
                    reqConfig.apttusFilterField = Constants.FIELD_ID; //Territory MDM ID

                    RequestModel requestModel = new RequestModel();
                    requestModel.logicalExpression = null;
                    requestModel.select = reqConfig.select;
                    requestModel.filter = new List<FilterModel>();
                    
                    string[] filterValue = new string[] { productId.ToString() };
                    requestModel.filter.Add(new FilterModel { field = reqConfig.apttusFilterField, @operator = Constants.OPR_EQUAL, value = filterValue });

                    var jsonQuery = new JavaScriptSerializer().Serialize(requestModel);
                    var responseString = Utilities.GetSearch(jsonQuery, reqConfig);

                    if (responseString.IsSuccessStatusCode)
                    {
                        string responseJSON = responseString.Content.ReadAsStringAsync().Result.ToString();
                        ResponseModel jsonResp = new JavaScriptSerializer().Deserialize<ResponseModel>(responseJSON);
                        product = MapRespToProduct(jsonResp.SerializedResultEntities);
                    }                    
                }
                return product;
            }

            catch (Exception ex)
            {
                throw;
            }
        }

        public static AccountLocationModel GetShipToAccountLocation(string shipToAccountLocationId, string accessToken)
        {
            AccountLocationModel shipToAccountLocation = new AccountLocationModel();
            try
            {
                if(!String.IsNullOrEmpty(shipToAccountLocationId))
                {
                    var reqConfig = new RequestConfigModel();
                    reqConfig.accessToken = accessToken;
                    reqConfig.objectName = Constants.OBJ_ACCOUNTLOCATION;
                    reqConfig.select = new string[] { Constants.FIELD_ID, Constants.FIELD_STATE,Constants.FIELD_COUNTRY };
                    reqConfig.apttusFilterField = Constants.FIELD_ID; //Territory MDM ID

                    RequestModel requestModel = new RequestModel();
                    requestModel.logicalExpression = null;
                    requestModel.select = reqConfig.select;
                    requestModel.filter = new List<FilterModel>();

                    string[] filterValue = new string[] { shipToAccountLocationId.ToString() };
                    requestModel.filter.Add(new FilterModel { field = reqConfig.apttusFilterField, @operator = Constants.OPR_EQUAL, value = filterValue });

                    var jsonQuery = new JavaScriptSerializer().Serialize(requestModel);
                    var responseString = Utilities.GetSearch(jsonQuery, reqConfig);

                    if (responseString.IsSuccessStatusCode)
                    {
                        string responseJSON = responseString.Content.ReadAsStringAsync().Result.ToString();
                        ResponseModel jsonResp = new JavaScriptSerializer().Deserialize<ResponseModel>(responseJSON);
                        shipToAccountLocation = MapRespToAccountLocation(jsonResp.SerializedResultEntities);
                    }
                }

                return shipToAccountLocation;
            }
            catch(Exception ex)
            {
                throw;
            }
        }

        public static decimal GetTaxRate(string contributesTo, string sellingEntity,string country, string state, string taxType, string accessToken)
        {
            try
            {
                string productCategory = String.Empty;
                //if (productCategory != null)
                //{
                //    if (contributesTo.ToLower() == Constants.SUBSCRIPTION_LICENSE_REVENUE.ToLower() || contributesTo.ToLower() == Constants.LICENSE_REVENUE.ToLower())
                //        productCategory = Constants.SUBSCRIPTION;
                //    else if (contributesTo.ToLower() == Constants.TRAINING_REVENUE.ToLower())
                //        productCategory = Constants.EDUCATION;
                //    else if(contributesTo.ToLower() == Constants.TOTAL_PS_REVENUE.ToLower())
                //        productCategory = Constants.SERVICES;
                //}

                if (contributesTo != null)
                {
                    if (contributesTo.Contains(Constants.SUBSCRIPTION))
                        productCategory = Constants.SUBSCRIPTION;
                    else if (contributesTo.Contains(Constants.EDUCATION))
                        productCategory = Constants.EDUCATION;
                    else if (contributesTo.Contains(Constants.SERVICES))
                        productCategory = Constants.SERVICES;
                }
                else productCategory = Constants.SUBSCRIPTION;                

                if (!String.IsNullOrEmpty(productCategory))
                {                    
                    var reqConfig = new RequestConfigModel();
                    reqConfig.accessToken = accessToken;
                    reqConfig.objectName = Constants.OBJ_EXT_SALESTAXRATES;                    
                    reqConfig.select = new string[] { Constants.FIELD_EXT_PRODUCTCATEGORY, Constants.FIELD_EXT_TAXTYPE, Constants.FIELD_EXT_TAXRATENEW };                    
                    reqConfig.apttusFilterField = Constants.FIELD_QUOTEID;

                    RequestModel requestModel = new RequestModel();
                    requestModel.logicalExpression = null;
                    requestModel.select = reqConfig.select;
                    requestModel.filter = new List<FilterModel>();
                    
                    string[] filterValue = new string[] { productCategory.ToString() };
                    requestModel.filter.Add(new FilterModel { field = Constants.FIELD_EXT_PRODUCTCATEGORY, @operator = Constants.OPR_EQUAL, value = filterValue });
                    filterValue = new string[] { taxType.ToString() };
                    requestModel.filter.Add(new FilterModel { field = Constants.FIELD_EXT_TAXTYPE, @operator = Constants.OPR_EQUAL, value = filterValue });

                    if(sellingEntity!=null)
                    {
                        filterValue = new string[] { sellingEntity };
                        requestModel.filter.Add(new FilterModel { field = Constants.FIELD_EXT_SELLINGENTITY, @operator = Constants.OPR_EQUAL, value = filterValue });
                    }

                    if (!String.IsNullOrEmpty(country))
                    {
                        filterValue = new string[] { country };
                        requestModel.filter.Add(new FilterModel { field = Constants.FIELD_EXT_COUNTRY, @operator = Constants.OPR_EQUAL, value = filterValue });
                    }

                    if (!String.IsNullOrEmpty(state))
                    {
                        filterValue = new string[] { state };
                        requestModel.filter.Add(new FilterModel { field = Constants.FIELD_EXT_STATE, @operator = Constants.OPR_EQUAL, value = filterValue });
                    }

                    var jsonQuery = new JavaScriptSerializer().Serialize(requestModel);
                    var responseString = Utilities.GetSearch(jsonQuery, reqConfig);

                    decimal taxRate = 0;
                    if (responseString.IsSuccessStatusCode)
                    {
                        string responseJSON = responseString.Content.ReadAsStringAsync().Result.ToString();
                        ResponseModel jsonResp = new JavaScriptSerializer().Deserialize<ResponseModel>(responseJSON);
                        if (jsonResp.SerializedResultEntities != null && jsonResp.SerializedResultEntities.Count() > 0)
                        {
                            if (!String.IsNullOrEmpty(country) && !String.IsNullOrEmpty(state) && !String.IsNullOrEmpty(sellingEntity))
                                taxRate = jsonResp.SerializedResultEntities.FirstOrDefault()[Constants.FIELD_EXT_TAXRATENEW];

                            //else
                            ////taxRate = jsonResp.SerializedResultEntities[1][Constants.FIELD_EXT_TAXRATE];
                            //{
                            //    var tax = jsonResp.SerializedResultEntities.Where(s => s[Constants.FIELD_EXT_TAXRATE] != 0).FirstOrDefault(); ;//[Constants.FIELD_EXT_TAXRATE];
                            //    if (tax != null)
                            //    {
                            //        taxRate = tax[Constants.FIELD_EXT_TAXRATE];
                            //    }

                            //    else
                            //        taxRate = jsonResp.SerializedResultEntities.FirstOrDefault()[Constants.FIELD_EXT_TAXRATE];
                            //}
                        }
                    }                    
                    
                    return taxRate;                    
                }

                else
                    return 0;
            }

            catch (Exception ex)
            {
                throw;
            }
        }

        private static QuoteModel MapRespToQuote(dynamic[] deserializedEntity, string accessToken)
        {
            QuoteModel quote = new QuoteModel();
            foreach (dynamic obj in deserializedEntity)
            {
                quote.Id = obj[Constants.FIELD_ID];
                quote.Name = obj[Constants.FIELD_NAME];
                if (obj[Constants.FIELD_EXT_SELLINGENTITY] != null)
                    quote.SellingEntity = obj[Constants.FIELD_EXT_SELLINGENTITY][Constants.FIELD_ID];
                if (obj[Constants.FIELD_EXT_SHIPTOCOUNTRY] != null)
                    quote.SellingEntity = obj[Constants.FIELD_EXT_SHIPTOCOUNTRY][Constants.FIELD_ID];
                if (obj[Constants.FIELD_EXT_SHIPTOSTATE] != null)
                    quote.SellingEntity = obj[Constants.FIELD_EXT_SHIPTOSTATE][Constants.FIELD_ID];
                if (obj[Constants.FIELD_EXT_TAXEXEMPT] != null)
                    quote.TaxExempt = obj[Constants.FIELD_EXT_TAXEXEMPT];
                if (obj[Constants.FIELD_EXT_SHIPTOLOCATION] != null)
                    quote.ShipToLocation = obj[Constants.FIELD_EXT_SHIPTOLOCATION][Constants.FIELD_ID];
            }
            return quote;
        }
        private static List<QuoteLineItem> MapRespToQLI(dynamic[] deserializedEntity, string accessToken)
        {
            List<QuoteLineItem> qLineItems = new List<QuoteLineItem>();
            foreach (dynamic obj in deserializedEntity)
            {
                QuoteLineItem qLitem = new QuoteLineItem();
                qLitem.Id = obj[Constants.FIELD_ID];
                qLitem.Name = obj[Constants.FIELD_NAME];
                qLitem.TCV = obj[Constants.FIELD_EXT_TCV];
                qLitem.NetPrice = obj[Constants.FIELD_NETPRICE];
                if (obj[Constants.FIELD_PRODUCTID] != null)
                    qLitem.ProductId = obj[Constants.FIELD_PRODUCTID][Constants.FIELD_ID];
                //qLitem.ContributesTo = obj[Constants.FIELD_EXT_CONTRIBUTESTO];
                qLineItems.Add(qLitem);
            }
            return qLineItems;
        }

        private static Product MapRespToProduct(dynamic[] deserializedEntity)
        {            
            Product product = new Product();
            deserializedEntity.ToList().ForEach(x =>
            {
                product.Id = x[Constants.FIELD_ID];
                product.Name = x[Constants.FIELD_NAME];
                                                       
                if (x[Constants.FIELD_EXT_FAMILY] != null)
                {
                    product.ExtFamily = new SelectOption();
                    product.ExtFamily.Key = x[Constants.FIELD_EXT_FAMILY][Constants.FIELD_KEY];
                    product.ExtFamily.Value = x[Constants.FIELD_EXT_FAMILY][Constants.FIELD_VALUE];
                }
            });
            return product;
        }

        private static AccountLocationModel MapRespToAccountLocation(dynamic[] deserializedEntity)
        {
            AccountLocationModel accountLocation = new AccountLocationModel();
            deserializedEntity.ToList().ForEach(x =>
            {
                accountLocation.Id = x[Constants.FIELD_ID];
                //accountLocation.Name = x[Constants.FIELD_NAME];
                accountLocation.State = x[Constants.FIELD_STATE];
                accountLocation.Country = x[Constants.FIELD_COUNTRY];
            });
            return accountLocation;
        }
    }
}