﻿/*************************************************************
@Name: CartLineItemRepository.cs
@Author: Chirag Modi
@CreateDate: 11-Oct-2017
@Description: This class contains Get, Update & calculate methods and logic for Financial fields calculations.
@UsedBy: Description of how this class is being used
******************************************************************
@ModifiedBy: Author who modified this class
@ModifiedDate: Date the class was modified
@ChangeDescription: A brief description of what was modified
 
 
**** PS: @ModifiedBy and @ChangeDescription does not apply to ‘In Development’ code – should be used for major changes in functionality or subsequent releases.
******************************************************************/
using System;
using System.Collections.Generic;
using System.Linq;
using Apttus.SNowPS.Model;
using Apttus.SNowPS.Common;
using System.Net.Http;
using Newtonsoft.Json.Linq;
using Apttus.DataAccess.Common.Model;
using Apttus.DataAccess.Common.Enums;
using Newtonsoft.Json;
using System.Configuration;

namespace Apttus.SNowPS.Repository.Renewals
{
    public class CartLineItemRepository
    {
        /// <summary>
        /// To Get Cart Line Items data..
        /// </summary>
        /// <param name="request">HttpRequestMessage</param>
        /// <param name="cartLineItems">List of Cart Line Items</param>
        /// <returns>List of Cart Line Items with Updated data</returns>
        public List<CartLineItemModel> GetCartLineItems(string accessToken, string configurationId)
        {
            //Build AQL Query to fetch Cart Line Item and associated Asset Line Item details.

            var query = new Query(Constants.OBJ_CPQ_LINEITEM);
            query.AddColumns(Constants.OBJ_CARTLINEITEM_SELECTFIELDS.Split(Constants.CHAR_COMMA));
            Expression exp = new Expression(ExpressionOperator.AND);
            exp.AddCondition(new Condition(Constants.FIELD_CONFIGURATIONID, FilterOperator.Equal, configurationId));
            query.SetCriteria(exp);

            Join assetLineItem = new Join(Constants.OBJ_CPQ_LINEITEM, Constants.OBJ_CPQ_ASSETLINEITEM, Constants.FIELD_ASSETLINEITEMID, Constants.FIELD_ID, JoinType.LEFT, Constants.OBJALIAS_ASSETLINEITEM);
            query.AddJoin(assetLineItem);

            Join prodConfigJoin = new Join(Constants.OBJ_LINEITEM, Constants.OBJ_PRODUCTCONFIGURATION, Constants.FIELD_CONFIGURATIONID, Constants.FIELD_ID, JoinType.LEFT, Constants.OBJALIAS_PRODUCTCONFIGURATION);
            query.AddJoin(prodConfigJoin);

            Join quoteJoin = new Join(Constants.OBJALIAS_PRODUCTCONFIGURATION, Constants.OBJ_QUOTE, Constants.FIELD_QUOTEID, Constants.FIELD_ID, JoinType.LEFT, Constants.OBJALIAS_QUOTE);
            query.AddJoin(quoteJoin);

            Join agreementJoin = new Join(Constants.OBJALIAS_QUOTE, Constants.OBJ_AGREEMENT, Constants.FIELD_EXT_REFAGREEMENT, Constants.FIELD_ID, JoinType.LEFT, Constants.OBJALIAS_AGREEMENT);
            query.AddJoin(agreementJoin);

            var jsonQuery = query.Serialize();

            var requestConfig = new RequestConfigModel();
            requestConfig.accessToken = accessToken;
            requestConfig.searchType = SearchType.AQL;
            requestConfig.objectName = Constants.OBJ_CPQ_LINEITEM;

            var response = Utilities.Search(jsonQuery, requestConfig);
            var responseStringResult = response.Content.ReadAsStringAsync().Result;
            var cartLineItems = new List<CartLineItemModel>();
            if (response != null && response.IsSuccessStatusCode && !string.IsNullOrEmpty(responseStringResult))
            {
                cartLineItems = JObject.Parse(responseStringResult).SelectToken(Constants.NODE_SERIALIZEDRESULTENTITIES).ToObject<List<CartLineItemModel>>();
            }

            return cartLineItems;
        }

        /// <summary>
        /// Save updated Cart Line Items through API
        /// </summary>
        /// <param name="cartLineItems">List of Cart Line Items</param>
        /// <returns>HttpResponseMessage post save API</returns>
        public HttpResponseMessage UpdateCartLineItems(string accessToken, List<CartLineItemModel> cartLineItems)
        {
            var reqConfig = new RequestConfigModel
            {
                accessToken = accessToken,
                objectName = Constants.OBJ_CPQ_LINEITEM,
                apttusFilterField = Constants.FIELD_ID
            };

            var lstLineItemsToUpdate = new List<dynamic>();
            foreach (var lstLineItems in cartLineItems)
            {
                lstLineItemsToUpdate.Add(new
                {
                    Id = lstLineItems.Id,
                    ext_BlendedPrice = lstLineItems.ext_BlendedPrice,
                    ext_NNACV = lstLineItems.ext_NNACV,
                    ext_RenewalACV = lstLineItems.ext_RenewalACV,
                    ext_TCV = lstLineItems.ext_TCV,
                    ext_flaggedAs1 = lstLineItems.ext_FlaggedAs1,
                    ext_ListPriceWithAdjustments = lstLineItems.ext_ListPriceWithAdjustments,
                    ext_AdjustmentAmount = lstLineItems.ext_AdjustmentAmount,
                    ext_AdjustmentDiscountPercent = lstLineItems.ext_AdjustmentDiscountPercent,
                    ext_AnnualContractValue = lstLineItems.ext_AnnualContractValue,
                    ext_AnnualListPrice = lstLineItems.ext_AnnualListPrice,
                    ext_EstimatedTax = lstLineItems.ext_EstimatedTax,
                    ext_EstimatedTotal = lstLineItems.ext_EstimatedTotal,
                    ext_SalesPrice = lstLineItems.ext_SalesPrice
                });
            }

            var res = Utilities.Update(lstLineItemsToUpdate, reqConfig);
            return res;
        }

        public List<CartLineItemModel> CalculateFinancialFields(List<CartLineItemModel> cartLineItems)
        {
            for (int i = 0; i < cartLineItems.Count; i++)
            {
                //Update Delta quantity.. 
                if (cartLineItems[i].ItemSequence == 1)
                {
                    cartLineItems[i].ext_DeltaQuantity = (cartLineItems[i].Quantity.ConvertToDecimal() - cartLineItems[i].AssetQuantity.ConvertToDecimal());
                }
                else
                {
                    var previousRampLineQuantity = GetPreviousRampLine(cartLineItems[i], cartLineItems).Quantity.ConvertToDecimal();
                    cartLineItems[i].ext_DeltaQuantity = (cartLineItems[i].Quantity.ConvertToDecimal() - previousRampLineQuantity.ConvertToDecimal());
                }

                //Get Scenario Type based on Date Logic..
                var scenarioType = GetScenarioType(cartLineItems[i], cartLineItems);

                if (scenarioType != null && scenarioType.Count > 0)
                {
                    scenarioType.ForEach(scenario => cartLineItems[i].ext_FlaggedAs1 += scenario.GetEnumDisplayName() + ";");
                    cartLineItems[i].ext_FlaggedAs1 = cartLineItems[i].ext_FlaggedAs1.TrimEnd(';');
                }

                //Calculation for Blended Price..
                CalculateBlendedPrice(cartLineItems, cartLineItems[i]);

                //Calculations..
                CalculateACVFields(cartLineItems, cartLineItems[i], scenarioType);

                //Code Appended by Asha as on 08-11-2017
                cartLineItems[i].ext_ListPriceWithAdjustments = cartLineItems[i].ExtendedPrice.ConvertToDouble();

                if (cartLineItems[i].ListPrice != 0)
                {
                    cartLineItems[i].ext_AdjustmentAmount = (cartLineItems[i].ListPrice.ConvertToDouble() * cartLineItems[i].Quantity.ConvertToDouble() * cartLineItems[i].Term) - cartLineItems[i].NetPrice;
                    cartLineItems[i].ext_AdjustmentDiscountPercent = ((cartLineItems[i].ListPrice * cartLineItems[i].Quantity.ConvertToDouble() * cartLineItems[i].Term) - cartLineItems[i].NetPrice) * 100 / (cartLineItems[i].ListPrice.ConvertToDouble() * cartLineItems[i].Quantity.ConvertToDouble() * cartLineItems[i].Term);
                }
                if (cartLineItems[i].ProductId.Id != null)
                {
                    if (cartLineItems[i].ProductId.ext_Family != null && cartLineItems[i].ProductId.ext_Family.Value.ToString().ToLower() == Constants.STR_SUBSCRIPTION)
                    {
                        cartLineItems[i].ext_AnnualContractValue = 0;
                        cartLineItems[i].ext_AnnualListPrice = 0;
                    }
                    else if (cartLineItems[i].SellingFrequency != null && cartLineItems[i].SellingFrequency.Value.ToString().ToLower() == Constants.STR_YEARLY)
                    {
                        cartLineItems[i].ext_AnnualContractValue = cartLineItems[i].NetUnitPrice * cartLineItems[i].Quantity.ConvertToDouble();
                        cartLineItems[i].ext_AnnualListPrice = cartLineItems[i].BasePrice * cartLineItems[i].Quantity.ConvertToDouble();
                    }
                    else
                    {
                        cartLineItems[i].ext_AnnualContractValue = cartLineItems[i].NetUnitPrice * 12 * cartLineItems[i].Quantity.ConvertToDouble();
                        cartLineItems[i].ext_AnnualListPrice = cartLineItems[i].BasePrice * 12 * cartLineItems[i].Quantity.ConvertToDouble();
                    }
                }
                cartLineItems[i].ext_EstimatedTax = cartLineItems[i].ext_EstimatedTax;

                //Estimated Total: Total Value + EstimatedTax 
                cartLineItems[i].ext_EstimatedTotal = cartLineItems[i].NetPrice + cartLineItems[i].ext_EstimatedTax;

                //Sales Price
                cartLineItems[i].ext_SalesPrice = cartLineItems[i].NetUnitPrice;
            }

            return cartLineItems;
        }


        #region Private Methods

        /// <summary>
        /// Private Method: This method contains logic to calculate Blended Price value for current Line Item.
        /// </summary>
        /// <param name="lineItemsInfo">This contains all the details for all the Line Items.</param>
        /// <param name="currentLineItemInfo">This contains all the details for current Line Item.</param>
        private void CalculateBlendedPrice(List<CartLineItemModel> lineItemsInfo, CartLineItemModel currentLineItemInfo)
        {
            if ((currentLineItemInfo.ItemSequence == 1))
            {
                currentLineItemInfo.ext_BlendedPrice = ((currentLineItemInfo.ALI.ext_BlendedPrice.ConvertToDouble() * currentLineItemInfo.AssetQuantity.ConvertToDouble() * 12.00)
                                                                + (currentLineItemInfo.ext_DeltaQuantity.ConvertToDouble() * currentLineItemInfo.NetUnitPrice.ConvertToDouble()))
                                                        / currentLineItemInfo.Quantity.ConvertToDouble();
            }
            else
            {
                var previousRampLine = GetPreviousRampLine(currentLineItemInfo, lineItemsInfo);
                currentLineItemInfo.ext_BlendedPrice = ((previousRampLine.ext_BlendedPrice.ConvertToDouble() * previousRampLine.Quantity.ConvertToDouble() * 12.00)
                                                                + (currentLineItemInfo.ext_DeltaQuantity.ConvertToDouble() * currentLineItemInfo.NetUnitPrice.ConvertToDouble()))
                                                        / currentLineItemInfo.Quantity.ConvertToDouble();
            }
        }

        /// <summary>
        /// Private Method: This method contains logic to calculate NNACV field value for current Line Item.
        /// </summary>
        /// <param name="lineItemsInfo">This contains all the details for all the Line Items.</param>
        /// <param name="currentLineItemInfo">This contains all the details for current Line Item.</param>
        private static void CalculateNNACVFields(List<CartLineItemModel> lineItemsInfo, CartLineItemModel currentLineItemInfo)
        {
            //To check for Ramp-lines..
            if (currentLineItemInfo.ItemSequence == 1)
            {
                currentLineItemInfo.ext_NNACV = ((currentLineItemInfo.NetUnitPrice.ConvertToDouble() * currentLineItemInfo.ext_DeltaQuantity.ConvertToDouble() * 12.00));
                if (currentLineItemInfo.AssetLineItemId != null)
                {
                    currentLineItemInfo.ext_NNACV += ((currentLineItemInfo.AssetQuantity.ConvertToDouble() * (currentLineItemInfo.NetUnitPrice.ConvertToDouble() - currentLineItemInfo.ALI.NetUnitPrice.ConvertToDouble()) * 12));
                }
            }
            else
            {
                var previousRampLine = GetPreviousRampLine(currentLineItemInfo, lineItemsInfo);

                currentLineItemInfo.ext_NNACV = ((currentLineItemInfo.NetUnitPrice.ConvertToDouble() * currentLineItemInfo.ext_DeltaQuantity.ConvertToDouble() * 12.00)
                                                + (previousRampLine.Quantity.ConvertToDouble() * (currentLineItemInfo.NetUnitPrice.ConvertToDouble() - previousRampLine.NetUnitPrice.ConvertToDouble()) * 12));
            }
        }

        /// <summary>
        /// Private Method: This method contains logic to calculate Renewal ACV field value for current Line Item.
        /// </summary>
        /// <param name="lineItemsInfo">This contains all the details for all the Line Items.</param>
        /// <param name="currentLineItemInfo">This contains all the details for current Line Item.</param>
        private static void CalculateRenewalACVFields(List<CartLineItemModel> lineItemsInfo, CartLineItemModel currentLineItemInfo)
        {
            if ((currentLineItemInfo.ItemSequence == 1) && (currentLineItemInfo.AssetLineItemId != null))
            {
                currentLineItemInfo.ext_RenewalACV = (currentLineItemInfo.ALI.NetUnitPrice.ConvertToDouble() * currentLineItemInfo.AssetQuantity.ConvertToDouble() * 12.00);
            }
            else
            {
                var previousRampLine = GetPreviousRampLine(currentLineItemInfo, lineItemsInfo);
                currentLineItemInfo.ext_RenewalACV = ((previousRampLine.Quantity.ConvertToDouble() * previousRampLine.NetUnitPrice.ConvertToDouble() * 12));
            }
        }

        /// <summary>
        /// Private Method: This method contains logic to calculate TCV field value for current Line Item.
        /// </summary>
        /// <param name="lineItemsInfo">This contains all the details for all the Line Items.</param>
        /// <param name="currentLineItemInfo">This contains all the details for current Line Item.</param>
        /// <param name="scenarioType">This contains Scenario Type which is a Flagged As values.</param>
        private static void CalculateTCVFields(List<CartLineItemModel> lineItemsInfo, CartLineItemModel currentLineItemInfo, List<Enums.ScenarioType> scenarioType)
        {
            if (scenarioType.Count == 0)
            {
                currentLineItemInfo.ext_TCV = 0;
            }
            else if (scenarioType.Count == 1 && scenarioType.Contains(Enums.ScenarioType.Upsell) && currentLineItemInfo.AssetLineItemId != null)
            {
                var sellingTerm = Math.Round((currentLineItemInfo.ALI.EndDate.Value - currentLineItemInfo.StartDate).Days / (365.2425 / 12));
                currentLineItemInfo.ext_TCV = (currentLineItemInfo.NetUnitPrice.ConvertToDouble() * currentLineItemInfo.ext_DeltaQuantity.ConvertToDouble() * sellingTerm.ConvertToDouble());
            }
            else
            {
                currentLineItemInfo.ext_TCV = (currentLineItemInfo.NetUnitPrice.ConvertToDouble() * currentLineItemInfo.Quantity.ConvertToDouble() * currentLineItemInfo.SellingTerm.ConvertToDouble());
            }
        }

        /// <summary>
        /// Private Method: This method contains logic to calculate all ACV field values i.e. NNACV, Renewal ACV & TCV for current Line Item.
        /// </summary>
        /// <param name="lineItemsInfo">This contains all the details for all the Line Items.</param>
        /// <param name="currentLineItemInfo">This contains all the details for current Line Item.</param>
        /// <param name="scenarioType">This contains Scenario Type which is a Flagged As values.</param>
        private static void CalculateACVFields(List<CartLineItemModel> lineItemsInfo, CartLineItemModel currentLineItemInfo, List<Enums.ScenarioType> scenarioType)
        {
            //To Do: Check the calculations for NNACV and RenewalACV

            CalculateNNACVFields(lineItemsInfo, currentLineItemInfo);

            if (scenarioType.Contains(Enums.ScenarioType.Renewal))
            {
                CalculateRenewalACVFields(lineItemsInfo, currentLineItemInfo);
            }

            CalculateTCVFields(lineItemsInfo, currentLineItemInfo, scenarioType);
        }


        /// <summary>
        /// Private Method: This method contains logic to calculate Renewal ACV field value for current Line Item.
        /// </summary>
        /// <param name="currentLineItemInfo">This contains all the details for current Line Item.</param>
        /// <param name="lineItemInfo">This contains all the details for all the Line Items.</param> 
        /// <returns>List of Enum values for Scenario Types.</returns>
        private List<Enums.ScenarioType> GetScenarioType(CartLineItemModel currentLineItemInfo, List<CartLineItemModel> lineItemInfo)
        {
            var scenarioTypes = new List<Enums.ScenarioType>();

            bool setSpecificDataCenter = false;
            setSpecificDataCenter = SetSpecificDataCenter(currentLineItemInfo);
            if (setSpecificDataCenter)
            {
                scenarioTypes.Add(Enums.ScenarioType.SpecificDataCenter);
            }

            //For Flagged As = 'First Time Purchase', if all the Line Items have no reference to AssetLineItem..
            var assetLineItemsCount = lineItemInfo.Count(l => l.AssetLineItemId != null);
            if (assetLineItemsCount == 0)
            {
                scenarioTypes.Add(Enums.ScenarioType.FirstTimePurchase);

                return scenarioTypes; //To Do: verify different scenarios for First Time Purchase..
            }

            var assetLineItemEndDate = currentLineItemInfo.ALI.EndDate ?? GetPreviousRampLine(currentLineItemInfo, lineItemInfo)?.EndDate;

            if (currentLineItemInfo.AssetLineItemId == null && !IsPriceReset(currentLineItemInfo, lineItemInfo))
            {
                //For Build-in-upsell..
                //if (currentLineItemInfo.StartDate > lineItemInfo.Except(new List<CartLineItemModel>() { currentLineItemInfo }).ToList().Max(m => m.StartDate))
                var lineItemsExceptCurrent = lineItemInfo.Except(new List<CartLineItemModel> { currentLineItemInfo }).ToList();
                if (lineItemsExceptCurrent.Count > 0 && currentLineItemInfo.StartDate > lineItemsExceptCurrent.Max(m => m.StartDate))
                {
                    scenarioTypes.Add(Enums.ScenarioType.BuildInUpsell);
                }
                //For a Product cross-sell..
                else
                {
                    if (currentLineItemInfo.ext_DeltaQuantity > 0)
                    {
                        scenarioTypes.Add(Enums.ScenarioType.Upsell);
                    }
                    if (currentLineItemInfo.ItemSequence == 1)
                    {
                        scenarioTypes.Add(Enums.ScenarioType.FirstTimePurchase);
                    }
                    if (assetLineItemEndDate != null && currentLineItemInfo.StartDate == assetLineItemEndDate.Value.AddDays(1))
                    {
                        scenarioTypes.Add(Enums.ScenarioType.Renewal);
                    }
                }
                return scenarioTypes;
            }

            //For Upsell with Quantity change..
            if (currentLineItemInfo.ext_DeltaQuantity > 0)
            {
                scenarioTypes.Add(Enums.ScenarioType.Upsell);
            }

            if (assetLineItemEndDate != null)
            {
                //Renewal scenarios.. 
                if (currentLineItemInfo.StartDate == assetLineItemEndDate.Value.AddDays(1))
                {
                    scenarioTypes.Add(Enums.ScenarioType.Renewal);
                }

                //Early Renewal scenario..
                if ((currentLineItemInfo.StartDate < assetLineItemEndDate) && (currentLineItemInfo.EndDate > assetLineItemEndDate))
                {
                    scenarioTypes.Add(Enums.ScenarioType.EarlyRenewal);
                }

                ////Account Consolidation - Renewal
                //if (scenarioTypes.Contains(Enums.ScenarioType.Renewal) && currentLineItemInfo.Quantity == 0 && currentLineItemInfo.StartDate > assetLineItemEndDate && currentLineItemInfo.EndDate > assetLineItemEndDate)
                //{
                //scenarioTypes.Remove(Enums.ScenarioType.Renewal);
                //    scenarioTypes.Add(Enums.ScenarioType.Renewal);
                //}
                ////Account Consolidation - Upsell
                //if (scenarioTypes.Contains(Enums.ScenarioType.Upsell) && currentLineItemInfo.Quantity != 0 && currentLineItemInfo.StartDate > assetLineItemEndDate && currentLineItemInfo.EndDate == assetLineItemEndDate)
                //{
                //     scenarioTypes.Remove(Enums.ScenarioType.Upsell);
                //    scenarioTypes.Add(Enums.ScenarioType.Upsell);
                //}

                //'Early Renewal' & 'Has Replacements' scenarios..
                //If product on positive line is same as product on negative line(or Quantity 0) then mark that as 'Early Renewal' or mark it as 'Has Replacements'.
                //'Superseding' & 'Credit & Replace' scenarios..
                //All negative lines(Or with Quantity 0) will either be marked as 'Credit & Replace' or 'Superseding' based on below scenarios.
                //Whenever there is a negative line(Or with Quantity 0) and a positive line on the cart, it is either a 'Credit & Replace' or 'Superseding'.
                //If LineItem.EnDate is equal to ReferenceAgreement.EndDate, then it is 'Superseding' else it is 'Credit & replace'.
                if (currentLineItemInfo.Quantity == 0 && (GetMaxLineItemEndDate(lineItemInfo) > assetLineItemEndDate))
                {
                    scenarioTypes.Add(Enums.ScenarioType.CreditAndReplace);
                    scenarioTypes.Add(Enums.ScenarioType.HasReplacement);
                }
                else if (currentLineItemInfo.Quantity == 0 && (GetMaxLineItemEndDate(lineItemInfo) == assetLineItemEndDate))
                {
                    scenarioTypes.Add(Enums.ScenarioType.Superseding);
                    scenarioTypes.Add(Enums.ScenarioType.HasReplacement);
                }
            }
            //'Has Price Reset' scenario..
            if (scenarioTypes.Contains(Enums.ScenarioType.Renewal) && (currentLineItemInfo.ItemSequence != 1) && IsPriceReset(currentLineItemInfo, lineItemInfo))
            {
                scenarioTypes.Remove(Enums.ScenarioType.Renewal);
                scenarioTypes.Add(Enums.ScenarioType.HasPriceReset);
            }


            return scenarioTypes;
        }

        //For Specific Data Center
        // When new business happen, at line the instances if the data center is selected as "United States" 
        //Then system will tag the Flaggd AS as "Specific Data Center". 
        //During renewal or upsell if the customer buys additional Prod/Non-prod instance 
        //and if they select the data center and if its falls into the list mention in Enum and is other than "United States", 
        //then system will tag the Flagged AS "Specific Data Center". 0
        //If the user select data center value other than in the list mention in Enum(example, Austrialia)
        //then the Flagged AS is not set to Specific Data Center
        //TODO : check
        //if(currentLineItemInfo.ext_datacenter.Key != lineItemInfo.Any(l => l.ext_datacenter))
        /// <summary>
        /// Private Method: To check if 'Specific Data Center' Flagged As needs to be set.
        /// </summary>
        /// <param name="currentLineItemInfo">This contains all the details for current Line Item.</param>
        /// <returns></returns>
        private bool SetSpecificDataCenter(CartLineItemModel currentLineItemInfo)
        {
            bool setFlag = false;
            try
            {

                var quoteType = (currentLineItemInfo.Q.ext_Type.ConvertToStringNull() != null) ? currentLineItemInfo.Q.ext_Type.Key.ConvertToStringNull() : null;
                var PreviousDataCenter = (currentLineItemInfo.AGR.ext_quoteid.ext_DataCenter.ConvertToStringNull() != null) ? currentLineItemInfo.AGR.ext_quoteid.ext_DataCenter.Value.ConvertToString() : null;
                var CurrentDataCenter = (currentLineItemInfo.ConfigurationId != null && currentLineItemInfo.ConfigurationId.QuoteId != null && currentLineItemInfo.ConfigurationId.QuoteId.ext_DataCenter != null)
                                              ? currentLineItemInfo.ConfigurationId.QuoteId.ext_DataCenter.Value.ConvertToStringNull() : null;

                //if Quote Type = New Business
                if (quoteType == Enums.QuoteType.NewBusiness.GetEnumDisplayName())
                {
                    if (Enum.IsDefined(typeof(Enums.SpecificDataCenter), CurrentDataCenter))
                    {
                        setFlag = true;
                        return setFlag;
                    }
                }
                //If Quote Type = Renewal
                if ((quoteType == Enums.QuoteType.Renewal.GetEnumDisplayName()) || (quoteType == Enums.QuoteType.LicenseUpsell.GetEnumDisplayName()))
                {
                    //If Data Center is different than earlier
                    if (PreviousDataCenter != CurrentDataCenter)
                    {
                        //If Data Center is within Specific Data Center
                        if (Enum.IsDefined(typeof(Enums.SpecificDataCenter), CurrentDataCenter))
                        {
                            setFlag = true;
                            return setFlag;
                        }
                    }
                    else
                    {
                        return false;
                    }
                }
                else
                {
                    return false;
                }
            }
            catch (Exception ex)
            {

            }
            return setFlag;
        }

        /// <summary>
        /// Private Method: To check if Price is Reset or not.
        /// </summary>
        /// <param name="currentLineItemInfo">This contains all the details for current Line Item.</param>
        /// <param name="lineItemInfo">This contains all the details for all the Line Items.</param> 
        /// <returns>Boolean: True if Price is Reset.</returns>
        private bool IsPriceReset(CartLineItemModel currentLineItemInfo, List<CartLineItemModel> lineItemInfo)
        {
            var previousRampLine = GetPreviousRampLine(currentLineItemInfo, lineItemInfo);
            if (previousRampLine == null)
                return false;
            return currentLineItemInfo.NetUnitPrice.ConvertToDouble() != previousRampLine.NetUnitPrice.ConvertToDouble();
        }

        /// <summary>
        /// Private Method: To get previous Ramp Line data.
        /// </summary>
        /// <param name="currentLineItemInfo">This contains all the details for current Line Item.</param>
        /// <param name="lineItemInfo">This contains all the details for all the Line Items.</param> 
        /// <returns>All the data for Previos Ramp Line.</returns>
        private static CartLineItemModel GetPreviousRampLine(CartLineItemModel currentLineItemInfo, List<CartLineItemModel> lineItemInfo)
        {
            return lineItemInfo.Where(l => l.LineNumber == currentLineItemInfo.LineNumber
                                                                        && l.ItemSequence == currentLineItemInfo.ItemSequence - 1).SingleOrDefault();
        }

        /// <summary>
        /// Private Method: To get Associate Asset Line Item's End Date..
        /// </summary>
        /// <param name="currentLineItemInfo">This contains all the details for current Line Item.</param>
        /// <param name="lineItemInfo">This contains all the details for all the Line Items.</param>
        /// <returns>DateTime: End Date for the Associate Asset Line Item.</returns>
        private DateTime? GetAssociatedAssetLineItemEndDate(CartLineItemModel currentLineItemInfo, List<CartLineItemModel> lineItemInfo)
        {
            var currentLineItems = lineItemInfo.Where(x => x.LineNumber == currentLineItemInfo.LineNumber);
            return currentLineItems.Where(c => c.ItemSequence == currentLineItems.Max(m => m.ItemSequence)).SingleOrDefault()?.ALI.EndDate;
        }

        private DateTime GetMaxLineItemEndDate(List<CartLineItemModel> lineItemInfo)
        {
            return lineItemInfo.Where(l => l.EndDate == lineItemInfo.Max(m => m.EndDate)).FirstOrDefault().EndDate;
        }

        #endregion  Private Methods



        #region PS
        /// <summary>
        /// Calculation of Travel & Expense price 
        /// </summary>
        /// <param name="quoteId"></param>
        /// <param name="accessToken"></param>
        /// <returns></returns>
        public HttpResponseMessage CalculateTravelExpensePrice(string quoteId, string accessToken)
        {
            try
            {
                decimal TotalPrice = 0;
                var TEProdcutList = GetTravelExpenseProdcutList(accessToken);
                if (TEProdcutList == null)
                {
                    return new HttpResponseMessage
                    {
                        Content = new StringContent("TravelExpensePrice" + TEProdcutList, System.Text.Encoding.UTF8, "application/json"),
                        StatusCode = System.Net.HttpStatusCode.OK
                    };
                }
                var query = new Query(Constants.OBJ_QUOTE);
                query.AddColumns(Constants.OBJ_TANDECARTLINEITEM_SELECTFIELD.Split(Constants.CHAR_COMMA));
                Expression exp = new Expression(ExpressionOperator.AND);
                exp.AddCondition(new Condition(Constants.FIELD_ID, FilterOperator.Equal, quoteId));
                query.SetCriteria(exp);

                Join prodcutConfiguration = new Join(Constants.OBJ_QUOTE, Constants.OBJ_PRODUCTCONFIGURATION, Constants.FIELD_ID, Constants.FIELD_QUOTEID, JoinType.INNER, Constants.OBJ_PRODUCTCONFIGURATION);
                Join prodConfigJoin = new Join(Constants.OBJ_PRODUCTCONFIGURATION, Constants.OBJ_LINEITEM, Constants.FIELD_ID, Constants.FIELD_CONFIGURATIONID, JoinType.INNER, Constants.OBJ_LINEITEM);

                query.AddJoin(prodcutConfiguration);
                query.AddJoin(prodConfigJoin);

                var jsonQuery = query.Serialize();
                var reqConfig = new RequestConfigModel();
                reqConfig.accessToken = accessToken;
                reqConfig.searchType = SearchType.AQL;
                reqConfig.objectName = Constants.OBJ_QUOTE;
                reqConfig.appUrl = ConfigurationManager.AppSettings[Constants.CONFIG_APPURL];

                var response = Utilities.Search(jsonQuery, reqConfig);

                if (response != null && response.IsSuccessStatusCode)
                {
                    var responseString = JObject.Parse(response.Content.ReadAsStringAsync().Result).SelectToken(Constants.NODE_SERIALIZEDRESULTENTITIES).ToString();
                    //var lineItemData = JsonConvert.DeserializeObject<List<TravelExpenseProductModel>>(responseString).OrderBy(a=>a.LineItem.ParentBundleNumber).ToList();
                    var lineItemData = JsonConvert.DeserializeObject<List<TravelExpenseProductModel>>(responseString);
                    var TEProdcutPriceList = new List<dynamic>();
                    decimal baseExtendedPrice = 0;
                    bool IsMatchedBundle = false;
                    foreach (var lineItem in lineItemData)
                    {

                        if (lineItem.LineItem.OptionId == null)
                        {
                            IsMatchedBundle = false;
                            baseExtendedPrice = lineItem.LineItem.BaseExtendedPrice == null ? 0 : lineItem.LineItem.BaseExtendedPrice.Value;
                            foreach (var teProdcut in TEProdcutList)
                            {
                                if (teProdcut.ProductId.Id == lineItem.LineItem.ProductId.Id)
                                {
                                    TotalPrice = TotalPrice + baseExtendedPrice;
                                    IsMatchedBundle = true;
                                }
                            }
                        }
                        else if (!IsMatchedBundle)
                        {

                            foreach (var teProdcut in TEProdcutList)
                            {
                                if (teProdcut.ProductId.Id == lineItem.LineItem.OptionId.Id)
                                {
                                    TotalPrice = TotalPrice + baseExtendedPrice;
                                    IsMatchedBundle = true;
                                }
                            }
                        }
                    }
                    if (TotalPrice != 0)
                    {
                        TotalPrice = (TotalPrice * 10) / 100;
                        return new HttpResponseMessage
                        {
                            Content = new StringContent("TravelExpensePrice" + TotalPrice, System.Text.Encoding.UTF8, "application/json"),
                            StatusCode = System.Net.HttpStatusCode.OK
                        };
                    }
                }

                return new HttpResponseMessage
                {
                    Content = new StringContent("TravelExpensePrice:" + TotalPrice, System.Text.Encoding.UTF8, "application/json"),
                    StatusCode = System.Net.HttpStatusCode.OK
                };
            }
            catch (Exception ex)
            {
                return new HttpResponseMessage
                {
                    Content = new StringContent("Message :" + ex.Message + " " + "StackStrace :" + ex.StackTrace, System.Text.Encoding.UTF8, "application/json"),
                    StatusCode = System.Net.HttpStatusCode.BadRequest
                };
            }
        }

        /// <summary>
        /// Get Travel and ExpenseP rodcutList
        /// </summary>
        /// <param name="accessToken"></param>
        /// <returns></returns>
        public List<TravelExpenseProductGroupModel> GetTravelExpenseProdcutList(string accessToken)
        {
            try
            {
                var query = new Query(Constants.OBJ_CPQ_PRODCUTGROUPMEMBER);
                query.AddColumns(Constants.OBJ_PRODCUTGROUPMEMBER_SELECTFIELD.Split(Constants.CHAR_COMMA));
                var jsonQuery = query.Serialize();
                var reqConfig = new RequestConfigModel();
                reqConfig.accessToken = accessToken;
                reqConfig.searchType = SearchType.AQL;
                reqConfig.objectName = Constants.OBJ_CPQ_PRODCUTGROUPMEMBER;

                var response = Utilities.Search(jsonQuery, reqConfig);

                if (response != null && response.IsSuccessStatusCode)
                {
                    var responseString = JObject.Parse(response.Content.ReadAsStringAsync().Result).SelectToken(Constants.NODE_SERIALIZEDRESULTENTITIES).ToString();
                    var data = JsonConvert.DeserializeObject<List<TravelExpenseProductGroupModel>>(responseString);
                    return data.Where(d => d.ProductGroupId.Name == Constants.STR_TRAVELEXPENSE_GROUP).ToList();
                }

                return null;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion PS
    }
}
