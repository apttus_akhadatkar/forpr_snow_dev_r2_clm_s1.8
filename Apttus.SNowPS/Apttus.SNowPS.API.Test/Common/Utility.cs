﻿/****************************************************************************************
@Name: Utility.cs
@Author: Kiran Satani
@CreateDate: 7 Oct 2017
@Description: Utilty for Unit Test Project
@UsedBy: This will be used by development of unit test.

@ModifiedBy: 
@ModifiedDate: 
@ChangeDescription: 
*****************************************************************************************/


using Newtonsoft.Json;
using System.Collections.Generic;
using System.IO;
using System.Reflection;

namespace Apttus.SNowPS.API.Test.Common
{
	public static class Utility
	{
		/// <summary>
		/// Get Json
		/// </summary>
		/// <param name="moduleName"></param>
		/// <param name="requestJsonFileName"></param>
		/// <returns></returns>
		public static List<Dictionary<string, object>> GetRequestDictionary(string moduleName, string requestJsonFileName)
		{
			List<Dictionary<string, object>> jsonContent = new List<Dictionary<string, object>>();
			var absolteFilePath = Assembly.GetExecutingAssembly().Location.Substring(0, Assembly.GetExecutingAssembly().Location.IndexOf("bin"));
			var jsonFileUrl = absolteFilePath + "Json/" + moduleName + "/" + requestJsonFileName + ".json";
			using (StreamReader r = new StreamReader(jsonFileUrl))
			{
				string json = r.ReadToEnd();
				jsonContent = JsonConvert.DeserializeObject<List<Dictionary<string, object>>>(json);
			}
			return jsonContent;
		}

	}
}
