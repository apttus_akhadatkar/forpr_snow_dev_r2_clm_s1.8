﻿/****************************************************************************************
@Name: QuoteControllerTests.cs
@Author: Kiran Satani
@CreateDate: 7 Oct 2017
@Description: Quote for Unit Test Project
@UsedBy: This will be used by quote for unit test.

@ModifiedBy: 
@ModifiedDate: 
@ChangeDescription: 
*****************************************************************************************/


using Apttus.SNowPS.API.Controllers;
using Apttus.SNowPS.API.Test.Common;
using Apttus.SNowPS.Common;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Hosting;

namespace Apttus.SNowPS.API.Test.Controllers
{
	[TestClass()]
	public class QuoteControllerTests
	{
		[TestMethod()]
		public void PostTest()
		{
			try
			{
				var controller = new QuoteController();
				controller.Request = new HttpRequestMessage();
				controller.Request.Properties.Add(HttpPropertyKeys.HttpConfigurationKey, new HttpConfiguration());
				controller.Request.Headers.Add("Authorization", Utilities.GetAuthToken());

				List<Dictionary<string, object>> jsonContent = Utility.GetRequestDictionary("Quote", "QuoteSynch");
				var test = controller.SyncToMule(jsonContent[0]);
				if (!test.IsSuccessStatusCode)
				{
					Assert.IsTrue(false, "Fail while asserting");
				}
			}
			catch (AssertFailedException ex)
			{
				throw new Exception(ex.Message);
			}
		}
	}
}